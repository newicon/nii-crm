angular.module('CrmApp')
.controller('pipeline', function($scope, DealService, DealStatusService, DealDate, Contacts){
	Contacts.filters = {};
	DealService.reset();
	$scope.deals = DealService;
	$scope.deals.fetch();
	$scope.statusService = DealStatusService;
	$scope.dealDate = DealDate;
	$scope.byPercent = function(){
		DealService.amountByPercent = !DealService.amountByPercent;
	};
	$scope.dateBack = function(){
		DealDate.dateBack();
	};
	$scope.dateForward = function(){
		DealDate.dateForward();
	};
	$scope.filterByClosed = function(value, index){
		if (value.status == 'WON' || value.status == 'LOST') {
			return true;
		}
		return false;
	};
	$scope.filterByClosed = function(value, index){
		return DealService.isClosed(value);
	};
	$scope.filterByOpen = function(value, index){
		return DealService.isOpen(value);
	};
})

.directive('dealContact', function(){
	return {
		restrict:'E',
		scope: {deal:'='},
		link: function($scope, element, attrs){
			var deal = $scope.deal;
			var html = '<a href="#/contact/'+deal.contact_id+'">' +
				'<strong class="crm-deal-contact-name">' + deal.contact_name + '</strong>';
			if (deal.company_name) {
				html += ' (' + deal.company_name + ')';
			}
			html += '</a>';
			element.replaceWith(html);
        }
	};
})

/**
 * Attributes:
 * deal: a deal model
 * save-changes: 'true' if true then the deal will save the changes
 * @param {type} assetUrl
 * @param {type} DealStatusService
 * @returns {pipeline-controller_L47.pipeline-controllerAnonym$1}
 */
.directive('dealStatusDd', function(assetUrl, DealStatusService){
	return {
		restrict: 'E',
		scope: {deal:'=', saveChanges:'='},
		templateUrl:assetUrl+'/deals-pipeline/deal-status-dd.html',
		replace:true,
		link:function($scope, element, attrs){
			$scope.status = DealStatusService;
			$scope.setDealStatus = function(deal, status){
				deal.status = status;
				console.log(attrs.saveChanges,'attrs.saveChanges');
				if (attrs.saveChanges == 'true') {
					deal.save();
				}
			}
		}
	}
})

.directive('dealDatepicker', function ($parse) {
    return {
		template:'<div><a href ng-click="showDp()">{{deal.close_date | mysqlDate:\'d MMM yyyy\'}} <span class="caret" style="margin-top:8px;"></span></a><input ng-model="deal.close_date" style="visibility:hidden;width:1px;height:1px;" /></div>',
		scope: {deal:'=', saveChanges:'='},
		link:function (scope, element, attrs, controller) {
			var ngModel = $parse(attrs.ngModel);
			var $input = element.find('input');
			var $a = element.find('a');
			scope.showDp = function(){
				$input.datepicker('show');
			};
			$input.datepicker({
				dateFormat: 'yy-mm-dd 09:00:00',
				onSelect:function (dateText, inst) {
					scope.$apply(function(scope){
						// Change binded variable
						ngModel.assign(scope, dateText);
						if (attrs.saveChanges == 'true') {
							scope.deal.save();
						}
					});
				}
			});
		}
    };
})

.filter('stripZeros', function () {
	return function(input) {
		if (input)
			return input.replace('.00', '');
		return input;
	};
})

/**
 * Adds the ability for a text input to accept a currency in a human format
 */
.directive('currecnyInput', function ($filter) {	
    return {
		require:'ngModel',
		link:function($scope, $element, $attrs, ngModelCtrl){
			// coverts a model value into a view value (add to angular)
			ngModelCtrl.$formatters.push(function(modelValue){
				return $filter('currency')(modelValue, '£');
			});
			// coverts a view value into a model value (add to angular)
			ngModelCtrl.$parsers.push(function(viewValue){
				// convert a currency back to a number
				// remove all non digit or dot characters.
				var modelValue = parseFloat(viewValue.replace(/[^0-9\.]+/g,""));
				if (isNaN(modelValue))
					modelValue = 0;
				return modelValue;
			});
		}
    };
})

/**
 * Edit in place box for amount.
 */
.directive('dealAmount', function ($timeout) {
	var template = '<div>';
	template += '<div ng-hide="editable" style="text-decoration:underline;cursor:pointer;" ng-click="makeEditable()">{{deal.amount | currency:\'£\'}} <span ng-show="deal.multi_month==1">(x {{deal.month_count}})</span></div>';
	template += '<div ng-show="editable" class="input-append mbn">';
	template +=     '<input ng-keyup="keyUp($event)" style="width:80px;border-radius:3px;" ng-model="deal.amount" currecny-input type="text" />';
	template +=     '<button class="btn phs btn-link" ng-click="cancel()"><i class="icon-remove"></i></button>';
	template +=     '<button class="btn pls prs  btn-link" ng-click="save()"><i class="icon-ok"></i></button>';
	template += '</div>';
	template += '</div>';
    return {
		template:template,
		scope: {deal:'=', saveChanges:'='},
		link:function ($scope, element, attrs) {
			$scope.editable = false;
			var amount = $scope.deal.amount
			$scope.makeEditable = function(){
				$scope.editable = true;
				// store the amount before editing
				amount = $scope.deal.amount;
				$timeout(function(){element.find('input').focus()}, 50);
			}
			var escapeKey = 27, returnKey = 13;
			$scope.keyUp = function($event){
				if ($event.keyCode == escapeKey)
					$scope.cancel();
				if ($event.keyCode == returnKey)
					$scope.save();
			}
			$scope.save = function(){
				$scope.editable = false;
				$scope.deal.save();
			}
			$scope.cancel = function(){
				// undo the amount back to original value before we started editing
				$scope.deal.amount = amount;
				$scope.editable = false;
			}
		}
    };
})