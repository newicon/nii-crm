angular.module('CrmApp')
.service('DealService', function(Andalay, $http, baseUrl, DealStatusService){
	var Model = Andalay.Model.extend({
		isClosed:function(){
			return (this.status == 'WON' || this.status == 'LOST' || this.status == 'DORMANT');
		},
		isOpen:function(){
			return (this.status != 'WON' && this.status != 'LOST' && this.status != 'DORMANT');
		},
		/**
		 * Different to simply returning this.amount
		 * If the collection valueByPercent then the amount is a percentage of the amount
		 * based on its current pending stage
		 * @returns {undefined}
		 */
		getAmount:function(){
			if (!this.collection.amountByPercent)
				return this.amount;
			var status = DealStatusService.get(this.status);
			var percent = status.percent;
			return this.amount * (percent / 100);
		},
		// returns the close date as a javascript Date object
		getCloseDate:function(){
			// Split timestamp into [ Y, M, D, h, m, s ]
			var t = this.close_date.split(/[- :]/);
			// Apply each element to the Date function
			var d = new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5]);
			return d;
		}
	});
	var Collection = Andalay.Collection.extend({
		url: baseUrl+'/crm/api/deal',
		model:Model,
		amountByPercent:true,
		getAmountTotal:function(type){
			collection = this;
			var total = 0;
			if (type == 'closed') {
				this.forEach(function(model){
					if (model.isClosed()) {
						total += parseFloat(model.amount)
					}
				});
			} else if (type == 'open') {
				this.forEach(function(model){
					if (model.isOpen()) {
						total += parseFloat(model.amount)
					}
				});
			} else {
				this.forEach(function(model){
					total += parseFloat(model.amount)
				});
			}
			return total;
		},
		getAllClosed:function(){
			return this.forEach(function(model){
				return Collection.isClosed(model);
			});
		},
		getAllOpen:function(){
			return this.forEach(function(model){
				return Collection.isOpen(model);
			});
		},
		isClosed:function(model){
			return (model.status == 'WON' || model.status == 'LOST' || model.status == 'DORMANT');
		},
		isOpen:function(model){
			return (model.status != 'WON' && model.status != 'LOST' && model.status != 'DORMANT');
		},
		getDealsFor: function(contactId){
			if (!contactId) return [];
			var collection = this;
			$http.get(baseUrl+'/crm/api/deal/for/contact/'+contactId).success(function(response){
				collection.addMany(response);
			});
			return this.filter(function(model){model.contact_id = contactId});
		}
	});
	return new Collection();
});

angular.module('CrmApp')
.service('DealStatusService', function(Andalay, $http, baseUrl){
	var Model = Andalay.Model.extend({});
	var Collection = Andalay.Collection.extend({
		model:Model,
		getAllPending:function(){
			return this.where({type:'PENDING'});
		}
	});
	return new Collection([
		{percent: '10',  name:'Qualification',	id:'PENDING_10', type:'PENDING'},
		{percent: '25',  name:'',				id:'PENDING_25', type:'PENDING'},
		{percent: '50',  name:'Decision',		id:'PENDING_50', type:'PENDING'},
		{percent: '75',  name:'',				id:'PENDING_75', type:'PENDING'},
		{percent: '90',  name:'Negotiation',	id:'PENDING_90', type:'PENDING'},
		{percent: '100', name:'Won',			id:'WON',        type:'WON'},
		{percent: '0',   name:'Lost',			id:'LOST',       type:'LOST'},
		{percent: '0',   name:'Dormant',		id:'DORMANT',    type:'LOST'}
	]);
});

angular.module('CrmApp')
.service('DealDate', function(Andalay, $http, baseUrl, DealService){
	var Model = Andalay.Model.extend({
		getDeals:function(){
			var dateModel = this;
			return DealService.filter(function(deal) {
				return dateModel.dealDateMatchesMyDate(deal);
			});
		},
		dealDateMatchesMyDate:function(dealModel) {		
			// get the current month and set the to the 1st of that month
			var intCurrentDate =  Date.UTC(this.date.getFullYear(), this.date.getMonth(), 1);
			// set the close date to the first day of the close date month (e.g. so 22nd of July would become the 1st of July)
			var dCloseDate = dealModel.getCloseDate();
			var intCloseDate = Date.UTC(dCloseDate.getFullYear(), dCloseDate.getMonth(), 1);
			// monthly deals span multiple date ranges....
			if (dealModel.multi_month == 1) {
				// set the final date as the close date + the number of months in the deal
				var intFinalMonthDate = Date.UTC(dCloseDate.getFullYear(), dCloseDate.getMonth()+parseInt(dealModel.month_count), 1);
				return (intCurrentDate >= intCloseDate && intCurrentDate <= intFinalMonthDate)
			} else {
				return (intCurrentDate === intCloseDate);
			}
		},
		
		getDealTotalWonAndPending:function(){
			var total = 0;
			var dateModel = this;
			DealService.forEach(function(deal){
				if (dateModel.dealDateMatchesMyDate(deal) && deal.status != 'LOST') {
					total += parseFloat(deal.getAmount());
				}
			});
			return total;
		},
		getDealTotalWonAmount:function(){
			var total = 0;
			var dateModel = this;
			DealService.forEach(function(deal){
				if (dateModel.dealDateMatchesMyDate(deal) && deal.status == 'WON') {
					total += parseFloat(deal.getAmount());
				}
			});
			return total;
		},
		getDealPendingAmount: function(){
			return this._getDealTotalPendingAmount().total;
		},
		getDealPendingCount: function(){
			return this._getDealTotalPendingAmount().total;
		},
		/**
		 * Gets the total amount of pending deals for the current date
		 * returns object with total and count properties
		 * @returns {total:int, count:int}
		 */
		_getDealTotalPendingAmount: function(){
			var ret = {total:0, count:0};
			var dateModel = this;
			DealService.forEach(function(deal){
				if (dateModel.dealDateMatchesMyDate(deal) && deal.status != 'LOST' && deal.status != 'WON') {
					ret.total += parseFloat(deal.getAmount());
					ret.count++;
				}
			});
			return ret;
		},
		getDealAmountForState: function(state){
			return this._getDealTotalAmountForState(state).total;
		},
		getDealCountForState: function(state){
			return this._getDealTotalAmountForState(state).count;
		},
		/**
		 * Gets the total amount for a given state, and the number of deals
		 * The returned object has a total, and count property
		 * @param string state the state id eg: 'PENDING_10', 'WON'
		 * @returns Object with total, and count properties
		 */
		_getDealTotalAmountForState: function(state){
			var ret = {total:0, count:0};
			var dateModel = this;
			DealService.forEach(function(deal){
				if (dateModel.dealDateMatchesMyDate(deal) && deal.status == state) {
					ret.total += parseFloat(deal.getAmount());
					ret.count++;
				}
			});
			return ret;
		}
	});
	var Collection = Andalay.Collection.extend({
		model:Model,
		dateBack:function(){
			this.forEach(function(model){
				model.date = moment(model.date).subtract(1, 'month').toDate();
			});
		},
		dateForward:function(){
			console.log('Forward');
			this.forEach(function(model){
				model.date = moment(model.date).add(1, 'month').toDate();
			});
		}
	});
	var Dates = new Collection();
	Dates.addMany([
		{id:1, date:moment().toDate()},
		{id:2, date:moment().add(1,'month').toDate()},
		{id:3, date:moment().add(2,'month').toDate()},
		{id:4, date:moment().add(3,'month').toDate()},
		{id:5, date:moment().add(4,'month').toDate()},
		{id:6, date:moment().add(5,'month').toDate()}
	]);
	return Dates;
});