'use strict';

angular.module('CrmApp').service('Contacts', function($http, $q, $resource, $collection, $state) {
	
	var Api = {};
	
	Api.collection = $collection.getInstance({idAttribute: 'id'});
	Api.collection.loading = false;
	
	
	Api.collection.count = {
		ownedByMe : 0
	};
	
	// stores the results of querying contacts
	// results : array of contact objects
	// pagination : attributes relating to paination
	Api.pagination = {};
	// the current contact in the detail view
	Api.current = null;
	
	// depreacted to be removed in favour of Api.current
	Api.activeContactId = null;
	// idea to improve things...
	//	Api.current = {}; // the current contact selected (the object with id activeContactId)
	Api.checkedContacts = [];
	
	// Default filters
	Api.defaultFilters = {};
	
	// Filters that are currently applied
	Api.filters = angular.copy(Api.defaultFilters);
	
	Api.Contact = $resource(CrmApp.baseUrl+'/crm/api/contact/:id', {id:'@id'});
	
	Api.reset = function(data){
		Api.collection.removeAll();
		var models = [];
		if (_.isArray(data.results)) {
			models = data.results
		} else if (_.isArray(data)) {
			models = data
			Api.collection.addAll(data);
		} else {
			throw "Unrecognised argument passed to reset function. You should pass either " +
				  "an array of contact objects or an object in the format of {results:[], pagination:{}}"
		}
		Api.collection.addAll(models);
		if (_.isObject(data.pagination)) {
			Api.pagination = data.pagination;
		}
		// relink the Api.current model;
		angular.forEach(Api.collection.all(), function(model){
			if (Api.current !== null && Api.current.id == model.id) {
				// merges with the Api.current model in case the user has completed changes (that are not yet in sync)
				// changes the Api.current to reference the module in the collection. 
				// Thus updates are reflected in the list view.
				Api.current = angular.extend(model, Api.current);
			}
		});
	};
	
	Api.addAllAndRelinkCurrent = function(models){
		angular.forEach();
	};
	
	Api.loadMore = function(){
		if (Api.pagination.page < Api.pagination.page_count) {
			Api.collection.loading = true;
			Api.pagination.page = parseInt(Api.pagination.page) + 1;
			$http(Api.getHttpConfig({}, Api.pagination.page)).success(function(json){
				Api.collection.loading = false;
				Api.pagination = json.pagination;
				// append results
				Api.collection.addAll(json.results);
			});
		}
	}
	
	Api.loadPrev = function(){}
	
	/**
	 * 
	 * @param object filters
	 * @param boolean reset // resets all filters and only applies those passed in the filters param
	 * @return void
	 */
	Api.find = function(filters, reset){
		Api.collection.loading = true;
		reset = reset || false;
		if (reset) {
			Api.filters = angular.copy(Api.defaultFilters);
		}
		var url = Api.getHttpConfig(filters, 0);
		return $http(url).success(function(json){
			Api.collection.loading = false;
			if (json.pagination) {
				Api.pagination = json.pagination;
				Api.reset(json.results);
			}
		});
	}

	Api.removeFilters = function(){
		Api.filters = angular.copy(Api.defaultFilters);
	}

	// specify a filters object to filter the results
	// or pass an empty object {} to use the currently set filters
	Api.getHttpConfig = function(filters, page){
		filters = _.extend(Api.filters, filters);
		// remove default filters if they are empty
		if (Api.filters.name == '')
			delete Api.filters.name;
		if (Api.filters.letter == '')
			delete Api.filters.letter;
		if (Api.filters.group == '')
			delete Api.filters.group;
		
		filters.page = page;
		var url = CrmApp.url('crm/api/contact', filters);
		return {url: url, method:'GET', headers:{"X-Requested-With":"XMLHttpRequest"}};
	}
	
	Api.findById = function(id){
		return Api.collection.get(id);
	}
	
	Api.setActive = function(id){
		Api.activeContactId = id;
	}
	
	Api.check = function(id){
		Api.checkedContacts.push(id);
	}
	
	Api.uncheck = function(id){
		var index = _.indexOf(Api.checkedContacts, id);
		if (index != -1)
			Api.checkedContacts.splice(index, 1);
	}
	
	Api.isChecked = function(id){
		return _.find(Api.checkedContacts, function(cid) {
			return cid == id;
		});
	}
	
	// return an array of ids representing each checked contact
	Api.getChecked = function(){
		return Api.checkedContacts;
	}
	
	Api.checkAll = function(){
		_.each(Api.collection.all(), function(c){
			Api.checkedContacts.push(c.id);
		});
	}
	
	Api.uncheckAll = function() {
		Api.checkedContacts = [];
	};
	
	Api.countChecked = function() {
		return Api.checkedContacts.length;
	};
	
	Api.deleteContact = function(contactId) {
		Api.collection.remove(contactId);
		$http({method:'DELETE', url:CrmApp.url('crm/api/contact/'+contactId)});
	}
	
	// looks up a contact from the local storage and returns it,
	// if not found will initiate an ajax request to get the contact
	// returns an angular promise object with the contact
	// e.g. getContact(123).then(function(contact){})
	Api.getContact = function(id) {
		var deferred = $q.defer();
		// first try to find in local objects
		var c = Api.findById(id);
		if (!_.isUndefined(c)) {
			// found locally so return
			 deferred.resolve(c);
		} else {
			// not found locally so do an ajax request
			var params = {
				url: CrmApp.url('crm/api/contact/'+id), 
				method:'GET', 
				headers:{"X-Requested-With":"XMLHttpRequest"}
			}
			$http(params).success(function(ret){
				Api.collection.add(ret);
				deferred.resolve(ret);
			}).error(function(error){
				deferred.reject(error);
			});
		}
		return deferred.promise;
	};
	
	/**
	 * Remove a contact from a group
	 * @param int groupId
	 * @param id || array contactIds array of contactIds
	 * @return promise
	 */
	Api.removeFromGroup = function(groupId, contactIds) {
		var deferred = $q.defer();
		if (!_.isArray(contactIds))
			contactIds = [contactIds];
		
		// if the current collection is filtered for this group
		// then remove contacts as they have been removed from this group
		if (Api.filters.group == groupId) {
			angular.forEach(contactIds, function(id){
				Api.collection.remove(id);
			})
		}
		$http.post(CrmApp.url('/crm/api/group/'+groupId+'/removeContacts'), {"contacts": contactIds}).success(function(data) {
			 deferred.resolve(data.result);
		});
		return deferred.promise;
	}
	
	/**
	 * Get the first letter of the contact name
	 * For use to display the alphabet position of this contact
	 * note a contact staring with a number will return a '#'
	 * @param string name
	 * @return string
	 */
	Api.getFirstLetter = function(name){
		var letter = name[0]
		// if the letter is a number set to a hash
		if (!_.isNaN(parseInt(letter)))
			letter = '#';
		if (_.isUndefined(name) || _.isEmpty(name))
			letter = '?';
		return letter.toUpperCase();
	}
	
	
	/**
	 * Save a contact, creates a new contact if the passed in contact object does not have an id attribute
	 * 
	 */
	Api.save = function(contact){
		if (!_.isObject(contact) || _.isArray(contact) || _.isFunction(contact)) {
			throw "Contact argument must be an object";
		}
		
		Api.collection.save = true;
		var idAttr = Api.collection.idAttribute;
		var url = CrmApp.url('crm/api/contact');
		if (_.has(contact, 'id') && !_.isUndefined(contact[idAttr]) && contact[idAttr] != '')
			url += '/' + contact[idAttr];
		return $http.post(url, contact).success(function(response){
			Api.collection.save = false;
//			Api.collection.update(response);
			return response;
		});
	};
	
	Api.link = function(contact) {
		return '#/contact/'+contact.id;
	};
	
	return Api;
});