'use strict';
angular.module('CrmApp').service('Actions', function($http, $q, $collection) {
	
	return _.extend($collection.getInstance({idAttribute: 'id'}), {
		
		dueLabel: function(action) {
			if (!_.isObject(action) || action.due == '')
				return '';
			if (action.status === 'DONE')
				return 'Done';
			if (action.due_select == 'asap')
				return 'ASAP';
			var due = moment(action.due, 'YYYY-MM-DD HH:mm:ss');
			// if due is yesterday
			if (due.isSame(moment().subtract('days', 1), 'day'))
				return 'Yesterday';
			// if due is today
			if (due.isSame(moment(), 'day'))
				return 'Today';
			// if due is tomorrow
			if (due.isSame(moment().add('days', 1), 'day'))
				return 'Tomorrow';
			return due.format('D MMM YY');
		},
		
		dueColor: function(action) {
			if (!_.isObject(action) || action.due == '')
				return '';
			if (action.status == 'DONE')
				return '#468847';
			if (action.due_select == 'asap')
				return '#ED1313';
			var due = moment(action.due, 'YYYY-MM-DD HH:mm:ss');
			// if due is before yesterday
			if (due.isBefore(moment().subtract('days', 2), 'day'))
				return '#b94a48';
			// if due is yesterday
			if (due.isSame(moment().subtract('days', 1), 'day'))
				return '#dd5e16';
			// if due is today
			if (due.isSame(moment(), 'day'))
				return '#ED8713';
			// if due is tomorrow
			if (due.isSame(moment().add('days', 1), 'day'))
				return '#dbbc40';
			// if due is 5 days away
			if (due.isAfter(moment().add('days', 5), 'day'))
				return '#40dbd5';
			// if due is 4 days away
			if (due.isAfter(moment().add('days', 4), 'day'))
				return '#40db91';
			// if due is 3 days away
			if (due.isAfter(moment().add('days', 3), 'day'))
				return '#40db53';
			// if due is 2 days away
			if (due.isAfter(moment().add('days', 2), 'day'))
				return '#6bdb40';
			// if due is 1 days away
			if (due.isAfter(moment().add('days', 1), 'day'))
				return '#bcdb40';
		}
		
	});
	
});