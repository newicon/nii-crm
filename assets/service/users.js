'use strict';
angular.module('CrmApp').service('Users', function($http, $q, $collection) {
	
	var Api = {};
	
	Api.collection = $collection.getInstance({idAttribute: 'id'});
	
	var Users = angular.extend(Api.collection, {
		
	});

	return Users;
	
});