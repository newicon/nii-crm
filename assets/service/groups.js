'use strict';
angular.module('CrmApp').service('Groups', function($resource, $http, $collection, baseUrl) {
	
	var Api = {};
	
	Api.collection = $collection.getInstance({idAttribute: 'id'});
	
	return angular.extend(Api.collection, {
		
		Group: $resource(CrmApp.baseUrl+'/crm/api/group/:id', {id:'@id'}, {
			addContacts: {method:'POST'}
		}),
		
		reset : function(data) {
			this.removeAll();
			this.addAll(data);
		},
		
		findById : function(id) {
			return this.get(id);
		},
		
		addContacts : function(groupId, contacts, success, error) {
			var url = CrmApp.url('/crm/api/group/'+groupId+'/addContacts');
			error = error || function(){};
			$http.post(url, {"contacts": contacts}).success(success).error(error);
		},
		
		destroy:function(group) {
			Api.collection.remove(group);
			$http.delete(baseUrl+'/crm/api/group/'+group.id);
		},
		
		// returns the largest order value currently in the list
		getLargestOrderValue: function(){
			var order = 0;
			angular.forEach(this.array, function(model){
				if (model.order > order)
					order = model.order;
			});
			return order;
		},
		
		save : function(group) {
			if (!group.name) {
				// dont bother saving if the name is empty
				Api.collection.remove(group);
				return;
			}
			var url = this.isNew(group) ? CrmApp.url('crm/api/group/') : CrmApp.url('crm/api/group/'+group.id);
			
			return $http.post(url, group).success(function(data){
				//TODO add error message if duplicate group is entered
				// remove group with auto-gen temporary key
				Api.collection.remove(group.id);
				group = Api.collection.get(data.id);
				Api.collection.update(data);
			}).error(function(){
				// if the group could not be saved then remove it.
				Api.collection.remove(group);
			});
		}
		
	});
});