'use strict';
// reset at app startup
angular.module('CrmApp').service('Status', function($http, $q, $resource, $collection) {
	
	var Api = {};
	
	Api.collection = $collection.getInstance({idAttribute: 'id'});
	
	return _.extend(Api.collection, {
		sayHello:function(){
			return 'hello';
		}
	});
	
});