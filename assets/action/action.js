angular.module('CrmApp')
.directive('contactAction', function(assetUrl, $http, Contacts, baseUrl, $stateParams, $timeout, userId, Actions){
	return {
		restrict: 'E',
        templateUrl: assetUrl + '/action/action.html',
        replace: true,
		scope:{},
		link:function($scope, $element, $attrs){
			$scope.data = {};
			$scope.data.action = {};
			$scope.baseUrl = baseUrl;
			$scope.contacts = Contacts;
			// map jquery and moment js formats
			var momentDbFormat = 'YYYY-MM-DD HH:mm:ss';

			$scope.data.action.due = moment().format(momentDbFormat);

			$scope.data.actionReset = function() {
				$scope.data.action.action = '';
				$scope.data.action.due = moment().format(momentDbFormat);
				$scope.data.action.assigned_id = userId;
				$scope.data.action.due_select = 'today';
			};
			
			$scope.data.actionReset();

			$scope.dueSelectChange = function() {
				if ($scope.data.action.due_select == 'asap')
					$scope.data.action.due = '0000-00-00 00:00:00';
				else if ($scope.data.action.due_select == 'today')
					$scope.data.action.due = moment().format(momentDbFormat);
				else if ($scope.data.action.due_select == 'tomorrow')
					$scope.data.action.due = moment().add('days',1).format(momentDbFormat);
				else if ($scope.data.action.due_select == '7_days')
					$scope.data.action.due = moment().add('days',7).format(momentDbFormat);
				else if ($scope.data.action.due_select == '14_days')
					$scope.data.action.due = moment().add('days',14).format(momentDbFormat);
				else if ($scope.data.action.due_select == '30_days')
					$scope.data.action.due = moment().add('days',30).format(momentDbFormat);
			};

			// function called when you check the done tick box
			$scope.done = function() {
				$scope.contacts.current.action.status = 'DONE';
				$scope.data.actionReset();
				$http.post(CrmApp.url('crm/api/action')+'/'+$scope.contacts.current.action.id, 
					$scope.contacts.current.action)
					.success(function(data){});
			};
			// uncheck the done action box
			$scope.undone = function() {
				$scope.contacts.current.action.status = 'TODO';
				$http.post(CrmApp.url('crm/api/action')+'/'+$scope.contacts.current.action.id, $scope.contacts.current.action)
					.success(function(data){});
			};

			$scope.getDueDate = function(action) {
				return Actions.dueLabel(action);
			};
			
			$scope.getDueColor = function(action) {
				return Actions.dueColor(action);
			};

			$scope.actionEditCancel = function() {
				$scope.editMode = false;
				$scope.data.action = angular.copy($scope.undoEditAction);
				$scope.contacts.current.action = $scope.undoEditAction;
			};
			
			$scope.editAction = function() {
				$scope.editMode = true;
				$scope.undoEditAction = angular.copy($scope.contacts.current.action);
				$scope.data.action = $scope.contacts.current.action;
			};

			$scope.saveAction = function() {
				// save action
				$scope.editMode = false;
				var url = CrmApp.url('crm/api/action');
				if ($scope.data.action.id) {
					url += '/' + $scope.data.action.id;
				}
				$scope.data.action.contact_id = $scope.contacts.current.id;
				$http.post(url, $scope.data.action).success(function(data) {
					// need to reorder the actions.
					Contacts.find();
					if (_.isArray($scope.contacts.current.action)) {
						$scope.contacts.current.action.unshift(data);
					} else {
						$scope.contacts.current.action = data;
					}
					
				});
			};
		}
	};
})

.filter('actionDueDate', function(Actions){
	return function(action) {
		return Actions.dueLabel(action);
	};
})

.filter('actionColor', function(Actions){
	return function(action) {
		return Actions.dueColor(action);
	};
});