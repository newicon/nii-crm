// contact detail view controller
CrmApp.contactCtrl = function($scope, $stateParams, baseUrl, Contacts, $http, Status, Users, Groups, $rootScope, Actions, DEBUG) {
	$scope.debug = DEBUG;
	$scope.groups = Groups;
	$scope.status = Status;
	$scope.users = Users;
	$scope.contacts = Contacts;
	$scope.contactFound = true;
	$scope.user_id = CrmApp.user_id;
	$scope.loading = false;
	$scope.baseUrl = baseUrl;
	// hacky but will do for now
	$scope.isFinanceAdmin = (CrmApp.user_id == 3 || CrmApp.user_id == 7 || CrmApp.user_id == 21);
	if ($stateParams.contactId) {
		console.log('GETTING CONTACT')
		$scope.loading = true;
		Contacts.getContact($stateParams.contactId).then(function(contact) {
			$scope.contacts.current = Contacts.findById(contact.id);
			Contacts.activeContactId = contact.id;
			
			if (contact.kashflow_id && $scope.isFinanceAdmin) {
				$http.get(baseUrl+'/crm/index/quality/customerId/'+$scope.contacts.current.id).success(function(response){
					$scope.customerStats = response;
					$scope.customerStats.profitLossRaw = parseFloat(response.profitLossRaw);
				});
			}
		}, function(error){
			$scope.contactFound = false;
		}).finally(function(){$scope.loading = false;});
	}
	
	// groups / tags
	$scope.removeGroup = function(id){
		// remove from contact collection:
		angular.forEach($scope.contacts.current.groups, function(grp, index){
			if (grp.id == id) {
				$scope.contacts.current.groups.splice(index,1);
			}
		})
		Contacts.removeFromGroup(id, $scope.contacts.current.id).then(function(groupCount){
			var group = Groups.findById(id);
			group.contacts_count = groupCount;
			$rootScope.$$phase || $rootScope.$apply();
		});
		// jquery hack to remove tooltips!
		$('.tooltip').remove();
	};
	
	
	
	$scope.setState = function(stateId) {
		$scope.contacts.current.status_id = stateId;
		$http.post(CrmApp.url('crm/api/contact') + '/' + $scope.contacts.current.id, $scope.contacts.current).success(function(response){
			// too update status counts
		});
	};

	$scope.setOwner = function(ownerId) {
		$scope.contacts.current.owner_id = ownerId;
		$http.post(CrmApp.url('crm/api/contact') + '/' + $scope.contacts.current.id, $scope.contacts.current).success(function(response){
			// too update status counts
			$http.get(CrmApp.url('crm/api/contact/countownedby/userid/'+CrmApp.user_id)).success(function(count){
				Contacts.collection.count.ownedByMe = parseInt(count);
			});
		});
	};
	
	$scope.callNumber = function(number) {
		if (number.substr(0,2) == '44') {
			var phoneNumber = number.substr(2, number.length);
			window.location = baseUrl+'/call/index/webrtc?cc=44&number='+phoneNumber;
		} else {
			alert('Non UK number.  At the moment this only supports UK numbers starting with 44... Its really easy to do though if you fancy updating me :-)');
		}
	}

	// $scope.callNumber = function(number){
	// 	if (!confirm('Are you sure you wish to call '+number+'?'))
	// 		return false;
	// 	$http.get(CrmApp.baseUrl+'/call/index/dial/number/'+number, function(response){
	// 		alert('Dialling: ' + number);
	// 	});
	// };
	
	$scope.mapLink= function(address){
		return 'http://www.google.com/maps?f=q&q='
			+address.lines.replace(/\n/, ' ')+', '+address.city+', '+address.postcode+', '+address.country;
	}
	
}