// group controller
CrmApp.groupCtrl = function($scope, Groups, Contacts, Status, Users, $location) {
	$scope.groups = Groups;
	$scope.contacts = Contacts;	
	$scope.status = Status;
	$scope.myCount = CrmApp.myCount;
	$scope.user_id = CrmApp.user_id;
	$scope.users = Users;

	$scope.companiesCount = CrmApp.companiesCount;
	$scope.contactsCount = CrmApp.contactsCount;
	
	$scope.addGroup = function() {
		var order = Groups.getLargestOrderValue();
		var g = new Groups.Group({color: '#4EACE0', order:order});
		Groups.add(g);
		g.editing = true;
		$scope.editedGroup = g;
	};
	// in order for the orberby to work on the group.order
	// it must be a number
	$scope.groupOrderValue = function(group){
		return parseInt(group.order);
	};
	
	$scope.selectGroup = function(groupId) {
		Contacts.find({group:groupId}, true);
	};
	
	$scope.startEditing = function(group) {
		group.editing = true;
		$scope.editedGroup = group;
	};
	
	$scope.updateColor = function(group) {
		Groups.save(group);
	};

	$scope.doneEditing = function(group) {
		if (group.editing == false)
			return true;
		group.editing = false;
		// if the model is saved in the database then it WILL NOT have a auto generated guid
		if (!group.name) {
			if (angular.isDefined(group.guid)) {
				// this has not yet been saved
				// lets remove it as its empty without fuss
				Groups.remove(group);
			} else {
				// This group exists in the database.  The name has been removed.
				// Do they want to delete it?
				var remove = confirm('Are you sure you wish to delete this group?' + "\n" + 'Note: no contacts will be deleted');
				if (remove) {
					Groups.destroy(group);
					// the filter would currently be active because we had to select it to delete it.
					// therefore lets remove it from the active filter and reload the list
					delete $scope.contacts.filters['group'];
					$scope.contacts.find($scope.contacts.filters, true);
					$location.path('/');
				}
			}
		} else {
			Groups.save(group);
		}
		$scope.editedGroup = null;
	};

	$scope.grpVisible = true;
	$scope.grpVisibleToggle = function(){
		$scope.grpVisible = !$scope.grpVisible;
	}
	$scope.statusVisible = true;
	$scope.statusVisibleToggle = function(){
		$scope.statusVisible = !$scope.statusVisible;
	}
	$scope.teamVisible = true;
	$scope.teamVisibleToggle = function(){
		$scope.teamVisible = !$scope.teamVisible;
	}

	$scope.filterByUser= function(userId){
		Contacts.find({owner_id:userId});
	}
	
	$scope.groupAll = function(){
		$location.path('/');
		Contacts.find({}, true);
	}
	
	$scope.filterStatus = function(statusId){
		Contacts.find({status_id:statusId});
	}
};