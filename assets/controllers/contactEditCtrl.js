// edit controller
CrmApp.contactEditCtrl = function($scope, $stateParams, Contacts, $http, $timeout, Users, $location) {
	
	$scope.users = Users;
	$scope.saving = false;
	$scope.lastSaved = '';
	
	Contacts.getContact($stateParams.contactId).then(function(contact){
		Contacts.collection.add(contact);
		$scope.contact = Contacts.collection.get(contact);
		$scope.contactSaved = angular.copy(contact);
		Contacts.activeContactId = contact.id;
	});
	
	$scope.emailLabels = CrmApp.emailLabels;
	$scope.phoneLabels = CrmApp.phoneLabels;
	$scope.websiteLabels = CrmApp.websiteLabels;
	$scope.addressLabels = CrmApp.addressLabels;
	$scope.add = function(attrArray){
		$scope.contact[attrArray].push({label:''})
	}
	$scope.remove = function(attrArray, $index){
		$scope.contact[attrArray].splice($index, 1)
	}
	
	$scope.deleteContact = function(contactId){
		if (confirm('Are you sure you want to delete this contact?')){
			Contacts.deleteContact(contactId)
			$location.path('/');
		}
	}
	
	// when clicking done / save button
	$scope.done = function(){
		$scope.save(true);
		$location.path('/contact/'+$scope.contact.id);
	}
	
	$scope.$watch('contact', function() {
		$scope.saveThrottle();
	}, true);
	
	// returns true if the local and server copy are different!
	// meaning the record needs to be saqved, or we are currently between ajax calls
	$scope.needsSaving = function() {
		if (_.isUndefined($scope.contactSaved))
			return true;
		// only want to save if the local copy and the server copy are different
		// get keys to compare
		return !$scope.isTheSame($scope.contact, $scope.contactSaved);
	}
	
	$scope.isTheSame = function(contact1, contact2) {
		// only want to save if the local copy and the server copy are different
		// get keys to compare
		var compareKeys = ['name', 'company_role', 'company_id', 'emails', 'phones', 'addresses',
			'websites', 'owner_id', 'background'];
		var c1 = _.pick(contact1, compareKeys);
		var c2 = _.pick(contact2, compareKeys);
		return angular.equals(c1, c2);
	}
	
	$scope.save =  function(force) {
		force = _.isUndefined(force) ? true : false;
		if ($scope.needsSaving() || force) {
			$scope.saving=true;
			Contacts.save($scope.contact).success(function(response){
				$scope.saving=false;
				$scope.contactSaved = angular.copy(response);
				$scope.lastSaved = $scope.contactSaved.updated_at;
				// sometimes multiple ajax requests can be queued meaning that
				// the response given is not the latest update version.
				// Therefore if the scope and the sever response dont match we will prevent updating
				// the collection (preventing overriding local user changes to the model) and assume
				// a later request will update the model appropriatly
				if ($scope.isTheSame(response, $scope.contact)){
					Contacts.collection.update(response);
				}
			})
			$scope.$$phase || $scope.$apply();
		}
	}
	
	var timeout;
	// save contact details
	$scope.saveThrottle = function(){
		// throttle function
		if (timeout)
			clearTimeout(timeout);
		timeout = setTimeout($scope.save, 500, self)
	}
	
	window.onbeforeunload = function () {
		if ($scope.isTheSame($scope.contactSaved, $scope.contact)) {
			return "You have attempted to leave this page.  If you have made any changes to the fields without clicking the Save button, your changes will be lost.  Are you sure you want to exit this page?";
		}
	}
}