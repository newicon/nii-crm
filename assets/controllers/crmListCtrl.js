// list controller
CrmApp.crmListCtrl = function($scope, $state, Status, Actions, Contacts, Users, Groups, $location, $rootScope) {
	$scope.contacts = Contacts;
	$scope.groups = Groups;
	$scope.query;
	$scope.users = Users;
	$scope.status = Status;
	$scope.state = $state;
	$scope.actionService = Actions;
	
	$scope.selectContact = function(contact) {
		$location.path('contact/'+contact.id);
	};
	
	$scope.letterSearch = function(letter){
		$scope.contacts.find({letter:letter});
	};
	// Enables angular to update the height of the filters box.
	// When you add lots of filters they conitue to wrap.
	// This then calculates the hiehgt.
	// It is likely to be very inefficient!
	// Todo: this could all be handled by css: possibly Flex Box?
	$contactFiltersDiv = $('#contact-filters');
	$scope.getTop = function(){
		var top = $contactFiltersDiv.position().top + $contactFiltersDiv.height() + 4;
		return top;
	};
	
	$scope.searchy = function(query) {
		$scope.contacts.find({name:query});
	};
	$scope.checkContact = function($event, id) {
		var checkbox = $event.target;
		if (checkbox.checked) {
			Contacts.check(id);
		} else
			Contacts.uncheck(id)
	}
	$scope.isChecked = function(contact) {
		return Contacts.isChecked(contact.id)
	}
	
	$scope.selectTodo = function() {
		$scope.contacts.find({todo:true}, true);
	}
	
	$scope.checkAll = function($event){
		var checkbox = $event.target;
		if (checkbox.checked)
			Contacts.checkAll();
		else
			Contacts.uncheckAll();
	}
	
	$scope.removeFromGroup = function(contact) {
		Contacts.removeFromGroup(Contacts.filters.group, contact.id).then(function(groupCount){
			var group = Groups.findById(Contacts.filters.group);
			group.contacts_count = groupCount;
			$rootScope.$$phase || $rootScope.$apply();
		});
	}

	// redirect to export link
	$scope.export = function(){
		window.location = $scope.getExportUrl();
	}

	// generate an export link
	$scope.getExportUrl = function(){
		var filters = angular.copy($scope.contacts.filters);
		delete filters.page;
		var urlParams = jQuery.param(filters);
		return CrmApp.baseUrl + '/crm/index/export?'+urlParams;
	}
	
	/**
	 * Determines if a the current filter is in a group/tag
	 * Used to display contact list group functions like a close / delete button to remove this contact
	 * from the group
	 */
	$scope.isUserDefinedGroup = function() {
		var ret = (Contacts.filters.group != 'all' 
				&& Contacts.filters.group != 'contacts'
				&& !_.isUndefined(Contacts.filters.group)
				&& Contacts.filters.group != 'companies' 
				&& Contacts.filters.group != '');
		return ret;
	}
	
	$scope.getGroup = function(groupId){
		var g = $scope.groups.findById(groupId);
		return g;
	}
	
	$scope.addContact = function(){
		var contact = new Contacts.Contact({});
		contact.$save(function(){
			Contacts.collection.addAsFirst(contact);
			$location.path('edit/'+contact.id);
		});
	}
	
	// click delete filter button
	// to remove the current selected filter
	// remove the specifed filter key
	$scope.removeFilter = function(key){
		delete $scope.contacts.filters[key];
		// if we clear the name search remove the query text
		if (key == 'name') {
			$scope.query = '';
		}
		$scope.contacts.find($scope.contacts.filters, true);
		$location.path('/');
	}
	
	$scope.isCurrent = function(contact){
		if (Contacts.current == null)
			return false;
		if (Contacts.current.id == contact.id) {
			return true;
//			console.log(Contacts.collection.get(contact.id), 'iscurrent result of get');
//			// this code is gross.... We are relinking here.
//			Contacts.current = Contacts.collection.get(contact.id);
//			return true;
		}
		return false;
	}
	
}