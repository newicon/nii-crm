// list controller
CrmApp.phoneListCtrl = function($scope, $state, Status, Groups, Contacts, $location, $rootScope) {
	$scope.groups = Groups;
	$scope.contacts = Contacts;
	$scope.query;
	
	$scope.state = $state;
	
	$scope.contacts.collection.removeAll();
	$scope.contacts.find({search_options:{'page_size':1000}}, true);
	
	$scope.searchy = function(query) {
		$scope.contacts.find({search_options:{'page_size':1000}, name:query}, true);
	};

	$scope.showNumbers = function(contact) {
		var contactId = contact.id;
		$('.contact-link:not([data-id='+contactId+'])').removeClass('active');
		$('.phone-numbers').addClass('hide');
		// $(this).addClass('active');
		$('#phoneNumbers'+contactId).removeClass('hide');
	};

	$scope.callNumber = function(phone) {
		var number = phone.number;
		if (!confirm('Are you sure you want to call '+number+'?'))
			return false;
		var host = CrmApp.phone.host;
		var endpoint = CrmApp.phone.endpoint;
		var deviceName = CrmApp.phone.deviceName;
		var url = host+'command.htm?number=' + number +'&outgoing_uri='+endpoint;
		if (number.substr(0,2) == '44') {
			var phoneNumber = number.substr(2, number.length);
			window.location = CrmApp.baseUrl + '/call/index/webrtc?cc=44&number='+phoneNumber;
		} else {
			alert('Non UK number.  At the moment this only supports UK numbers starting with 44... Its really easy to do though if you fancy updating me :-)');
		}
		// $.get(url, function(){
		// 	alert('Calling '+number+' from '+ '\''+deviceName+'\'')
		// });
	};
	
	$scope.filter = '';
	$scope.$watch('contacts.filters', function(curFilter){
		// preserve original
		var filters = angular.copy(curFilter);
		// to debug
		$scope.filter = filters;
		delete filters.page;
		if (_.isEmpty(filters)){
			$scope.filter = '';
		}
		if (_.has(filters, 'group')){
			// look up group name
			if (filters.group == 'companies') {
				$scope.filter = 'Organisations';
			} else if (filters.group == 'contacts') {
				$scope.filter = 'People';
			} else {
				var g = Groups.get(filters.group);
				$scope.filter = g.name;
			}
		}
		if (_.has(filters, 'name')){
			$scope.filter = filters.name;
		}
	});
	
	// click delete filter button
	// to remove the current selected filter
	$scope.removeFilter = function(){
		$scope.contacts.find({}, true);
		$scope.query = '';
		$location.path('/');
	};
	
};