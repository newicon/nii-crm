angular.module('CrmApp')

.directive('contactDeal', function(baseUrl, assetUrl, $http, Contacts, userId, $timeout, $stateParams, DealService){
	return {
		restrict: 'E',
        templateUrl: assetUrl + '/deals/deal.html',
        replace: true,
		scope:true,
		scope:{},
		link:function($scope, $element, $attrs){
			$scope.deal = {};
			$scope.DealService = DealService;
			$scope.showDealForm = false;
			DealService.reset();
			DealService.getDealsFor($stateParams.contactId);
//			DealService.fetch({params:{});
			//$scope.deals = Contacts.current.deals;
			// we have to use statePArams as the Contacts.current.id is not yet set (this is ran whilst its still ajaxing)
			$scope.addDeal = function(){
				$scope.showDealForm = true;
				$scope.reset();
				$timeout(function(){angular.element('#deal_name').trigger('focus');},50);
			};
			$scope.save = function(){
				$scope.deal.contact_id = Contacts.current.id;
				$scope.saving = true;
				$http.post(baseUrl+'/crm/api/deal', $scope.deal).success(function(response){
					DealService.addOne(response);
				}).finally(function(){$scope.saving = false;});
				$scope.close();
			};
			$scope.close = function(){
				$scope.showDealForm = false;
				$scope.reset();
			};
			$scope.reset = function(){
				$scope.deal = {
					status: 'PENDING_10',
					month_count: 0,
					multi_month: 0,
					owner_id: userId,
					close_date: moment().format('YYYY-MM-DD HH-mm-dd')
				};
			};
			$scope.reset();
		}
	};
})

.filter('stageStatusLabel', function(){
	var statusLabels = {
		'PENDING_10' : '10%',
		'PENDING_25' : '25%',
		'PENDING_50' : '50%',
		'PENDING_75' : '75%',
		'PENDING_90' : '90%',
		'WON'		 : 'WON',
		'LOST'		 : 'LOST',
		'DORMANT'	 : 'DORMANT'
	};
	return function(input) {
		return statusLabels[input];
	};
});