CrmApp.directive('contactNotes', function(assetUrl, $http, Contacts, baseUrl, $stateParams, $timeout){
	return {
		restrict: 'E',
        templateUrl: assetUrl + '/notes/notes.html',
        replace: true,
		scope:{},
		link:function($scope, $element, $attrs){
			console.log($scope, '$scope Notes');
			$scope.showAddNoteForm = false;
			$scope.note = '';
			$scope.data = {};
			$scope.data.notes = [];
			// we have to use statePArams as the Contacts.current.id is not yet set (this is ran whilst its still ajaxing)
			if ($stateParams.contactId) {
				$http.get(baseUrl+'/crm/api/note/for/contact/'+$stateParams.contactId).success(function(response){
					$scope.data.notes = response;
				});
			}
			$scope.cancel = function(){
				$scope.showAddNoteForm = false;
			};
			$scope.addNote = function(){
				$scope.showAddNoteForm = true;
				$timeout(function(){angular.element('#contact-note-textbox').trigger('focus');},50);
			};
			$scope.save = function(){
				$http.post(baseUrl + '/crm/api/note', {
					note:$scope.note,
					contact_id:Contacts.current.id
				}).success(function(response){
					$scope.note = '';
					$scope.showAddNoteForm = false;
					$scope.data.notes.unshift(response);
				});
			};
		}
	};
});