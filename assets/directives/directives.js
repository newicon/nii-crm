CrmApp.directive('whenScrolledDown', function() {
		return function(scope, elm, attr) {
			var raw = elm[0];
			elm.bind('scroll', function() {
				if (raw.scrollTop + raw.offsetHeight >= raw.scrollHeight-10) {
					scope.$apply(attr.whenScrolledDown);
				}
			});
		};
	});
	
CrmApp.directive('whenScrolledUp', function() {
	return function(scope, elm, attr) {
		var raw = elm[0];
		elm.bind('scroll', function() {
			if (raw.scrollTop == 0) {
				scope.$apply(attr.whenScrolledUp);
			}
		});
	};
});

	
CrmApp.directive('draggable', function(Contacts) {
	return {
		// A = attribute, E = Element, C = Class and M = HTML Comment
		restrict: 'A',
		//The link function is responsible for registering DOM listeners as well as updating the DOM.
		link: function(scope, element, attrs) {
			var contactId = attrs.draggable;
			var selectedOnDrag = false;
			element.draggable({
				revert: 'invalid',
				zIndex: 100,
				// you have to drag the element 10 pixels before dragging starts
				distance:10,
				// dragging starts after 200ms
				delay:50,
				cursorAt: {left: 0, top: 15},
				start: function(event, ui) {
					// dragging has started add class to indicate all the draggables that are being dragged
					// this refers to the element being dragged (not the helper the actual element)
					$('.crm-list').find('.checked').addClass('dragging');
				},
				helper: function() {
					selectedOnDrag = false;		
					if (!Contacts.isChecked(contactId)){
						// the item was not selected before starting drag, so when retuning from an unsuccessful drag
						// event we want to unselect.
						selectedOnDrag = true;
						Contacts.check(contactId);
						scope.$apply();
					}

					var count = Contacts.countChecked();
					// select a nice image
					// be very cool to use actual images...
					var img = 'drag-contact-5';
					if (count > 0 && count < 6) {
						img = 'drag-contact-' + count;
					}
					var helperHtml = '<div class="dragHelper"><img class="img" src="'+CrmApp.assetsUrl+'/styles/images/' + img + '.png" />';
					helperHtml += '<span class="badge badge-important">' + count + '</span> contacts</div>';
					return $(helperHtml).appendTo('body').get()
				},
				stop: function() {
					if (selectedOnDrag)
						Contacts.uncheck(contactId);
					$('.crm-list').find('.dragging').removeClass('dragging');
					scope.$apply();
				}
			});
		}
	};
});
	
CrmApp.directive('droppable', function(Contacts, Groups) {
	return {
		restrict: 'A',
		link: function(scope, element, attrs) {
			element.droppable({
				greedy: true,
				accept: ".contact-list-item",
				hoverClass: "drop-hover",
				drop: function(event, ui) {
					var group = Groups.findById(attrs.droppable);
					var contacts = Contacts.getChecked();
					// update the contact pre-emptively
					_.each(contacts, function(cId){
						var c = Contacts.findById(cId);
						if (angular.isUndefined(c))
							return false;
						if (_.isUndefined(_.find(c.groups, function(g){return g.id == group.id})))
							c.groups.push(group);
					});
					// add the checked contacts to the group
					Groups.addContacts(group.id, contacts, function(data) {
						if (data !== false)
							group.contacts_count = data.contacts_count;
					});
					Contacts.uncheckAll()
				}
			});
		}
	}
});

CrmApp.directive('droppableState', function(Contacts, Status, $http) {
	return {
		restrict: 'A',
		link: function(scope, element, attrs) {
			element.droppable({
				greedy: true,
				hoverClass: "drop-hover",
				
				drop: function(event, ui) {
					var status = Status.get(attrs.droppableState);
					var contacts = Contacts.getChecked();
					// update the contact pre-emptively
					_.each(contacts, function(cId){
						var c = Contacts.findById(cId);
						c.status_id = status.id;
						Contacts.save(c);
					});
					var config = {url: CrmApp.url('crm/api/contact', {status_id:status.id}), method:'GET', headers:{"X-Requested-With":"XMLHttpRequest"}};
					$http(config).success(function(data){
						status.count = data.pagination.item_count;
					});
					// add the checked contacts to the group
					Contacts.uncheckAll();
				}
			});
		}
	}
});
/**
 * Todo: Pass in the sortable config via attribute.
 * Todo: Pass in additional stop function to call in the controller (to save)
 * usage: 
 * ~~~
 * <div ni-sortable ni-model-name="group" ni-model-order-field="order">
 *		<div ng-repeat="group in groups.array">{{group.order}} - {{group.name}}</div>
 * </div>
 * ~~~
 */
CrmApp.directive('niSortable', function(Groups, baseUrl, $http) {
	return {
		restrict: 'A',
		scope:{
			modelName:'@niModelName',
			modelOrderField:'@niModelOrderField'
		},
		link: function($scope, $element, attrs) {
			var modelName = $scope.modelName;
			var modelOrderField = $scope.modelOrderField;
			$element.sortable({
				revert: 10,
				// you have to drag the element 10 pixels before dragging starts
				distance:10,
				// dragging starts after 50ms
				delay:50,
				axis: "y",
				stop: function() {
					angular.forEach($element.children(), function(child, index){
						// Now, extract all of the ngRepeat items out
                        // of the dom using the scope() plugin.
                        var ngRepeatScope = $(child).scope();
						if (angular.isDefined(ngRepeatScope[modelName])) {
							ngRepeatScope[modelName][modelOrderField] = index;
						}
					});
					$http.post(baseUrl+'/crm/api/groups', Groups.array)
				}
			});
			
		}
	};
});

CrmApp.directive('letter', function(Contacts) {
	return {
		restrict: 'A',
		link: function(scope, element, attrs) {
			attrs.$observe('contactName', function(value) {
				var letter = Contacts.getFirstLetter(attrs.contactName);
				if (letter != this.lastLetter || attrs.index == 0) {
					element.html(letter).show();
				} else {
					element.remove();
				}
				this.lastLetter = letter;
			});
		}
	}
});

		
CrmApp.directive('letterScroll', function(Contacts, $rootScope) {
	return {
		restrict: 'A',
		link: function(scope, element, attrs) {
			$fixedHeader = element;
			var updateLetter = function (){
				var hits = $fixedHeader.collision('.crm-list li');
				if (hits.length >= 1) {
					var elementScope = $(hits[0]).scope();
					var name = _.isUndefined(elementScope.contact) ? '?' : elementScope.contact.name;
					var letter = Contacts.getFirstLetter(name);
					$fixedHeader.html(letter);
				}
			}
			// scroll stop jquery event extension
			$.fn.scrollStopped = function(callback) {
				$(this).scroll(function(){
					var self = this, $this = $(self);
					if ($this.data('scrollTimeout')) {
					  clearTimeout($this.data('scrollTimeout'));
					}
					$this.data('scrollTimeout', setTimeout(callback,100,self));
				});
			};

			// watch first item in the list
			attrs.$observe('watchFirstLi', function(value) {
				updateLetter();
			})
			var throttle = false;
			element.parent().scroll(function(){
				// throttle this function as collision detection is fairly expensive
				if (!throttle) {
					updateLetter();
					throttle = true;
					setTimeout(function(){throttle=false},100);
				}
				// fire event when scroll stops
				var self = this, $this = $(self);
				if ($this.data('scrollTimeout')) {
				  clearTimeout($this.data('scrollTimeout'));
				}
				$this.data('scrollTimeout', setTimeout(updateLetter(),250,self));
			})
			.scrollStopped(function(){updateLetter();});
		}
	}
});


CrmApp.directive('focusLastOnClick', function(){
	return function(scope, element, attrs) {
		$(element).click(function(){
			scope.$observe(attrs.focusLastOnClick, function(value) {
				var el = $(value).last();
				$timeout(function() {
						el[0].focus();
				});
			})
		});
	}
})

//
// <due date=""></due>
// <span class="label label-important pull-right">{{getDueDate(contact.action)}}</span>
//
CrmApp.directive('contactActionDue', function (Actions) {
    return {
		scope:{contact:'=contact'},
        restrict:'E',
		link:function($scope, element){
			$scope.dueLabel = Actions.dueLabel($scope.contact.action);
			$scope.color = Actions.dueColor($scope.contact.action);
		},
        template:'<span class="label label-important pull-right" ng-style="{backgroundColor:color}">' +
            '{{dueLabel}}' +
			'</span>',
        replace:true
    };
});


CrmApp.filter('nl2br', function() {
    return function(input) {
		var breakTag = '<br />';
		return (input + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
	};
});

CrmApp.directive('datey', function () {
    return {
        restrict:'A',
		require:'ngModel',
		link:function($scope, $element, $attrs, ngModelCtrl){
			var juiViewFormat = 'dd/mm/yy';
			var momentViewFormat = 'DD/MM/YYYY';
			var momentDbFormat = 'YYYY-MM-DD HH:mm:ss';
			var minDate = moment().subtract(1, 'day').toDate();
			$element.datepicker({dateFormat:juiViewFormat, minDate:minDate});
			// coverts a model value into a view value
			var modelToView = function(modelValue){
				// the model value will be a mysql date format
				if (ngModelCtrl.$isEmpty(modelValue)) {
					modelValue = moment().format(momentDbFormat);
				}
				var displayValue = moment(modelValue, momentDbFormat).format(momentViewFormat);
				return displayValue
			};
			// coverts a view value into a model value
			var viewToModel = function(viewValue){
				var modelValue = moment(viewValue, momentViewFormat).format(momentDbFormat);
				return modelValue;
			};
			// coverts a model value into a view value (add to angular)
			ngModelCtrl.$formatters.push(modelToView);
			// coverts a view value into a model value (add to angular)
			ngModelCtrl.$parsers.push(viewToModel);
		}
    };
});

CrmApp.filter('trustedUrl', function($sce) {
    return function(url) {
        return $sce.trustAsResourceUrl(url);
    };
});


CrmApp.directive('audios', function($sce) {
	return {
		restrict: 'A',
		scope: { code:'=' },
		replace: true,
		template: '<audio ng-src="{{url}}" controls></audio>',
		link: function (scope) {
			scope.$watch('load', function (newVal, oldVal) {
				if (newVal !== undefined) {
					scope.url = $sce.trustAsResourceUrl(newVal);
				}
			});
		}
	};
});

CrmApp.directive('html5vfix', function() {
    return {
        restrict: 'A',
        link: function(scope, element, attr) {
            attr.$set('src', attr.vsrc);
        }
    }
});


// <call-log></call-log>
CrmApp.directive('callLog', function (Contacts, $http, $stateParams, $sce) {
    return {
        restrict:'E',
		link: function($scope, element){
			$scope.data = {calls:[]};
			
			Contacts.getContact($stateParams.contactId).then(function(contact){
				console.log('current contact', contact.phones);
				console.log('current contact', contact.phones.legnth);
				if (contact.phones) {
				
					var numbers = [];
					_.each(contact.phones, function(phone){
						numbers.push(phone.number);
					})
					
					var config = {url: CrmApp.url('call/api/calls'), data:{numbers:numbers}, method:'POST', headers:{"X-Requested-With":"XMLHttpRequest"}};
					$http(config).success(function(data) {
						_.each(data.data, function(call) {
							console.log('trust', call.url)
							$sce.trustAsResourceUrl(call.url);
						});
						$scope.data.calls = data.data;
					});
				}
			});
			
		},
        template:`
		<div ng-repeat="call in data.calls">
			{{call.created_at}} - {{call.direction}} -
			{{call.from}} - {{call.to}}
			<div ng-if="call.url">
				<audio  html5vfix vsrc="{{call.url}}" controls></audio>
			</div>
			<div ng-if="!call.url">
				{{call.direction}} - NO recordong
			</div>
		</div>
		`,
        replace:true
    };
});