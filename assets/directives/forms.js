CrmApp.directive('labelSelect', function() {
	// return the directive link function. (compile function not needed)
	return {
		require: '?ngModel',
		link:function(scope, element, attrs, ngModel) {
			attrs.$observe('labelSelect', function(val) {
				var data = angular.fromJson(val);
				element.select2({
					//multiple: true,
					createSearchChoice:function(term, data) { 
						if ($(data).filter(function() { 
							return this.text.localeCompare(term)===0;
						}).length===0) {return {id:term,text:term};} 
					},
					placeholder:'Type',
					data:data,
					allowClear: true,
					maximumSelectionSize:1,
					formatResult:function(item){
						return item.text;
					}
				}).on("change", function(e) {
					scope.$apply(ngModel.$setViewValue(e.val));
				});
			});
		}
	}
});


// options defined via attributes:
// group = 'all,contacts,companies,[id]'
// placeholder
// canCreateNew
// NOTE!: make sure all fields have a value="" attribute otherwise the initSelection never gets called!
CrmApp.directive('contactSelect', function(Contacts, $rootScope, $timeout) {
	var img = CrmApp.assetsUrl + '/styles/images/company-add-24.png';
	return {
		require: '?ngModel',
		scope: true,
		link: function(scope, element, attrs, ngModel) {
			
			if(!ngModel) return; // do nothing if no ng-model
			
			attrs.group = attrs.group || '';
			attrs.placeholder = attrs.placeholder || 'Select contact';
			attrs.canCreateNew = attrs.canCreateNew || false;

			var model = element.attr('ng-model');
			$('input',element).attr('ng-model', model);

			// store the initSelection callback
			var initSelectionCallback;
			
			ngModel.$render = function() {
				if (!_.isUndefined(ngModel.$viewValue)) {
					// look up company
					if (!_.isNull(scope.contact.company) && !_.isUndefined(scope.contact.company.name)){
						initSelectionCallback({id:0, text:scope.contact.company.name, img:scope.contact.company.image_url_24})
					} else {
						$(element).val('');
						initSelectionCallback([]);
					}
				}
			}
			
			element.select2({
				createSearchChoice:function(term, data) {
					if (attrs.canCreateNew) {
						var filter = $(data).filter(function() {
							return this.text.toLowerCase().localeCompare(term)===0;
						});
						if (filter.length===0) {
							return {img:img,id:term, text:term, isNew:true};
						}
					}
				},
				placeholder:attrs.placeholder,
				//multiple: true,
				allowClear: true,
				maximumSelectionSize:1,
				quietMillis: 50,
				ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
					url: CrmApp.url('crm/widget/ddFind'),
					dataType: 'json',
					data: function (term, page) {
						return {
							name: term, // search term
							group: attrs.group,
							page_limit: 30,
							page: page
						};
					},
					results: function (data, page) { 
						var more = (page * 30) < data.pagination.item_count
						// parse the results into the format expected by Select2.
						// since we are using custom formatting functions we do not need to alter remote JSON data
						return {results: data.results, more:more};
					}
				},
				initSelection: function(element, callback) {
					initSelectionCallback = callback
				},
				formatResult:function(item){
					var itemImg = item.img;
					if (item.isNew)
						itemImg = img
					
					
					return '<img width="24" src="'+itemImg+'"/> '+item.text;
				},
				formatSelection:function(item){
					
					if (_.isEmpty(item)){
						return '<img width="24" src="http://placehold.it/24x24"/> '+ 'unknown';
					}
					
					var selectedCompany = {name:item.text, type:'COMPANY'};
					
					if (item.isNew) {
						scope.contact.company = selectedCompany;
						Contacts.save(selectedCompany).success(function(savedCompany){
							// add new company contact to the collection (needs to add in correct place alphabeticaly)
							Contacts.collection.add(savedCompany);
														
							// this line of code should be sorted by the server
							savedCompany.staff.push(scope.contact);
							
							// update the contact with the new company id
							scope.contact.company_id = savedCompany.id;
							Contacts.save(scope.contact).success(function(contactServer){
								Contacts.collection.update(contactServer)
							})
							
							ngModel.$setViewValue(savedCompany.id);
							$rootScope.$$phase || $rootScope.$apply();
						})
					}
					
					return '<img width="24" src="'+item.img+'"/> '+item.text;
				},
				escapeMarkup: function(m) { return m; }
			}).on("change", function(e) {
				// when a contact is added we must update the 
				// contacts - company attribute and company_id
				// companies - staff array with the contact
				
				scope.$apply(ngModel.$setViewValue(e.val));
				// if we added a new item (is undefined if we remove a company)
				if (!_.isUndefined(e.added)){
					scope.contact.company = {
						id:e.added.id,
						name:e.added.text
					}
				} else {
					// remove the company
					scope.contact.company = {}
				}
				
				Contacts.save(scope.contact)
				$rootScope.$$phase || $rootScope.$apply();
				// lookup the company, add staff array
				
			});
		}
	}
})
	
CrmApp.directive('datepicker', function() {
	return {
		require: 'ngModel',
		link: function(scope, el, attr, ngModel) {
			$(el).datepicker({
				firstDay:1,
				dateFormat: attr.dpDateFormat,
				onSelect: function(dateText) {
				scope.$apply(function() {
					ngModel.$setViewValue(dateText);
				});
			}
		});
		}
	};
});

// focus an element if the attribute value of focus-me-if attribute is true
CrmApp.directive('focusMeIf', function($timeout) {
	return {
		link: function(scope, element, attrs) {
			scope.$watch(attrs.focusMeIf, function(value) {
				if(value === true) { 
					$timeout(function() {
						element[0].focus(); 
					});
				}
			});
		}
	};
});

// Credit for ngBlur and ngFocus to https://github.com/addyosmani/todomvc/blob/master/architecture-examples/angularjs/js/directives/
// call a function when an input is blurred
// ~~~
// <input ng-blur="save()" />
// ~~~
CrmApp.directive('ngBlur', function() {
	return function(scope, elem, attrs) {
		elem.bind('blur', function() {
			scope.$apply(attrs.ngBlur);
		});
	};
});

// run a function on focus
// 
CrmApp.directive('ngFocus', function() {
	return function(scope, elem, attrs) {
		elem.bind('focus', function() {
			scope.$apply(attrs.ngFocus);
		});
	};
});


// country data has been hard coded in
// see module Crm/models/data/countries.php
// PHP model that generates the data:: CrmAddress::getCountryArrayDd()
CrmApp.directive('country', function() {
	return {
		require: '?ngModel',
		link: function(scope, element, attrs, ngModel) {
			attrs.$observe('country', function(val) {
				$(element).select2({
					//multiple: true,
					formatResult: function(item){
						return '<img class="flag flag-'+item.id.toLowerCase()+'" alt="'+item.id+'" /> '+item.text;
					},
					formatSelection: function(item){
						if (_.isUndefined(item.id)) return '';
						return '<img class="flag flag-'+item.id.toLowerCase()+'" alt="'+item.id+'" /> '+item.text;
					},
					escapeMarkup:function(m){
						return m;
					},
					data:[{"id":"AF","text":"Afghanistan"},{"id":"AL","text":"Albania"},{"id":"DZ","text":"Algeria"},{"id":"AS","text":"American Samoa"},{"id":"AD","text":"Andorra"},{"id":"AO","text":"Angola"},{"id":"AI","text":"Anguilla"},{"id":"AQ","text":"Antarctica"},{"id":"AG","text":"Antigua And Barbuda"},{"id":"AR","text":"Argentina"},{"id":"AM","text":"Armenia"},{"id":"AW","text":"Aruba"},{"id":"AU","text":"Australia"},{"id":"AT","text":"Austria"},{"id":"AZ","text":"Azerbaijan"},{"id":"BS","text":"Bahamas"},{"id":"BH","text":"Bahrain"},{"id":"BD","text":"Bangladesh"},{"id":"BB","text":"Barbados"},{"id":"BY","text":"Belarus"},{"id":"BE","text":"Belgium"},{"id":"BZ","text":"Belize"},{"id":"BJ","text":"Benin"},{"id":"BM","text":"Bermuda"},{"id":"BT","text":"Bhutan"},{"id":"BO","text":"Bolivia"},{"id":"BA","text":"Bosnia And Herzegovina"},{"id":"BW","text":"Botswana"},{"id":"BV","text":"Bouvet Island"},{"id":"BR","text":"Brazil"},{"id":"IO","text":"British Indian Ocean Territory"},{"id":"BN","text":"Brunei"},{"id":"BG","text":"Bulgaria"},{"id":"BF","text":"Burkina Faso"},{"id":"BI","text":"Burundi"},{"id":"KH","text":"Cambodia"},{"id":"CM","text":"Cameroon"},{"id":"CA","text":"Canada"},{"id":"CV","text":"Cape Verde"},{"id":"KY","text":"Cayman Islands"},{"id":"CF","text":"Central African Republic"},{"id":"TD","text":"Chad"},{"id":"CL","text":"Chile"},{"id":"CN","text":"China"},{"id":"CX","text":"Christmas Island"},{"id":"CC","text":"Cocos (Keeling) Islands"},{"id":"CO","text":"Columbia"},{"id":"KM","text":"Comoros"},{"id":"CG","text":"Congo"},{"id":"CK","text":"Cook Islands"},{"id":"CR","text":"Costa Rica"},{"id":"CI","text":"Cote D'Ivorie (Ivory Coast)"},{"id":"HR","text":"Croatia (Hrvatska)"},{"id":"CU","text":"Cuba"},{"id":"CY","text":"Cyprus"},{"id":"CZ","text":"Czech Republic"},{"id":"CD","text":"Democratic Republic Of Congo (Zaire)"},{"id":"DK","text":"Denmark"},{"id":"DJ","text":"Djibouti"},{"id":"DM","text":"Dominica"},{"id":"DO","text":"Dominican Republic"},{"id":"TP","text":"East Timor"},{"id":"EC","text":"Ecuador"},{"id":"EG","text":"Egypt"},{"id":"SV","text":"El Salvador"},{"id":"GQ","text":"Equatorial Guinea"},{"id":"ER","text":"Eritrea"},{"id":"EE","text":"Estonia"},{"id":"ET","text":"Ethiopia"},{"id":"FK","text":"Falkland Islands (Malvinas)"},{"id":"FO","text":"Faroe Islands"},{"id":"FJ","text":"Fiji"},{"id":"FI","text":"Finland"},{"id":"FR","text":"France"},{"id":"FX","text":"France, Metropolitan"},{"id":"GF","text":"French Guinea"},{"id":"PF","text":"French Polynesia"},{"id":"TF","text":"French Southern Territories"},{"id":"GA","text":"Gabon"},{"id":"GM","text":"Gambia"},{"id":"GE","text":"Georgia"},{"id":"DE","text":"Germany"},{"id":"GH","text":"Ghana"},{"id":"GI","text":"Gibraltar"},{"id":"GR","text":"Greece"},{"id":"GL","text":"Greenland"},{"id":"GD","text":"Grenada"},{"id":"GP","text":"Guadeloupe"},{"id":"GU","text":"Guam"},{"id":"GT","text":"Guatemala"},{"id":"GN","text":"Guinea"},{"id":"GW","text":"Guinea-Bissau"},{"id":"GY","text":"Guyana"},{"id":"HT","text":"Haiti"},{"id":"HM","text":"Heard And McDonald Islands"},{"id":"HN","text":"Honduras"},{"id":"HK","text":"Hong Kong"},{"id":"HU","text":"Hungary"},{"id":"IS","text":"Iceland"},{"id":"IN","text":"India"},{"id":"ID","text":"Indonesia"},{"id":"IR","text":"Iran"},{"id":"IQ","text":"Iraq"},{"id":"IE","text":"Ireland"},{"id":"IL","text":"Israel"},{"id":"IT","text":"Italy"},{"id":"JM","text":"Jamaica"},{"id":"JP","text":"Japan"},{"id":"JO","text":"Jordan"},{"id":"KZ","text":"Kazakhstan"},{"id":"KE","text":"Kenya"},{"id":"KI","text":"Kiribati"},{"id":"KW","text":"Kuwait"},{"id":"KG","text":"Kyrgyzstan"},{"id":"LA","text":"Laos"},{"id":"LV","text":"Latvia"},{"id":"LB","text":"Lebanon"},{"id":"LS","text":"Lesotho"},{"id":"LR","text":"Liberia"},{"id":"LY","text":"Libya"},{"id":"LI","text":"Liechtenstein"},{"id":"LT","text":"Lithuania"},{"id":"LU","text":"Luxembourg"},{"id":"MO","text":"Macau"},{"id":"MK","text":"Macedonia"},{"id":"MG","text":"Madagascar"},{"id":"MW","text":"Malawi"},{"id":"MY","text":"Malaysia"},{"id":"MV","text":"Maldives"},{"id":"ML","text":"Mali"},{"id":"MT","text":"Malta"},{"id":"MH","text":"Marshall Islands"},{"id":"MQ","text":"Martinique"},{"id":"MR","text":"Mauritania"},{"id":"MU","text":"Mauritius"},{"id":"YT","text":"Mayotte"},{"id":"MX","text":"Mexico"},{"id":"FM","text":"Micronesia"},{"id":"MD","text":"Moldova"},{"id":"MC","text":"Monaco"},{"id":"MN","text":"Mongolia"},{"id":"MS","text":"Montserrat"},{"id":"MA","text":"Morocco"},{"id":"MZ","text":"Mozambique"},{"id":"MM","text":"Myanmar (Burma)"},{"id":"NA","text":"Namibia"},{"id":"NR","text":"Nauru"},{"id":"NP","text":"Nepal"},{"id":"NL","text":"Netherlands"},{"id":"AN","text":"Netherlands Antilles"},{"id":"NC","text":"New Caledonia"},{"id":"NZ","text":"New Zealand"},{"id":"NI","text":"Nicaragua"},{"id":"NE","text":"Niger"},{"id":"NG","text":"Nigeria"},{"id":"NU","text":"Niue"},{"id":"NF","text":"Norfolk Island"},{"id":"KP","text":"North Korea"},{"id":"MP","text":"Northern Mariana Islands"},{"id":"NO","text":"Norway"},{"id":"OM","text":"Oman"},{"id":"PK","text":"Pakistan"},{"id":"PW","text":"Palau"},{"id":"PA","text":"Panama"},{"id":"PG","text":"Papua New Guinea"},{"id":"PY","text":"Paraguay"},{"id":"PE","text":"Peru"},{"id":"PH","text":"Philippines"},{"id":"PN","text":"Pitcairn"},{"id":"PL","text":"Poland"},{"id":"PT","text":"Portugal"},{"id":"PR","text":"Puerto Rico"},{"id":"QA","text":"Qatar"},{"id":"RE","text":"Reunion"},{"id":"RO","text":"Romania"},{"id":"RU","text":"Russia"},{"id":"RW","text":"Rwanda"},{"id":"SH","text":"Saint Helena"},{"id":"KN","text":"Saint Kitts And Nevis"},{"id":"LC","text":"Saint Lucia"},{"id":"PM","text":"Saint Pierre And Miquelon"},{"id":"VC","text":"Saint Vincent And The Grenadines"},{"id":"SM","text":"San Marino"},{"id":"ST","text":"Sao Tome And Principe"},{"id":"SA","text":"Saudi Arabia"},{"id":"SN","text":"Senegal"},{"id":"SC","text":"Seychelles"},{"id":"SL","text":"Sierra Leone"},{"id":"SG","text":"Singapore"},{"id":"SK","text":"Slovak Republic"},{"id":"SI","text":"Slovenia"},{"id":"SB","text":"Solomon Islands"},{"id":"SO","text":"Somalia"},{"id":"ZA","text":"South Africa"},{"id":"GS","text":"South Georgia And South Sandwich Islands"},{"id":"KR","text":"South Korea"},{"id":"ES","text":"Spain"},{"id":"LK","text":"Sri Lanka"},{"id":"SD","text":"Sudan"},{"id":"SR","text":"Suriname"},{"id":"SJ","text":"Svalbard And Jan Mayen"},{"id":"SZ","text":"Swaziland"},{"id":"SE","text":"Sweden"},{"id":"CH","text":"Switzerland"},{"id":"SY","text":"Syria"},{"id":"TW","text":"Taiwan"},{"id":"TJ","text":"Tajikistan"},{"id":"TZ","text":"Tanzania"},{"id":"TH","text":"Thailand"},{"id":"TG","text":"Togo"},{"id":"TK","text":"Tokelau"},{"id":"TO","text":"Tonga"},{"id":"TT","text":"Trinidad And Tobago"},{"id":"TN","text":"Tunisia"},{"id":"TR","text":"Turkey"},{"id":"TM","text":"Turkmenistan"},{"id":"TC","text":"Turks And Caicos Islands"},{"id":"TV","text":"Tuvalu"},{"id":"UG","text":"Uganda"},{"id":"UA","text":"Ukraine"},{"id":"AE","text":"United Arab Emirates"},{"id":"GB","text":"United Kingdom"},{"id":"US","text":"United States"},{"id":"UM","text":"United States Minor Outlying Islands"},{"id":"UY","text":"Uruguay"},{"id":"UZ","text":"Uzbekistan"},{"id":"VU","text":"Vanuatu"},{"id":"VA","text":"Vatican City (Holy See)"},{"id":"VE","text":"Venezuela"},{"id":"VN","text":"Vietnam"},{"id":"VG","text":"Virgin Islands (British)"},{"id":"VI","text":"Virgin Islands (US)"},{"id":"WF","text":"Wallis And Futuna Islands"},{"id":"EH","text":"Western Sahara"},{"id":"WS","text":"Western Samoa"},{"id":"YE","text":"Yemen"},{"id":"YU","text":"Yugoslavia"},{"id":"ZM","text":"Zambia"},{"id":"ZW","text":"Zimbabwe"}],
					placeholder:'Country',
					allowClear: true,
					maximumSelectionSize:1,
					dropdownAutoWidth:true
				}).on("change", function(e) {
					scope.$apply(ngModel.$setViewValue(e.val));
				});
			});
		}
	}
});

// country tel data has been hard coded in
// see module Crm/models/data/countries-tel.php
// PHP model that generates the data:: CrmPhone::getCountryExtensionArray()
CrmApp.directive('countryTel', function() {
	// return the directive link function. (compile function not needed)
	return function(scope, element, attrs) {
		attrs.$observe('countryTel', function(val) {
			$(element).select2({
				//multiple: true,
				formatResult: function(item){
					return '<img class="flag flag-'+item.cc.toLowerCase()+'" alt="'+item.cc+'" /> '+item.text;
				},
				formatSelection: function(item){
					if (item.cc != undefined)
					return '<img class="flag flag-'+item.cc.toLowerCase()+'" alt="'+item.cc+'" />';
				},
				escapeMarkup:function(m){
					return m;
				},
				data:[{"cc":"AF","id":"+93","text":"Afghanistan +93"},{"cc":"AL","id":"+355","text":"Albania +355"},{"cc":"DZ","id":"+213","text":"Algeria +213"},{"cc":"AS","id":"+684","text":"American Samoa +684"},{"cc":"AD","id":"+376","text":"Andorra +376"},{"cc":"AO","id":"+244","text":"Angola +244"},{"cc":"AI","id":"+809","text":"Anguilla +809"},{"cc":"AG","id":"+268","text":"Antigua And Barbuda +268"},{"cc":"AR","id":"+54","text":"Argentina +54"},{"cc":"AM","id":"+374","text":"Armenia +374"},{"cc":"AW","id":"+297","text":"Aruba +297"},{"cc":"AU","id":"+61","text":"Australia +61"},{"cc":"AT","id":"+43","text":"Austria +43"},{"cc":"AZ","id":"+994","text":"Azerbaijan +994"},{"cc":"BS","id":"+1242","text":"Bahamas +1242"},{"cc":"BH","id":"+973","text":"Bahrain +973"},{"cc":"BD","id":"+880","text":"Bangladesh +880"},{"cc":"BB","id":"+1246","text":"Barbados +1246"},{"cc":"BY","id":"+375","text":"Belarus +375"},{"cc":"BE","id":"+32","text":"Belgium +32"},{"cc":"BZ","id":"+501","text":"Belize +501"},{"cc":"BJ","id":"+229","text":"Benin +229"},{"cc":"BM","id":"+1441","text":"Bermuda +1441"},{"cc":"BT","id":"+975","text":"Bhutan +975"},{"cc":"BO","id":"+591","text":"Bolivia +591"},{"cc":"BA","id":"+387","text":"Bosnia And Herzegovina +387"},{"cc":"BW","id":"+267","text":"Botswana +267"},{"cc":"BV","id":"+","text":"Bouvet Island +"},{"cc":"BR","id":"+55","text":"Brazil +55"},{"cc":"IO","id":"+284","text":"British Indian Ocean Territory +284"},{"cc":"BN","id":"+673","text":"Brunei +673"},{"cc":"BG","id":"+359","text":"Bulgaria +359"},{"cc":"BF","id":"+226","text":"Burkina Faso +226"},{"cc":"BI","id":"+257","text":"Burundi +257"},{"cc":"KH","id":"+855","text":"Cambodia +855"},{"cc":"CM","id":"+237","text":"Cameroon +237"},{"cc":"CA","id":"+1","text":"Canada +1"},{"cc":"CV","id":"+238","text":"Cape Verde +238"},{"cc":"KY","id":"+1345","text":"Cayman Islands +1345"},{"cc":"CF","id":"+236","text":"Central African Republic +236"},{"cc":"TD","id":"+235","text":"Chad +235"},{"cc":"CL","id":"+56","text":"Chile +56"},{"cc":"CN","id":"+86","text":"China +86"},{"cc":"CO","id":"+57","text":"Columbia +57"},{"cc":"KM","id":"+269","text":"Comoros +269"},{"cc":"CG","id":"+242","text":"Congo +242"},{"cc":"CK","id":"+682","text":"Cook Islands +682"},{"cc":"CR","id":"+506","text":"Costa Rica +506"},{"cc":"CI","id":"+225","text":"Cote D'Ivorie (Ivory Coast) +225"},{"cc":"HR","id":"+385","text":"Croatia (Hrvatska) +385"},{"cc":"CU","id":"+53","text":"Cuba +53"},{"cc":"CY","id":"+357","text":"Cyprus +357"},{"cc":"CZ","id":"+420","text":"Czech Republic +420"},{"cc":"CD","id":"+","text":"Democratic Republic Of Congo (Zaire) +"},{"cc":"DK","id":"+45","text":"Denmark +45"},{"cc":"DJ","id":"+253","text":"Djibouti +253"},{"cc":"DM","id":"+1767","text":"Dominica +1767"},{"cc":"DO","id":"+1809","text":"Dominican Republic +1809"},{"cc":"EC","id":"+593","text":"Ecuador +593"},{"cc":"EG","id":"+20","text":"Egypt +20"},{"cc":"SV","id":"+503","text":"El Salvador +503"},{"cc":"GQ","id":"+240","text":"Equatorial Guinea +240"},{"cc":"ER","id":"+291","text":"Eritrea +291"},{"cc":"EE","id":"+371","text":"Estonia +371"},{"cc":"ET","id":"+251","text":"Ethiopia +251"},{"cc":"FK","id":"+500","text":"Falkland Islands +500"},{"cc":"FO","id":"+298","text":"Faroe Islands +298"},{"cc":"FJ","id":"+679","text":"Fiji +679"},{"cc":"FI","id":"+358","text":"Finland +358"},{"cc":"FR","id":"+33","text":"France +33"},{"cc":"GF","id":"+594","text":"French Guinea +594"},{"cc":"PF","id":"+689","text":"French Polynesia +689"},{"cc":"GA","id":"+241","text":"Gabon +241"},{"cc":"GM","id":"+220","text":"Gambia +220"},{"cc":"GE","id":"+995","text":"Georgia +995"},{"cc":"DE","id":"+49","text":"Germany +49"},{"cc":"GH","id":"+233","text":"Ghana +233"},{"cc":"GI","id":"+350","text":"Gibraltar +350"},{"cc":"GR","id":"+30","text":"Greece +30"},{"cc":"GL","id":"+299","text":"Greenland +299"},{"cc":"GD","id":"+473","text":"Grenada +473"},{"cc":"GP","id":"+590","text":"Guadeloupe +590"},{"cc":"GU","id":"+1671","text":"Guam +1671"},{"cc":"GT","id":"+502","text":"Guatemala +502"},{"cc":"GN","id":"+224","text":"Guinea +224"},{"cc":"GW","id":"+245","text":"Guinea-Bissau +245"},{"cc":"GY","id":"+592","text":"Guyana +592"},{"cc":"HT","id":"+509","text":"Haiti +509"},{"cc":"HN","id":"+504","text":"Honduras +504"},{"cc":"HK","id":"+852","text":"Hong Kong +852"},{"cc":"HU","id":"+36","text":"Hungary +36"},{"cc":"IS","id":"+354","text":"Iceland +354"},{"cc":"IN","id":"+91","text":"India +91"},{"cc":"ID","id":"+62","text":"Indonesia +62"},{"cc":"IR","id":"+98","text":"Iran +98"},{"cc":"IQ","id":"+964","text":"Iraq +964"},{"cc":"IE","id":"+353","text":"Ireland +353"},{"cc":"IL","id":"+972","text":"Israel +972"},{"cc":"IT","id":"+39","text":"Italy +39"},{"cc":"JM","id":"+1876","text":"Jamaica +1876"},{"cc":"JP","id":"+81","text":"Japan +81"},{"cc":"JO","id":"+962","text":"Jordan +962"},{"cc":"KZ","id":"+7","text":"Kazakhstan +7"},{"cc":"KE","id":"+254","text":"Kenya +254"},{"cc":"KI","id":"+686","text":"Kiribati +686"},{"cc":"KP","id":"+850","text":"Korea North +850"},{"cc":"KR","id":"+82","text":"Korea South +82"},{"cc":"KW","id":"+965","text":"Kuwait +965"},{"cc":"KG","id":"+996","text":"Kyrgyzstan +996"},{"cc":"LA","id":"+856","text":"Laos +856"},{"cc":"LV","id":"+371","text":"Latvia +371"},{"cc":"LB","id":"+961","text":"Lebanon +961"},{"cc":"LS","id":"+266","text":"Lesotho +266"},{"cc":"LR","id":"+231","text":"Liberia +231"},{"cc":"LY","id":"+218","text":"Libya +218"},{"cc":"LI","id":"+423","text":"Liechtenstein +423"},{"cc":"LT","id":"+370","text":"Lithuania +370"},{"cc":"LU","id":"+352","text":"Luxembourg +352"},{"cc":"MO","id":"+853","text":"Macao +853"},{"cc":"MK","id":"+389","text":"Macedonia +389"},{"cc":"MG","id":"+261","text":"Madagascar +261"},{"cc":"MW","id":"+265","text":"Malawi +265"},{"cc":"MY","id":"+60","text":"Malaysia +60"},{"cc":"MV","id":"+960","text":"Maldives +960"},{"cc":"ML","id":"+223","text":"Mali +223"},{"cc":"MT","id":"+356","text":"Malta +356"},{"cc":"MH","id":"+692","text":"Marshall Islands +692"},{"cc":"MQ","id":"+596","text":"Martinique +596"},{"cc":"MR","id":"+222","text":"Mauritania +222"},{"cc":"MU","id":"+230","text":"Mauritius +230"},{"cc":"YT","id":"+269","text":"Mayotte +269"},{"cc":"MX","id":"+52","text":"Mexico +52"},{"cc":"FM","id":"+691","text":"Micronesia +691"},{"cc":"MD","id":"+373","text":"Moldova +373"},{"cc":"MC","id":"+33","text":"Monaco +33"},{"cc":"MN","id":"+976","text":"Mongolia +976"},{"cc":"MS","id":"+1473","text":"Montserrat +1473"},{"cc":"MA","id":"+212","text":"Morocco +212"},{"cc":"MZ","id":"+258","text":"Mozambique +258"},{"cc":"MM","id":"+95","text":"Myanmar (Burma) +95"},{"cc":"NA","id":"+264","text":"Namibia +264"},{"cc":"NR","id":"+674","text":"Nauru +674"},{"cc":"NP","id":"+977","text":"Nepal +977"},{"cc":"NL","id":"+31","text":"Netherlands +31"},{"cc":"AN","id":"+599","text":"Netherlands Antilles +599"},{"cc":"NC","id":"+687","text":"New Caledonia +687"},{"cc":"NZ","id":"+64","text":"New Zealand +64"},{"cc":"NI","id":"+505","text":"Nicaragua +505"},{"cc":"NE","id":"+227","text":"Niger +227"},{"cc":"NG","id":"+234","text":"Nigeria +234"},{"cc":"NU","id":"+683","text":"Niue +683"},{"cc":"NF","id":"+6723","text":"Norfolk Island +6723"},{"cc":"MP","id":"+1670","text":"Northern Mariana Islands +1670"},{"cc":"NO","id":"+47","text":"Norway +47"},{"cc":"OM","id":"+968","text":"Oman +968"},{"cc":"PK","id":"+92","text":"Pakistan +92"},{"cc":"PW","id":"+680","text":"Palau +680"},{"cc":"PA","id":"+507","text":"Panama +507"},{"cc":"PG","id":"+675","text":"Papua New Guinea +675"},{"cc":"PY","id":"+595","text":"Paraguay +595"},{"cc":"PE","id":"+51","text":"Peru +51"},{"cc":"PH","id":"+63","text":"Philippines +63"},{"cc":"PN","id":"+870","text":"Pitcairn +870"},{"cc":"PL","id":"+48","text":"Poland +48"},{"cc":"PT","id":"+351","text":"Portugal +351"},{"cc":"PR","id":"+1787","text":"Puerto Rico +1787"},{"cc":"QA","id":"+974","text":"Qatar +974"},{"cc":"RE","id":"+262","text":"Reunion +262"},{"cc":"RO","id":"+40","text":"Romania +40"},{"cc":"RU","id":"+7","text":"Russia +7"},{"cc":"RW","id":"+250","text":"Rwanda +250"},{"cc":"SH","id":"+290","text":"Saint Helena +290"},{"cc":"KN","id":"+1869","text":"Saint Kitts And Nevis +1869"},{"cc":"LC","id":"+1758","text":"Saint Lucia +1758"},{"cc":"PM","id":"+508","text":"Saint Pierre And Miquelon +508"},{"cc":"VC","id":"+1784","text":"Saint Vincent And The Grenadines +1784"},{"cc":"SM","id":"+378","text":"San Marino +378"},{"cc":"ST","id":"+239","text":"Sao Tome And Principe +239"},{"cc":"SA","id":"+966","text":"Saudi Arabia +966"},{"cc":"SN","id":"+221","text":"Senegal +221"},{"cc":"SC","id":"+248","text":"Seychelles +248"},{"cc":"SL","id":"+232","text":"Sierra Leone +232"},{"cc":"SG","id":"+65","text":"Singapore +65"},{"cc":"SK","id":"+421","text":"Slovak Republic +421"},{"cc":"SI","id":"+386","text":"Slovenia +386"},{"cc":"SB","id":"+677","text":"Solomon Islands +677"},{"cc":"SO","id":"+252","text":"Somalia +252"},{"cc":"ZA","id":"+27","text":"South Africa +27"},{"cc":"ES","id":"+34","text":"Spain +34"},{"cc":"LK","id":"+94","text":"Sri Lanka +94"},{"cc":"SD","id":"+249","text":"Sudan +249"},{"cc":"SR","id":"+597","text":"Suriname +597"},{"cc":"SZ","id":"+268","text":"Swaziland +268"},{"cc":"SE","id":"+46","text":"Sweden +46"},{"cc":"CH","id":"+41","text":"Switzerland +41"},{"cc":"SY","id":"+963","text":"Syria +963"},{"cc":"TW","id":"+886","text":"Taiwan +886"},{"cc":"TJ","id":"+7","text":"Tajikistan +7"},{"cc":"TZ","id":"+255","text":"Tanzania +255"},{"cc":"TH","id":"+66","text":"Thailand +66"},{"cc":"TG","id":"+228","text":"Togo +228"},{"cc":"TK","id":"+690","text":"Tokelau +690"},{"cc":"TO","id":"+676","text":"Tonga +676"},{"cc":"TT","id":"+1868","text":"Trinidad And Tobago +1868"},{"cc":"TN","id":"+216","text":"Tunisia +216"},{"cc":"TR","id":"+90","text":"Turkey +90"},{"cc":"TM","id":"+993","text":"Turkmenistan +993"},{"cc":"TC","id":"+1649","text":"Turks And Caicos Islands +1649"},{"cc":"TV","id":"+688","text":"Tuvalu +688"},{"cc":"UG","id":"+256","text":"Uganda +256"},{"cc":"UA","id":"+380","text":"Ukraine +380"},{"cc":"AE","id":"+971","text":"United Arab Emirates +971"},{"cc":"GB","id":"+44","text":"United Kingdom +44"},{"cc":"US","id":"+1","text":"United States +1"},{"cc":"UY","id":"+598","text":"Uruguay +598"},{"cc":"UZ","id":"+998","text":"Uzbekistan +998"},{"cc":"VU","id":"+678","text":"Vanuatu +678"},{"cc":"VA","id":"+39","text":"Vatican City (Holy See) +39"},{"cc":"VE","id":"+58","text":"Venezuela +58"},{"cc":"VN","id":"+84","text":"Vietnam +84"},{"cc":"VG","id":"+1284","text":"Virgin Islands (British) +1284"},{"cc":"VI","id":"+1340","text":"Virgin Islands (US) +1340"},{"cc":"WF","id":"+681","text":"Wallis And Futuna Islands +681"},{"cc":"WS","id":"+685","text":"Western Samoa +685"},{"cc":"YE","id":"+381","text":"Yemen +381"},{"cc":"YU","id":"+381","text":"Yugoslavia +381"},{"cc":"ZM","id":"+260","text":"Zambia +260"},{"cc":"ZW","id":"+263","text":"Zimbabwe +263"}],
				placeholder:'Type',
				maximumSelectionSize:1,
				dropdownAutoWidth:true
			});
		});
	}
});	