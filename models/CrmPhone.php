<?php

/**
 * This is the model class for table "nworx_crm__phone".
 *
 * The followings are the available columns in table 'nworx_crm__phone':
 * @property string $id
 * @property string $number
 * @property string $label
 * @property string $contact_id
 * @property string $verified
 */
class CrmPhone extends CrmActiveRecord
{
	public static function getLabelsJs()
	{
		$ret = array();
		foreach(self::getPhoneLabels() as $label)
			$ret[] = array('id'=>$label, 'text'=>$label);
		return json_encode($ret);
	}
	
	public static function getPhoneLabels()
	{
		return self::getLabels(__CLASS__,array(
			'Home',
			'Mobile',
			'Work',
			'Tel',
			'Fax'
		));
	}
	
	/**
	 * Build an array
	 * id => phone extension
	 * text => country + extension (United Kingdom +44)
	 * cc => two letter country code
	 * @return array
	 */
	public static function getCountryExtensionArray()
	{
		$data = require(Yii::getPathOfAlias('crm.models.data.countries-tel').'.php');
		$ret = array();
		foreach($data as $cc => $country) {
			$ret[] = array('cc'=>$cc, 'id'=>$country[1], 'text'=>$country[0] . ' ' . $country[1]);
		}
		return $ret;
	}
	
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('contact_id', 'required'),
			array('number, label', 'length', 'max'=>255),
			array('contact_id', 'length', 'max'=>11),
			array('verified', 'length', 'max'=>1),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, number, label, contact_id, verified', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'contact'=>array(self::BELONGS_TO, 'CrmContact', 'contact_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'number' => 'Number',
			'label' => 'Label',
			'contact_id' => 'Contact',
			'verified' => 'Verified',
		);
	}
	
	/**
	 * Returns the static model of the specified AR class.
	 * @return CrmPhone the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{crm_phone}}';
	}

	public function schema()
	{
		return array(
			'columns'=>array(
				'id'=>'pk',
				'contact_id'=>'int',
				'number'=>'string',
				'label'=>'string',
				'verified'=>'boolean'
			),
			'keys'=>array(
				array('contact_id')
			),
			'foreignKeys'=>array(
				array('crm_phone_contact', 'contact_id', 'crm_contact', 'id', 'CASCADE', 'CASCADE')
			)
		);
	}

	protected function beforeSave() {
		// normalize the number
		// 1. strip out any non-digit characters (except 'x' to denote extension)
		// $pattern = '/[^\dx]|x(?=[^x]*x)/'; // matches only numbers and the first 'x'
		// $normalized_number = preg_replace($pattern, '', $this->number);
        $normalized_number = str_replace(' ', '', $this->number);
		// 2. if the number starts with 00, they meant + and we assume what comes after is country code
		if (strcmp(substr($normalized_number, 0, 2), '00') == 0) {
			$normalized_number = substr($normalized_number, 2, strlen($normalized_number));
		} else if (strcmp(substr($normalized_number, 0, 1), '0') == 0) {
			// 3. if the number starts with 0, strip it and default the number to UK
			$normalized_number = substr($normalized_number, 1, strlen($normalized_number));
			$normalized_number = '44'.$normalized_number;
		}

		$this->number = $normalized_number;
		return parent::beforeSave();
	}
}