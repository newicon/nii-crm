<?php

/**
 * CrmActiveRecord class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * Description of CrmActiveRecord
 *
 * @author steve
 */
class CrmActiveRecord extends NActiveRecord 
{
	
	/**
	 * Save many related HAS_MANY records
	 * This will first delete all records and then re-add them based on
	 * the contents of the $array parameter.
	 * This parameter is an array of attribute arrays.
	 * ~~~
	 * array(
	 *     array('email'=>'steve@newicon.net', 'label'=>'work'),
	 *     array('email'=>'luke@newicon.net', 'label'=>'work')
	 * )
	 * ~~~
	 * Each item of the array is used as attributes to populate the ActiveRecord component this class extends.
	 * All db operations performed are wrapped in a transaction. So that errors occuring between/during deletion 
	 * and inserting are rolled back.
	 * @param array $array
	 * @param CrmContact $contact
	 * @param string $class the ActiveRecord class to create for each item (must have a contact_id field)
	 * @return boolean success/fail
	 */
	public function saveMany($array, $contact, $class=null) 
	{
		$t = Yii::app()->db->beginTransaction();
//		dp($array);exit;
		try {
			// make sure contact_id exists in all passed in items
			$ret = true;
			$model = ($class===null) ? get_class($this) : $class;
			CActiveRecord::model($model)->deleteAll('contact_id=:id', array(':id' => $contact->id));
			foreach ($array as $i => $v) {
				$m = new $model;
				$m->attributes = $array[$i];
				$m->contact_id = $contact->id;
				$ret = $m->save();
				if (!$ret) {
					dp($m->getErrors());exit;
				}
			}
			$t->commit();
		} catch (Exception $e) {
			$t->rollback();
		}
		return $ret;
	}
	
	/**
	 * get all labels using distinct from table and merge with $preSetArray
	 * 
	 * @param string $className
	 * @param array $preSetArray array('Home'=>array('title'=>''),'Work'=>array('title'=>''))
	 * @return array 
	 */
	public static function getLabels($className, $preSetArray) 
	{
		return self::labelArray(
			self::model($className)->cmd()->selectDistinct('label')->where('label!=""')->queryAll(), $preSetArray
		);
	}

	/**
	 * merge two arrays and return in format suitable for popup list
	 * @param type $rowset
	 * @param type $defaultsArray
	 * @return type 
	 */
	public static function labelArray($rowset, $defaultsArray)
	{
		$tmp=array();
		foreach($rowset as $l){
			if(isset($l->label)) continue;
			$tmp[$l['label']] = $l['label'];
		}
		return array_merge($tmp, array_combine(array_values($defaultsArray), array_values($defaultsArray)));
	}
	
}