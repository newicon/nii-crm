<?php

/**
 * This is the model class for table "nworx_crm__address".
 *
 * The followings are the available columns in table 'nworx_crm__address':
 * @property string $id
 * @property string $lines
 * @property string $city
 * @property string $postcode
 * @property string $county
 * @property string $country_id
 * @property string $label
 * @property string $contact_id
 * @property string $verified
 */
class CrmAddress extends CrmActiveRecord
{

	public function init()
	{
		// set to UK by default
		$this->country_id = 'GB';
	}
	
	/**
	 * Get all attributes override to provide the country name
	 * @param type $names
	 * @return array
	 */
	public function getAttributes($names=true)
	{
		$ret = parent::getAttributes($names);
		$ret['country'] = $this->getCountry();
		return $ret;
	}
	
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('contact_id','required'),
//			array('lines, city, postcode, county', 'nii.components.validators.NOneRequiredValidator'),
			array('city, county, label', 'length', 'max'=>250),
			array('postcode, country_id', 'length', 'max'=>10),
			array('contact_id', 'length', 'max'=>11),
			array('verified', 'length', 'max'=>1),
			array('lines, city, postcode, county', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, lines, city, postcode, county, country_id, label, verified', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'contact'=>array(self::BELONGS_TO, 'CrmContact', 'contact_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'lines' => 'Lines',
			'city' => 'City',
			'postcode' => 'Postcode',
			'county' => 'County',
			'country_id' => 'Country',
			'label' => 'Label',
			'contact_id' => 'Contact',
			'verified' => 'Verified',
		);
	}

	/**
	 * get the string url to a google map link web address
	 * @param boolean $directions if true google maps will display directions
	 * @return string url
	 */
	public function mapLink($directions=false)
	{
		$l = ($directions)?'daddr=':'q=';
		$l .= str_replace("\n",' ',$this->lines)
			. NData::param($this->city,'',' ')
			. NData::param($this->postcode,'',' ')
			. NData::param($this->country,'',' ');
		return 'http://www.google.com/maps?f=q&'.$l;
	}

	/**
	 * Print the address lines and format with html breaks
	 * ~~~
	 * <address>
	 *		<?php echo $address->printAddress(); ?>
	 * </address>
	 * ~~~
	 * @param string $newLine the newline character defaults to <br/>
	 * @return string
	 */
	public function printAddress($newLine='<br/>')
	{
		$lines = str_replace("\n", $newLine, trim($this->lines, "\n"));
		$a = NData::param($lines, '', $newLine);
		$a .= NData::param($this->city,   '', $newLine)
		   .  NData::param($this->postcode,'', $newLine)
		   .  NData::param($this->county,  '', $newLine)
		   .  NData::param($this->country, '', $newLine);
		return $a;
	}

	/**
	 * This function is not used yet but seems useful
	 * @param <type> $ipAddr
	 * @return <type>
	 */
	public function getCountryCityFromIP($ipAddr=null)
	{
		$ipAddr = ($ipAddr === null)?Yii::app()->getRequest()->getUserHostAddress():$ipAddr;
		//function to find country and city from IP address
		//Developed by Roshan Bhattarai [url]http://roshanbh.com.np[/url]
		//verify the IP address for the
		if(ip2long($ipAddr)== -1 || ip2long($ipAddr) === false){
			// error ip address is not valid
		}
		$ipDetail=array(); //initialize a blank array

		//get the XML result from hostip.info
		$xml = file_get_contents("http://api.hostip.info/?ip=".$ipAddr);

		//get the city name inside the node <gml:name> and </gml:name>
		preg_match("@<Hostip>(\s)*<gml:name>(.*?)</gml:name>@si",$xml,$match);

		//accessing the city name to the array
		$ipDetail['city']=$match[2];

		//get the country name inside the node <countryName> and </countryName>
		preg_match("@<countryName>(.*?)</countryName>@si",$xml,$matches);

		//assign the country name to the $ipDetail array
		$ipDetail['country']=$matches[1];

		//get the country name inside the node <countryName> and </countryName>
		preg_match("@<countryAbbrev>(.*?)</countryAbbrev>@si",$xml,$cc_match);
		$ipDetail['country_code']=$cc_match[1]; //assing the country code to array

		//return the array containing city, country and country code
		return $ipDetail;

	}
	
	public static function getAddressLabels()
	{
		return self::getLabels(__CLASS__, array(
			'Home',
			'Work',
			'Office',
			'Other',
		));
	}

	public static function getLabelsJs()
	{
		$ret = array();
		foreach(self::getAddressLabels() as $label)
			$ret[] = array('id'=>$label, 'text'=>$label);
		return json_encode($ret);
	}
	
	/**
	 * look up country name by country_id (2 character string ID e.g. GB)
	 * @return string country name
	 */
	public function getCountry()
	{
		$countries = self::getCountryArray();
		return array_key_exists($this->country_id, $countries) ? $countries[$this->country_id] : '';
	}
	
	/**
	 * Get an array of country_code => country name,
	 * The country code is the 2 letter code
	 * @return array
	 */
	public static function getCountryArray()
	{
		return require(Yii::getPathOfAlias('crm.models.data.countries').'.php');
	}
	
	/**
	 * Get country array in dd format
	 * id   => country code, 
	 * text => country
	 * @return array
	 */
	public static function getCountryArrayDd()
	{
		$ret = array();
		foreach(self::getCountryArray() as $code => $country)
			$ret[] = array('id'=>$code, 'text'=>$country);
		return $ret;
	}
	
	/**
	 * Returns the static model of the specified AR class.
	 * @return CrmAddress the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{crm_address}}';
	}

	public function schema(){
		return array(
			'columns'=>array(
				'id'=>'pk',
				'lines'=>'text',
				'city'=>'string',
				'postcode'=>'string',
				'county'=>'string',
				'country_id'=>'varchar(2)',
				'label'=>'string',
				'contact_id'=>'int',
				'verified'=>'boolean'
			),
			'keys'=>array(
				array('contact_id')
			),
			'foreignKeys' => array(
				array('crm_address_contact', 'contact_id', 'crm_contact', 'id', 'CASCADE', 'CASCADE')
			)
		);
	}
}