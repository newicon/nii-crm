<?php
/**
 * @property id
 * @property name
 */
Class CrmGroup extends NAppRecord
{
	public function rules()
	{
		return array(
			array('name', 'required'),
			array('name, color, contacts_count, order', 'safe')
		);
	}

	/**
	 * Adds a list of contacts to the group
	 * returns the group (specified with $groupId) active record model access the new count of contacts in this group via
	 * ~~~
	 * $group = CrmGroup::addContacts(2, array(243, 598));
	 * $group->contactsCount
	 * ~~~
	 * @param int $groupId the group id
	 * @param array $contactIds array of contact id's to add to this group
	 * @return CrmGroup model affected
	 */
	public static function addContacts($groupId, $contactIds)
	{
		if (empty($contactIds))
			return false;
		$group = NData::loadModel('CrmGroup', $groupId);
		foreach ($contactIds as $cid){
			$contact = CrmContact::model()->findByPk($cid);
			if ($contact === null)
				continue;
			$contact->addGroup($group->id);
		}
		// update the contacts count
		$group->contacts_count = $group->getContactsCount();
		$group->save();

		return $group;
	}

	/**
	 * Remove contacts from a group
	 * @param int $groupId
	 * @param array $contactIds
	 * @return CrmGroup model affected
	 */
	public static function removeContacts($groupId, $contactIds)
	{
		// Save the contacts so we can undo this operation?
		// Yii::app()->user->settings->set('crm-undo-remove-contacts', array($groupId, $contactIds));
		foreach ($contactIds as $c) {
			if($c != '') {
				$contact = CrmContact::model()->findByPk($c);
				if ($contact !== null)
					$contact->removeGroup($groupId);
			}
		}
		$group = CrmGroup::model()->findByPk($groupId);
		// update the contacts count
		$group->contacts_count = $group->getContactsCount();
		$group->save();
		return $group;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * @return CrmGroup the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{crm_group}}';
	}

	/**
	 * Returns the count of contacts in this group
	 */
	public function getContactsCount()
	{
		return CrmGroupContact::model()->countByAttributes(array(
			'group_id'=>$this->id
		));
	}

	public function toArray()
	{
		return parent::getAttributes();
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'color'=>'Colour',
			'contacts_count'=>'Contacts'
		);
	}

	public function schema(){
		return array(
			'columns'=>array(
				'id'=>'pk',
				'name'=>'string',
				'color'=>'string',
				'contacts_count'=>"int COMMENT 'Number of contacts in this group, this is a cache field updated by the application'",
				'order'=>'int not null default 0 COMMENT "Order of the groups in the front end view"'
			),
			'keys'=>array(
				array('groupname', 'name', true) // unique index
			),
			array('crm_group', 'contact_id', 'crm_contact', 'id', 'CASCADE', 'CASCADE')
		);
	}
}
