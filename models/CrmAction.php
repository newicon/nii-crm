<?php

/**
 * CrmAction class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * 
 */
class CrmAction extends NActiveRecord
{
	const STATUS_TODO = 'TODO';
	const STATUS_DONE = 'DONE';
	
	/**
	 * Gets the current action todo for this contact
	 * and caches it
	 * @param int $contactId
	 * @return array CrmAction record array representation
	 */
	public function getCurrentActionFor($contactId)
	{
		$actionRecord = $this->findByAttributes(array(
			'contact_id'=>$contactId, 
			'status'=>self::STATUS_TODO
			), array('order'=>'id DESC')
		);
		return $action = $actionRecord ? $actionRecord->toArray() : '';
	}
	
	/**
	 * Get the cache id used for caching the latest action for a contact
	 * @param int $contactId
	 * @return string
	 */
	public function getCacheIdLatestAction($contactId)
	{
		return 'crm-latest-action-contact-'.$contactId;
	}
	
	/**
	 * produce an ical VEVENT xml string representing this action
	 * @param $action a CrmAction object or array representation
	 * Will generate an iCal event similar to:
	 *	// Format to generate:
	 *	// BEGIN:VEVENT
	 *	// DTSTAMP;TZID=Etc/GMT:20150524T100609
	 *	// CREATED;TZID=Etc/GMT:20150524T100609
	 *	// DTSTART;VALUE=DATE:20130827
	 *	// DTEND;VALUE=DATE:20130828
	 *	// X-MICROSOFT-CDO-ALLDAYEVENT:TRUE
	 *	// X-MICROSOFT-MSNCALENDAR-ALLDAYEVENT:TRUE
	 *	// X-FUNAMBOL-ALLDAY:TRUE
	 *	// SEQUENCE:0
	 *	// UID:52136083eb89971efb000001@actions.onepagecrm.com
	 *	// SUMMARY: Bob O'Brien: Do this!
	 *	// DESCRIPTION:
	 *	// END:VEVENT
	 * @return string
	 */
	public function toIcal()
	{
		list($beginTime, $endTime) = $this->getBeginAndEndTimestamps();
		
		$ical  = "BEGIN:VEVENT\n";
		$ical .= "DTSTAMP;TZID=Etc/GMT:".date('Ymd').'T'.date('His')."\n"; // required by Outlook
		$ical .= "CREATED;TZID=Etc/GMT:".date('Ymd').'T'.date('His')."\n";		
		// Format to generate if using an all day event:
			// DTSTART;VALUE=DATE:20130827
			// DTEND;VALUE=DATE:20130828
			// X-MICROSOFT-CDO-ALLDAYEVENT:TRUE
			// X-MICROSOFT-MSNCALENDAR-ALLDAYEVENT:TRUE
			// X-FUNAMBOL-ALLDAY:TRUE
		// Format to generate if not using all day and specifying a time:
			// DTSTART;TZID=Etc/GMT:20150525T090000
			// DTEND;TZID=Etc/GMT:20150525T100000
			// X-MICROSOFT-CDO-ALLDAYEVENT:TRUE
			// X-MICROSOFT-MSNCALENDAR-ALLDAYEVENT:TRUE
			// X-FUNAMBOL-ALLDAY:TRUE		
		$description = '';
		if ($this->contact->company) {
			$description .= $this->contact->company->name . ":\n";
		}
		
		$summary = CrmAction::iCalEscapeString($this->getCalSummary());
		$description = CrmAction::iCalEscapeString($this->getCalDescription());
		
		$ical .= "DTSTART;TZID=Etc/GMT:".date('Ymd', $beginTime).'T'. date('His', $beginTime)."\n";
		$ical .= "DTEND;TZID=Etc/GMT:".date('Ymd', $endTime).'T'. date('His', $endTime)."\n";
		$ical .= "X-MICROSOFT-CDO-ALLDAYEVENT:TRUE"."\n";
		$ical .= "X-MICROSOFT-MSNCALENDAR-ALLDAYEVENT:TRUE"."\n";
		$ical .= "X-FUNAMBOL-ALLDAY:TRUE"."\n";
		
		$ical .= "SEQUENCE:0\n";
		$ical .= "SUMMARY:".$summary."\n";
		$ical .= "DESCRIPTION:".$description."\n";
		$ical .= "UID:crmaction".$this->id."@hub.newicon.net\n"; // required by Outlok
		$ical .= "URL:$url" . "\n";
		$ical .= "END:VEVENT\n";
		return $ical;
	}
	
	// Escapes a string of characters
	public static function iCalEscapeString($string) 
	{
	  return preg_replace('/([\,;])/','\\\$1', $string);
	}
	
	/**
	 * @inheritdoc
	 */
	public function relations()
	{
		return array(
			'contact' => array(self::BELONGS_TO, 'CrmContact', 'contact_id'),
		);
	}
	
	public function toArray()
	{
		return $this->getAttributes();
	}
	
	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return array(
			array('action, contact_id', 'required'),
			array('due, due_select, status, last_action_id, assigned_id', 'safe')
		);
	}
	
	/**
	 * @inheritdoc
	 */
	public function afterSave()
	{
		// reset the action contact cache
		$this->getCurrentActionFor($this->contact_id, true);
		CrmContact::cacheClearToArray($this->contact_id);
		parent::afterSave();
	}
	
	/**
	 * @inheritdoc
	 */
	public function tableName()
	{
		return '{{crm_action}}';
	}
	
	/**
	 * Hook into the before save
	 * @return boolean
	 */
	public function beforeSave()
	{
		if ($this->due_select == 'asap') {
			$this->due = '0000-00-00 00:00:00';
		}
		return parent::beforeSave();
	}
	
	/**
	 * Generate the begin and end dates for this action item
	 * Can be used like:
	 * ~~~
	 * list($beginTime, $endTime) = $this->getBeginAndEndTimestamps();
	 * ~~~
	 * @return array, key 
	 * 0 => begin timestamp
	 * 1 => end timestamp
	 */
	public function getBeginAndEndTimestamps()
	{
		$dueTimestamp = strtotime($this->due);
		// add 30 minutes to the start time to create an end time
		$endTimestamp = strtotime("+30 minutes", $dueTimestamp);
		return array($dueTimestamp, $endTimestamp);
	}
	
	/**
	 * Generates a link to google passing in parameters to fill in a calendar entry
	 * @return string url to google
	 */
	public function getGCalLink()
	{
		list($beginTime, $endTime) = $this->getBeginAndEndTimestamps();
		$beginDate = date('Ymd', $beginTime).'T'. date('His', $beginTime);
		$endDate = date('Ymd', $endTime).'T'. date('His', $endTime);
		$title = '';
		if ($this->contact()) {
			$title = $this->contact()->name. ': ';
		}
		$title .= $this->action;
		
		$url = 'https://www.google.com/calendar/render?action=TEMPLATE&pli=1';
		// the title
		$url .= '&text=' . $this->getCalSummary();
		$url .= '&details=' . $this->getCalDescription();
		$url .= '&dates=' . $beginDate . '/' . $endDate;
		return $url;
	}
	
	/**
	 * Get the url for this action
	 */
	public function getUrl()
	{
		return '/crm/index/index#/contact/' . $this->contact->id;
	}
	
	/**
	 * Generate a string representing multi line description text for this action
	 * @return string
	 */
	public function getCalDescription()
	{
		$description = '';
		if ($this->contact && $this->contact->company) {
			$description .= $this->contact->company->name . ":\n";
		}
		$description .= ' ' . NHtml::urlAbsolute($this->getUrl()) . "\n";
		if ($this->contact->phones) {
			$description .= ' Phone: ';
			foreach($this->contact->phones as $phone) {
				$description .= ' ' . ($phone->label ? $phone->label.': ' : '') . $phone->number . "\n";
			}
		}
		if ($this->contact->emails) {
			foreach($this->contact->emails as $email) {
				$description .= ' ' . $email->email . "\n";
			}
		}
		if ($this->contact->addresses) {
			foreach($this->contact->addresses as $address) {
			$description .= ' ' . trim($address->printAddress("\n" . ' '), "\n" . ' ') . "\n";
				$description .= ' ' . $address->mapLink() . "\n";
			}
		}
		return trim($description, "\n");
	}
	
	/**
	 * Get a summary to represent this action, suitable for dislay as a calendar entry title
	 * Typicall format is "Contact Name : The Action text"
	 * @return string
	 */
	public function getCalSummary()
	{
		$title = '';
		if ($this->contact)
			$title = $this->contact->name. ': ';
		$title .= $this->action;
		return $title;
	}
	
	/**
	 * Get the label text to show for the Due date.
	 * @return string
	 */
	public static function dueLabel($action)
	{
		if ($action['status'] == 'DONE')
			return 'Done';
		if ($action['due_select'] == 'asap')
			return 'ASAP';
		$due = NTime::datetimeToUnix($action['due']);
		// if due is today
		if (NTime::isToday($due))
			return 'Today';
		// if due is yesterday
		if (NTime::wasYesterday($due))
			return 'Yesterday';
		// if due is tomorrow
		$isTomorrow = date('Y-m-d', $due) == date('Y-m-d', strtotime('tomorrow'));
		if ($isTomorrow)
			return 'Tomorrow';
		return date('j M', $due);
	}
	
	/**
	 * Return the color (in hex or rgba) to use for the due label
	 * @param mixed $action array or object
	 * @return string
	 */
	public static function dueColor($action)
	{
		if ($action['status'] == 'DONE')
			return '#468847';
		if ($action['due_select'] == 'asap')
			return '#ED1313';
		$due = NTime::datetimeToUnix($action['due']);
		// if due is before yesterday
		if ($due < strtotime('-2 days'))
			return '#b94a48';
		// if due is yesterday
		if (NTime::wasYesterday($due))
			return '#dd5e16';
		// if due is today
		if (NTime::isToday($due))
			return '#ED8713';
		// if due is tomorrow
		$isTomorrow = date('Y-m-d', $due) == date('Y-m-d', strtotime('tomorrow'));
		if ($isTomorrow)
			return '#dbbc40';
		// if due is 5 days away
		if ($due > strtotime('+5 days'))
			return '#40dbd5';
		// if due is 4 days away
		if ($due > strtotime('+4 days'))
			return '#40db91';
		// if due is 3 days away
		if ($due > strtotime('+3 days'))
			return '#40db53';
		// if due is 2 days away
		if ($due > strtotime('+2 days'))
			return '#6bdb40';
		// if due is 1 days away
		if ($due > strtotime('+1 days'))
			return '#bcdb40';
		return '';
	}
	
	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return array(
			'trashable'=>array(
				'class'=>'nii.components.behaviors.NTrashBinBehavior',
				'onAfterTrash'=>array($this, 'afterTrash')
			),
			'timestampable'=>array(
				'class'=>'nii.components.behaviors.NTimestampable'
            ),
			'activity'=>array(
				'class'=>'activity.behaviors.ActivityBehavior'
			)
		);
	}
	
	/**
	 * @inheritdoc
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * @inheritdoc
	 */
	public function schema()
	{
		return array(
			'columns'=>array(
				'id'=>'pk',
				'contact_id'=>'int',
				'last_action_id'=>'int not null default 0',
				'assigned_id' => 'int',
				'action'=>'string',
				'due'=>'datetime COMMENT "The due date for this action. Note a due date of 0000-00-00 00:00:00 means asap.  This means that asap deadlines will always be filtered to the top"',
				'due_select'=>'string COMMENT "a selector to help pick the date.  Some special date formats apply such as asap, waiting, none"',
				'status'=>'enum("'.self::STATUS_TODO.'", "'.self::STATUS_DONE.'") not null DEFAULT "'.self::STATUS_TODO.'"'
			),
			'foreignKeys' => array(
				array('crm_action_contact', 'contact_id', 'crm_contact', 'id', 'CASCADE', 'CASCADE')
			)
		);
	}
}