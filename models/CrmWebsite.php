<?php

/**
 * This is the model class for table "nworx_crm__website".
 *
 * The followings are the available columns in table 'nworx_crm__website':
 * @property string $id
 * @property string $address
 * @property string $label
 * @property string $contact_id
 */
class CrmWebsite extends CrmActiveRecord
{
	public static function getWebsiteLabels()
	{
		return self::getLabels(__CLASS__,array(
			'Website',
			'Facebook',
			'LinkedIn',
			'Twitter',
			'Blog',
		));
	}
	
	public static function getLabelsJs()
	{
		$ret = array();
		foreach(self::getWebsiteLabels() as $label => $info)
			$ret[] = array('id'=>$label, 'text'=>$label);
		return json_encode($ret);
	}
	
	public function labelHints()
	{
		return array(
			'Website'=>'Enter a web address',
			'Facebook'=>'Enter a Facebook profile address e.g. "http://www.facebook.com/markzuckerberg"',
			'LinkedIn'=>'Enter a Linkedin profile address like "http://www.linkedin.com/in/profilename" or "http://www.linkedin.com/compnay/newicon"',
			'Twitter'=>'Enter a twitter username e.g. "newicon"',
			'Blog'=>'title'
		);
	}
	
	/**
	 * Gets the url to the user specified web address
	 * Ensures it starts with a http://
	 * @return string url
	 */
	public function getUrl()
	{
		if (substr($this->address, 0, 7) == 'http://') {
			return $this->address;
		} else {
			return 'http://'.$this->address;
		}
	}
	
	/**
	 * Returns the static model of the specified AR class.
	 * @return CrmWebsite the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{crm_website}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('address, contact_id', 'required'),
			array('address, label', 'length', 'max'=>250),
			array('contact_id', 'length', 'max'=>11),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, address, label, contact_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'contact'=>array(self::BELONGS_TO, 'CrmContact', 'contact_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'address' => 'Address',
			'label' => 'Label',
			'contact_id' => 'Contact',
		);
	}

	public function schema()
	{
		return array(
			'columns'=>array(
				'id'=>'pk',
				'contact_id'=>'int',
				'address'=>'string',
				'label'=>'string',
			),
			'keys'=>array(
				array('contact_id')
			),
			'foreignKeys'=>array(
				array('crm_website_contact', 'contact_id', 'crm_contact', 'id', 'CASCADE', 'CASCADE')
			)
		);
	}
	
}