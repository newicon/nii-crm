<?php

/**
 * CrmAction class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * 
 */
class CrmNote extends NActiveRecord
{
	public function tableName()
	{
		return '{{crm_note}}';
	}
	
	public function toArray()
	{
		return $this->getAttributes();
	}
	
	public function rules()
	{
		return array(
			array('note, contact_id', 'required'),
			array('note, added_by, added_on, contact_id', 'safe')
		);
	}
	
	public function beforeSave()
	{
		// if added_by is empty populate it
		if ($this->added_by === null)
			$this->added_by = Yii::app()->user->id;
		return parent::beforeSave();
	}
	
	public function behaviors()
	{
		return array(
			'activity'=>array(
				'class'=>'activity.behaviors.ActivityBehavior'
			)
		);
	}
	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function schema()
	{
		return array(
			'columns'=>array(
				'id'=>'pk',
				'contact_id'=>'int',
				'note'=>'text',
				'added_on' => 'timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
				'added_by' => 'int COMMENT "Foreign key of the user id that added this note"'
			),
			'keys'=>array(
				array('contact_id'),
				array('added_by')
			)
		);
	}
}