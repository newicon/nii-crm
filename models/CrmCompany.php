<?php

/**
 * CrmContact class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * Contact class represents an individual Contact or Company
 */
class CrmCompany extends CrmContact 
{
	public function init() 
	{
		$this->type = self::TYPE_COMPANY;
	}
}