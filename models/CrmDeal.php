<?php

/**
 * CrmDeal class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * The Crm Deal represents an Opportunity.  However Deal is less characters and sounds cooler
 * Dale boy trotter time!
 */
class CrmDeal extends NActiveRecord
{
	const STATUS_PENDING_10 = 'PENDING_10';
	const STATUS_PENDING_25 = 'PENDING_25';
	const STATUS_PENDING_50 = 'PENDING_50';
	const STATUS_PENDING_75 = 'PENDING_75';
	const STATUS_PENDING_90 = 'PENDING_90';
	const STATUS_WON = 'WON';
	const STATUS_LOST = 'LOST';
	const STATUS_DORMANT = 'DORMANT';
	
	/**
	 * @inheritdoc
	 * @return string
	 */
	public function tableName()
	{
		return '{{crm_deal}}';
	}
	
	/**
	 * return an array of all pending states
	 * @return array
	 */
	public function allPendingStates()
	{
		return array(
			STATUS_PENDING_10,
			STATUS_PENDING_25,
			STATUS_PENDING_50,
			STATUS_PENDING_75,
			STATUS_PENDING_90
		);
	}
	
	public function getStatusLabels()
	{
		return array(
			self::STATUS_PENDING_10 => '10%',
			self::STATUS_PENDING_25 => '25%',
			self::STATUS_PENDING_50 => '50%',
			self::STATUS_PENDING_75 => '75%',
			self::STATUS_PENDING_90 => '90%',
			self::STATUS_WON => 'WON',
			self::STATUS_LOST => 'LOST',
			self::STATUS_DORMANT=> 'DORMANT'
		);
	}
	
	public static function getstatusLabel($status)
	{
		$labels = CrmDeal::model()->getStatusLabels();
		return $labels[$status];
	}
	
	public function toArray()
	{
		return $this->getAttributes();
	}
	
	public function relations()
	{
		return array(
			'contact' => array(self::BELONGS_TO, 'CrmContact', 'contact_id'),
			'owner' => array(self::BELONGS_TO, 'User', 'owner_id'),
		);
	}
	
	public function rules()
	{
		return array(
			array('name, owner_id, contact_id, close_date', 'required'),
			array('id, contact_id, owner_id, name, amount, multi_month, month_count, status, description, created_on, created_by', 'safe')
		);
	}
	
	public function beforeSave()
	{
		// if added_by is empty populate it
		if ($this->created_by === null)
			$this->created_by = Yii::app()->user->id;
		return parent::beforeSave();
	}
	
	public function behaviors()
	{
		return array(
			'activity'=>array(
				'class'=>'activity.behaviors.ActivityBehavior'
			)
		);
	}
	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function schema()
	{
		return array(
			'columns'=>array(
				'id' => 'pk',
				'contact_id' => 'int',
				'owner_id' => 'int COMMENT "Foreign key (id) of the user that is responsible for this deal"',
				'name' => 'string',
				'amount'=> 'money',
				'close_date'=>'datetime COMMENT "The expected close date of the deal"',
				'multi_month'=>'bool not null default 0 COMMENT "If true (1) then this is a multi month deal. Making the total value amount * month_count "',
				'month_count' => 'int not null default 0 COMMENT "The number of months"',
				'status' => 'ENUM ("'.self::STATUS_PENDING_10.'", "'.self::STATUS_PENDING_25.'", "'.self::STATUS_PENDING_50.'", "'.self::STATUS_PENDING_75.'", "'.self::STATUS_PENDING_90.'", "'.self::STATUS_WON.'", "'.self::STATUS_LOST.'", "'.self::STATUS_DORMANT.'") not null default "'.self::STATUS_PENDING_10.'"',
				'description' => 'text',
				'label' => 'string COMMENT "A way of grouping deals by type e.g. SEO, wordpress, etc."',
				'created_on' => 'timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
				'created_by' => 'int COMMENT "Foreign key of the user id that added this note"'
			),
			'keys'=>array(
				array('contact_id'),
				array('owner_id')
			),
			'foreignKeys' => array(
				array('crm_deal_contact', 'contact_id', 'crm_contact', 'id', 'CASCADE', 'CASCADE')
			)
		);
	}
}