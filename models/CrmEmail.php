<?php

/**
 * This is the model class for table "nworx_crm__email".
 *
 * The followings are the available columns in table 'nworx_crm__email':
 * @property string $id
 * @property string $contact_id
 * @property string $email
 * @property string $label
 * @property string $verified
 */
class CrmEmail extends CrmActiveRecord
{
	/**
	 * 
	 * @return array
	 */
	public static function getEmailLabels()
	{
		return self::getLabels(__CLASS__, array(
			'Home',
			'Work',
			'Other'
		));
	}
	
	public static function getLabelsJs()
	{
		$ret = array();
		foreach(self::getEmailLabels() as $label)
			$ret[] = array('id'=>$label, 'text'=>$label);
		return json_encode($ret);
	}
	
	/**
	 * @return string url mailto link
	 */
	public function getMailToLink()
	{
		return 'mailto:' . $this->email; // ?bcc=postmark.inbound email so emails are attached to the client
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('contact_id', 'required'),
			array('email', 'safe'),
			array('contact_id', 'length', 'max' => 11),
			array('email, label', 'length', 'max' => 250),
			array('verified', 'length', 'max' => 1),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, contact_id, email, label, verified', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'contact' => array(self::BELONGS_TO, 'CrmContact', 'contact_id'),
		);
	}
	
	public function behaviors()
	{
		return array(
			'trash'=>array(
				'class'=>'nii.components.behaviors.NTrashBinBehavior',
			),
			'timestampable'=>array(
				'class'=>'nii.components.behaviors.NTimestampable'
            )
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'contact_id' => 'Contact',
			'email' => 'Email',
			'label' => 'Label',
			'verified' => 'Verified',
		);
	}
	
	/**
	 * Returns the static model of the specified AR class.
	 * @return CrmEmail the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{crm_email}}';
	}

	public function schema()
	{
		return array(
			'columns' => array(
				'id' => 'pk',
				'contact_id' => 'int',
				'email' => 'string',
				'label' => 'string',
				'verified' => 'boolean'
			),
			'keys' => array(
				array('contact_id'),
				array('email')
			),
			'foreignKeys' => array(
				array('crm_email_contact', 'contact_id', 'crm_contact', 'id', 'CASCADE', 'CASCADE')
			)
		);
	}

}