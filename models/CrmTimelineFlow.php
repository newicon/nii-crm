<?php

/**
 * The flow model represents the timeline for a particular contact or company.
 * Each row can represent a different type of object/model
 * Example rows could represent:
 * 
 * - A Note
 * - A Phone call (with the ability to play back)
 * - A new Deal created (might vary from a deal object itself as it represents activity of creating a note, not the deal itself)
 * - A deadline
 * - An Action to complete something (typically completed actions as a timeline is typically historical only)
 * 
 * The flow record
 * @property int id the primary key
 * @property string timestamp A datetime the date and time of this timeline flow item
 * @property int contact_id The id of the contact (crm_contact with type CONTACT) this related to, this can be null if this timeline entry is only relevant to a company
 * @property int company_id The id of the compny (crm_contact with type COMPANY) this also could be null but should be avoided
 * @property string model The full path to the model class e.g. CrmNote
 */
class CrmTimelineFlow extends NActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public function schema()
	{
		return array(
			'columns' => array(
				'id' => 'pk',
				'timestamp' => 'datetime COMMENTS "The date and time of this timeline flow item"',
				'contact_id' => 'int COMMENT "The id of the contact (crm_contact with type CONTACT) this related to, this can be null if this timeline entry is only relevant to a company"',
				'company_id' => 'int COMMENT "The id of the compny (crm_contact with type COMPANY) this also could be null but should be avoided"',
				'model_type' => 'string null',
				'model_class' => 'string COMMENT "The full path"',
				'model_id'	=> 'int',
				'data' => 'text null',
				'user_id' => 'int',
				'updated_at'=>'timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'
			)
		);
	}
}