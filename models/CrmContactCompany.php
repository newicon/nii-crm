<?php

/**
 * CrmContactCompanyRelation class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * This class links contacts to companies
 * 
 */
class CrmContactCompany extends NActiveRecord
{
	/**
	 * Attach a company to a contact record.
	 * Creates a new CrmContactCompany row.
	 * If $companyId is a string the function will attempt to create a new CrmCompany record
	 * with the string as the name and attach this to the contact
	 * @param string $role
	 * @param mixed $companyIdOrName id or string company name
	 * @param int $contactId
	 * @return boolean whether CrmContactCompany row was successfully saved
	 * @throws CException
	 */
	public static function saveCompany($role, $companyIdOrName, $contactId)
	{
		if (empty($companyIdOrName))
			throw new CException('A company ID or string must be specified.');
		if (!is_numeric($companyIdOrName)) {
			$company = new CrmCompany;
			$company->name = $companyIdOrName;
			if (!$company->save())
				throw new CException('Unable to save a new company');
			$companyIdOrName = $company->id;
		}
		$c = new CrmContactCompany;
		$c->role = $role;
		$c->company_id = $companyIdOrName;
		$c->contact_id = $contactId;
		
		return $c->save();
	}
	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function tableName()
	{
		return '{{crm_contact_company}}';
	}
	
	public function schema()
	{
		return array(
			'columns'=>array(
				'company_id' => 'int',
				'contact_id' => 'int',
				'role' => 'string',
				'Primary key (company_id, contact_id)'
			)
		);
	}
}