<?php

/**
 * ProjectController class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * Defines status types that a customer can be defined as.
 * Note this is subtly different to groups which are essentially tags, a contact can be in multiple groups
 * however a contact can only be one status at a time
 * @author steve
 */
class CrmStatus extends NActiveRecord
{
	public static function getStatusList()
	{
		$stats = CrmStatus::model()->with('countContacts')->findAll(array('order' => 'id'));
		$return = array();
		foreach($stats as $i => $stat) {
			$return[$i] = $stat->attributes;
			$return[$i]['count'] = $stat->countContacts;
		}
		return $return;
	}
	
	/**
	 * Get the default list of states
	 * @return array
	 */
	public function getDefaults()
	{
		return array(
			array('id'=>1, 'name'=>'Lead', 'description'=>'You intend to sell to', 'color'=>'#FF5959'),
			array('id'=>2, 'name'=>'Prospect', 'description'=>'You are actively selling to', 'color'=>'#FFB759'),
			array('id'=>3, 'name'=>'Customer', 'description'=>'You have previously sold too', 'color'=>'#77E359'),
			array('id'=>4, 'name'=>'Client', 'description'=>'You have sold to multiple times', 'color'=>'#59ACE3'),
			array('id'=>5, 'name'=>'Inactive', 'description'=>'You are not actively selling to', 'color'=>'#A6A6A6'),
			array('id'=>6, 'name'=>'General', 'description'=>'Non-sales related contacts', 'color'=>'#5E5E5E')
		);
	}
	
	/**
	 * returns the 'inactive' state id
	 */
	public function getInactiveId()
	{
		return 5;
	}
	
	/**
	 * returns the 'general' state id
	 */
	public function getGeneralId()
	{
		return 6;
	}
	
	public function relations() 
	{
		return array(
			'countContacts'=>array(self::STAT, 'CrmContact', 'status_id'),
		);
	}
	
	/**
	 * Insert default state rows
	 */
	public function installDefaultStates()
	{
		// Only install defaults if the table is empty
		if ($this->count()) 
			return;
		foreach ($this->getDefaults() as $status) {
			$r = new CrmStatus();
			$r->id = $status['id'];
			$r->name = $status['name'];
			$r->description = $status['description'];
			$r->color = $status['color'];
			$r->save();
		}
	}
	
	/**
	 * After install, install default states
	 */
	public function afterInstall() 
	{
		$this->installDefaultStates();
	}
	
	/**
	 * Define model rules
	 * @return array
	 */
	public function rules()
	{
		return array(
			array('id, name, description, color', 'safe')
		);
	}
	
	/**
	 * @return string table name
	 */
	public function tableName()
	{
		return '{{crm_status}}';
	}
	
	/**
	 * @param string $className
	 * @return CrmStatus
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * table schema
	 * @return array
	 */
	public function schema()
	{
		return array(
			'columns'=>array(
				'id'=>'pk',
				'name'=>'string',
				'description'=>'string',
				'color'=>'varchar(50)'
			)
		);
	}
}
