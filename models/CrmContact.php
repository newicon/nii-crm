<?php

/**
 * CrmContact class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * Contact class represents an individual Contact or Company
 */
class CrmContact extends CrmActiveRecord
{
	const TYPE_CONTACT = 'CONTACT';
	const TYPE_COMPANY = 'COMPANY';
	
	public $noName = '';
	
	public function init() 
	{
		$this->type = self::TYPE_CONTACT;
		// default options to set for a new record
		if ($this->getScenario() !== 'search' && $this->isNewRecord) {
			$this->name = $this->noName;
			$this->owner_id = Yii::app()->user->id;
		}
	}
	
	/**
	 * Determines if $value is a title
	 * @param string $value
	 */
	public function isTitle($value)
	{
		return (in_array(strtolower($value), array_map('strtolower', $this->titles())));
	}
	
	/**
	 * Get an array of titles
	 * @return array of titles
	 */
	public function titles()
	{
		return array('Mr','Mrs','Ms','Miss','Sir','Lord','Dr','Prof','Revd','Revd Dr','Revd Prof','Revd Canon','Most Revd','Rt Revd','Rt Revd Dr','Venerable','Revd Preb','Pastor','Sister');
	}
	
	/**
	 * This function takes a string or array representing the name and sets the relevent fields.
	 * If an array is passed it must contain one or more of the following keys:
	 * array(
	 *	 name_title  => 'mr',
	 *   name_first  => 'steve',
	 *   name_middle => 'Andrew'
	 *   name_last   => 'obrien',
	 *	 name_suffix => 'BA Hons'
	 * )
	 * 
	 * @param mixed $value
	 */
	public function setName($value)
	{
		if ($this->isCompany()) {
			if (is_string($value)) {
				$this->name = $value;
				// name company is used for search
				$this->name_company = $value;
			}
		} else {
			if (is_string($value)) {
				$value = $this->guessNameFromString($value);
			}
			if (is_array($value)) {
				// check array is valid
				$check = false;  
				foreach ($value as $key => $v){
					if (in_array($key, array('name_title', 'name_first', 'name_middle', 'name_last', 'name_suffix')))
						 $check = true;
				}
				if (!$check)
					throw new CException('Incorrect keys defined for the name attribute '. var_dump($value, true));
				// set inidivudal name parts
				parent::setAttributes($value);
				
				$name = (array_key_exists('name_first', $value)) ? $value['name_first'] : '';
				$name .= (array_key_exists('name_middle', $value)) ? ' ' . $value['name_middle'] : '';
				$name .= (array_key_exists('name_last', $value)) ? ' ' . $value['name_last'] : '';
				$this->name = $name;
			}
		}
	}
	
	/**
	 * This function takes a string representing the name of a contact
	 * and tries to save the individual parts, like: title, first, last and middle name
	 * Generally the first part of a string is the title, if it finds a match in the titles list,
	 * otherwise it is the first name, the last part of the string is the last name everything else in the middle
	 * is the middle name(s). This function will not attempt to set the suffix. You would have to define this yourself.
	 * returns an array of array(
	 *   name_title => '',
	 *   name_first => '',
	 *   name_middle => '',
	 *   name_last  => '',
	 *   name_suffix => ''
	 * )
	 * @param string $value name
	 * @return array
	 */
	public function guessNameFromString($value)
	{
		$name = array();
		$names = explode(' ', trim($value));
		// if the string has no spaces
		// assume the entered value is the first name
		if (count($names) == 0) {
			$name['name_first'] = $names[0];
		} else {
			if ($this->isTitle($names[0]))
				$name['name_title'] = array_shift($names);
			$count = count($names);
			$name['name_first'] = $names[0];
			if ($count == 2) {
				$name['name_last'] = $names[1];
			} else if ($count >= 3) {
				$name['name_middle'] = $names[1];
				if ($count > 4) {
					$stop = $count-2;
					for ($i = 2; $i<=$stop; $i++) {
						$name['name_middle'] .= ' ' . $names[$i];
					}
				}
				$name['name_last'] = $names[$count-1];
			}
		}
		return $name;
	}
	
	/**
	 * @return boolean true if record represents a contact
	 */
	public function isContact()
	{
		return $this->type == self::TYPE_CONTACT;
	}
	
	/**
	 * @return boolean true if record represents a company
	 */
	public function isCompany()
	{
		return $this->type == self::TYPE_COMPANY;
	}
	
	/**
	 * Core contact types
	 * @return array enum to human readable
	 */
	public function typeLabels()
	{
		return array(
			self::TYPE_CONTACT => 'Contact',
			self::TYPE_COMPANY => 'Company',
		);
	}
	
	/**
	 * @return Contact / Organisation
	 */
	public function getTypeLabel()
	{
		$labels = $this->typeLabels();
		return $labels[$this->type];
	}
	
	/**
	 * Attach a company to a contact record.
	 * If $companyId is a string the function will attempt to create a new CrmCompany record
	 * with the string as the name and attach this to the contact
	 * Note: $contact->save() must be called after calling this function to apply the changes to the current contact:
	 * Example useage:
	 * ~~~
	 * $contactOne->setcompany('Acme Inc', 'Director'); // will create the company Acme Inc
	 * $contactOne->save();
	 * $contactTwo->setcompany(5, 'Director'); // will use the company id 5
	 * $contactTwo->save();
	 * ~~~
	 * Note this function does not check for duplicates. If you pass the $companyIdOrName as a non numeric value
	 * a new company will be created. Any checks to determine whether this is the valid behavior should be done before
	 * invoking this function.
	 * @param string $role
	 * @param mixed $companyIdOrName id or string company name
	 * @param int $contactId
	 * @return int | false company id
	 * @throws CException if unable to create a new company
	 */
	public function setCompany($companyIdOrName, $companyRole)
	{
		if (empty($companyIdOrName))
			return false;
		if (!is_numeric($companyIdOrName)) {
			$company = new CrmCompany;
			$company->name = $companyIdOrName;
			if (!$company->save())
				throw new CException('Unable to save a new company');
			$companyIdOrName = $company->id;
		} else {
			$company = NData::loadModel('CrmCompany', $companyIdOrName, "Can not find a company record with id '$companyIdOrName'");
		}
		$this->company_role = $companyRole;
		$this->company_id = $company->id;
		// A new contact has been set to relate to this company.
		// Therefore the companies contacts relation cache needs to be invalidated so that the next time
		// this the company is loaded it also retrievs this newly associated record.
		return $this->company_id;
	}
	
	/**
	 * Get the route to the detail view for this record
	 * @return array
	 */
	public function getRoute()
	{
		return '/crm/index/index#/contact/' . $this->id;
	}
	
	/**
	 * Get the route to the edit page for this record
	 * @return array
	 */
	public function getRouteEdit()
	{
		return '/crm/index/index#/edit/' . $this->id;
	}
	
	/**
	 * Get the url to the detail view for this record
	 * @return type
	 */
	public function getUrl()
	{
		return NHtml::url($this->route);
	}
	
	/**
	 * Get the url to the contact / company profile image
	 * @return string
	 */
	public function getImageUrl()
	{
		return self::getImage($this->toArray());
	}
	
	/**
	 * Get the url to the contact / company profile image
	 * @return string
	 */
	public function getImage_url()
	{
		return $this->getImageUrl();
	}
	
	/**
	 * Get image url of the conact 24x24
	 * @return string url
	 */
	public function getImageUrlThumb24()
	{
		return self::getImage($this->toArray(), 24);
	}
	
	/**
	 * Get image url of the conact 24x24
	 * @return string url
	 */
	public function getImage_url_24()
	{
		return $this->getImageUrlThumb24();
	}
	
	/**
	 * 
	 * @param array $contactDataArray (pass in result of $contact->toArray()
	 * @return type
	 */
	public static function getImage($contactDataArray, $size=80)
	{
		$array = $contactDataArray;
		if ($array['image_id'] !== null) {
			$type = ($array['type']=='CONTACT') ? 'crm-contact' : 'crm-company';
			return NImage::url($array['image_id'], "$type-24");
		}
		// gravatar
		$email = '';
		if (count($array['emails'])){
			$email = $array['emails'][0]['email'];
		}
		//self::gravatarExists($email)
		if (true)
			return self::getGravatar($email, $contactDataArray['type'], $size);
		else {
			// check facebook
			// default
			if ($array['type'] == self::TYPE_CONTACT)
				return NHtml::url() . '/nii/index/show/id//type/crm-contact';
			else
				return NHtml::url() . '/nii/index/show/id//type/crm-company';//;
		}
	}
	
	/**
	 * Get either a Gravatar URL or complete image tag for a specified email address.
	 *
	 * @param string $email The email address
	 * @param string $s Size in pixels, defaults to 80px [ 1 - 2048 ]
	 * @param string $d Default imageset to use [ 404 | mm | identicon | monsterid | wavatar ]
	 * @param string $r Maximum rating (inclusive) [ g | pg | r | x ]
	 * @param boole $img True to return a complete IMG tag False for just the URL
	 * @param array $atts Optional, additional key/value attributes to include in the IMG tag
	 * @return String containing either just a URL or a complete image tag
	 * @source http://gravatar.com/site/implement/images/php/
	 */
	public static function getGravatar($email, $type, $s = 80, $d = 'mm', $r = 'g', $img = false, $atts = array()) 
	{
		$http = Yii::app()->getRequest()->isSecureConnection ? 'https' : 'http';
		$assets = Yii::app()->getModule('crm')->getAssetsUrl();
		$path =  "https://hub.newicon.net/$assets/styles/images/";
		$typeImg = ('CONTACT' == $type) ? 'contact' : 'company';
		$typeImg .= ($s == 24) ? '-24.jpg' : '.jpg';
		$d = $path.$typeImg;
		$url = $http.'://www.gravatar.com/avatar/';
		$url .= md5(strtolower(trim($email)));
		$url .= "?s=$s&d=$d&r=$r";
		return $url;
	}
	
	public static function gravatarExists($email) 
	{
		// Craft a potential url and test its headers
		$http = Yii::app()->getRequest()->isSecureConnection ? 'https' : 'http';
		$hash = md5(strtolower(trim($email)));
		$uri = $http.'://www.gravatar.com/avatar/' . $hash . '?d=404';
		$headers = @get_headers($uri);
		if (!preg_match("|200|", $headers[0])) {
			$has_valid_avatar = FALSE;
		} else {
			$has_valid_avatar = TRUE;
		}
		return $has_valid_avatar;
	}

	/**
	 * Determins if this is an empty record
	 * @return boolean whether this is an empty record
	 */
	public function isEmpty()
	{
		if ($this->isContact() && $this->name == $this->noName) {
			// find if we have any associated data
			// If its all empty then we are safe to delete.
			return true;
		}
		return false;
	}
	
	/**
	 * Model relations
	 * @return array
	 */
	public function relations() 
	{
		return array(
			'addresses' => array(self::HAS_MANY, 'CrmAddress', 'contact_id'),
			'emails' => array(self::HAS_MANY, 'CrmEmail', 'contact_id'),
			'phones' => array(self::HAS_MANY, 'CrmPhone', 'contact_id'),
			'websites' => array(self::HAS_MANY, 'CrmWebsite', 'contact_id'),
			'staff'=> array(self::HAS_MANY, 'CrmContact', 'company_id', 'condition'=>"type = '".self::TYPE_CONTACT."'"),
			// if a company has a company_id then it is refering to its department
			'departments'=> array(self::HAS_MANY, 'CrmContact', 'company_id', 'condition'=>"type = '".self::TYPE_COMPANY."'"),
			'company' => array(self::BELONGS_TO, 'CrmContact', 'company_id'),
			// this relation has not yet been tested
			'groups' => array(self::MANY_MANY, 'CrmGroup', 'crm_group_contact(contact_id, group_id)'),
			// 'actions' => array(self::HAS_MANY, 'CrmAction', 'contact_id', 'joinType'=>'INNER JOIN'),
			// 'latestAction'=> array(self::HAS_MANY, 'CrmAction', 'contact_id', 'order'=>'id DESC', 
			//	'condition'=>"latestAction.status = '".CrmAction::STATUS_TODO."'")
		);
	}
	
	/**
	 * Add an email address to this record. Note: the contact record must already exist as we need a pk
	 * @param type $email
	 * @param type $label
	 * @param type $verified
	 * @throws CException Throws an exception when using addEmail on a record that is not in the database.
	 * @return CrmEmail
	 */
	public function addEmail($email, $label=null, $verified=false)
	{
		if ($this->isNewRecord) {
			// This could easily be changed so that if we are on a new record the new email details are stored in an array
			// then hook into the records after save function and loop through email addresses that have been stored and save them.
			// but does this add too much gunk to a record? If its possible it should be handles by a low level framework
			// component so that relations like $record->emails returns all email records, those in the db and those that are yet to be saved
			// this way it would work much better with forms
			throw new CException('Emails can only be added to existing records');
		}
		$e = new CrmEmail;
		$e->email = $email;
		$e->label = $label;
		$e->verified = $verified;
		$e->contact_id = $this->id;
		$e->save();
		return $e;
	}
	
	/**
	 * Add a phone number to this contact record
	 * @param type $number
	 * @param string $label
	 * @param type $verified
	 * @throws CException
	 * @return CrmPhone
	 */
	public function addPhone($number, $label=null, $verified=false)
	{
		if ($this->isNewRecord) {
			// This could easily be changed so that if we are on a new record the new email details are stored in an array
			// then hook into the records after save function and loop through email addresses that have been stored and save them.
			// but does this add too much gunk to a record? If its possible it should be handles by a low level framework
			// component so that relations like $record->emails returns all email records, those in the db and those that are yet to be saved
			// this way it would work much better with forms
			throw new CException('Phone numbers can only be added to existing records');
		}
		$p = new CrmPhone();
		$p->number = $number;
		$p->label = $label;
		$p->verified = $verified;
		$p->contact_id = $this->id;
		$p->save();
		return $p;
	}
	
	/**
	 * Add a website record to the contact
	 * @param
	 * @throws CException
	 * @return CrmWebste
	 */
	public function addWebsite($address, $label='')
	{
		if ($this->isNewRecord) {
			// This could easily be changed so that if we are on a new record the new email details are stored in an array
			// then hook into the records after save function and loop through email addresses that have been stored and save them.
			// but does this add too much gunk to a record? If its possible it should be handles by a low level framework
			// component so that relations like $record->emails returns all email records, those in the db and those that are yet to be saved
			// this way it would work much better with forms
			throw new CException('Websites and Social Networks can only be added to existing records');
		}
		$w = new CrmWebsite();
		$w->address = $address;
		$w->label = $label;
		$w->contact_id = $this->id;
		$w->save();
		return $w;
	}
	
	/**
	 * 
	 * @param string $lines address lines seperated by line feed
	 * @param string $city
	 * @param string $county
	 * @param string $country two character country code e.g. 'GB'
	 * @param string $postcode
	 * @param string $label
	 * @param boolean $verified
	 * @throws CException
	 * @return CrmAddress
	 */
	public function addAddress($lines, $city, $county, $country, $postcode, $label='', $verified=false)
	{
		if ($this->isNewRecord) {
			// This could easily be changed so that if we are on a new record the new email details are stored in an array
			// then hook into the records after save function and loop through email addresses that have been stored and save them.
			// but does this add too much gunk to a record? If its possible it should be handles by a low level framework
			// component so that relations like $record->emails returns all email records, those in the db and those that are yet to be saved
			// this way it would work much better with forms
			throw new CException('Addresses can only be added to existing records');
		}
		$a = new CrmAddress();
		$a->lines = $lines;
		$a->city = $city;
		$a->county = $county;
		$a->country_id = $country;
		$a->postcode = $postcode;
		$a->label = $label;
		$a->verified = $verified;
		$a->contact_id = $this->id;
		$a->save();
		return $a;
	}
	
	/**
	 * output this contact as a vcard
	 * @return string vcard format
	 */
	public function toVcard()
	{
	}
	
	/**
	 * Get the id's of all groups this contact belongs to
	 * @retrun array group ids
	 */
	public function getGroupIds()
	{
		$grpIds = array();
		if (!$this->isNewRecord) {
			$res = Yii::app()->db->createCommand()
				->select('group_id')
				->from('crm_group_contact')
				->where('contact_id = '.$this->id)
				->queryColumn();

			foreach($res as $grp)
				$grpIds[] = $grp;
		}
		return $grpIds;
	}
	
	/**
	 * Add group
	 * @param int $groupId
	 */
	public function addGroup($groupId)
	{
		$grp = new CrmGroupContact;
		$grp->contact_id = $this->id;
		$grp->group_id = $groupId;
		$grp->save();
		// remove the group cache
		$this->updated_at = NTime::unixToDatetime();
		$this->save();
	}
	
	/**
	 * Remove a group from this contact
	 * @param int $groupId
	 */
	public function removeGroup($groupId)
	{
		CrmGroupContact::model()->deleteByPk(array('contact_id'=>$this->id, 'group_id'=>$groupId));
		$this->updated_at = NTime::unixToDatetime();
		$this->save();
	}
	
	/**
	 * Generate a small array representation of this contact
	 * @return array
	 */
	public function toArraySmall()
	{
		$attrs = parent::getAttributes(array('id', 'name', 'image_url', 'url', 'type'));
		if ($this->isContact()) {
            $attrs['company_role'] = $this->company_role;
			if ($this->company()) {
				$attrs['company'] = $this->company()->getAttributes(array('id', 'name', 'url', 'image_url', 'image_url_24'));
			}
		}
		return $attrs;
	}
	
	public function getArrayData($attributes)
	{
		$array = array();
		foreach($attributes as $attr){
			$array[$attr] = $this->$attr;
		}
		return $array;
	}
	
	/**
	 * Serialize contact to an array
	 * @return array
	 */
	public function toArray($refresh=false)
	{
		$attrs = self::getContactArray($this, $refresh);
		return CMap::mergeArray(parent::getAttributes(), $attrs);
	}
	
	public function toArrayExpanded()
	{
		$data = $this->toArray();
		// load expanded properties
		$data['deals'] = Yii::app()->db->createCommand('select * from crm_deal where contact_id = '.$data['id'])->queryAll();
		$data['notes'] = Yii::app()->db->createCommand('select * from crm_note where contact_id = '.$data['id'])->queryAll();
		return $data;
	}
	
	/**
	 * Create the array representing the contact and all its joined data (look like mongo much?)
	 * @param CrmContact $contact
	 * @param boolean $refresh whether to refresh the cache for this contact
	 * @return array
	 */
	public static function getContactArray($contact, $refresh=false)
	{
		
		$results = array();
		//$cid = self::cacheIdToArray($contact->id);
		//if ($refresh)
			//Yii::app()->cache->delete($cid);
		//$data = Yii::app()->cache->get($cid);
		$data = false;
		if ($data === false) {
			$data = $contact->getAttributes();
			if ($data['type'] == 'COMPANY')
				$data['staff'] = Yii::app()->db->createCommand('select id, name from crm_contact where company_id = '.$data['id'])->queryAll();
			
			if ($data['company_id']) {
				$data['company'] = Yii::app()->db->createCommand('select id, name from crm_contact where id = '.$data['company_id'])->queryRow();
				$data['company']['image_url'] = NHtml::url() . '/nii/index/show/id//type/crm-company';
			}
			$data['emails'] = Yii::app()->db->createCommand('select id, email, label from crm_email where contact_id = '.$data['id'])->queryAll();
			$data['phones'] = Yii::app()->db->createCommand('select id, number, label from crm_phone where contact_id = '.$data['id'])->queryAll();
			$data['websites'] = Yii::app()->db->createCommand('select id, address, label from crm_website where contact_id = '.$data['id'])->queryAll();
			$data['addresses'] = Yii::app()->db->createCommand('select * from crm_address where contact_id = '.$data['id'])->queryAll();
			$data['groups'] = Yii::app()->db->createCommand('select group_id as id from crm_group_contact where contact_id = '.$data['id'])->queryAll();
			$data['action'] = Yii::app()->db->createCommand('select * from crm_action where contact_id = '.$data['id']. ' and status = "TODO" order by id desc')->queryRow();
			$data['image_url'] = self::getImage($data);
			$data['url'] = $contact->getUrl();
			//Yii::app()->cache->set($cid, $data);
		}
		return $data;
	}
	
	/**
	 * Get the cache id used for caching the array representation of a contact record
	 * @return array
	 */
	public static function cacheIdToArray($contactId)
	{
		return "contact-toarray-$contactId";
	}
	
	/**
	 * Clear the 'toArray' cache
	 * @param int $contactId
	 */
	public static function cacheClearToArray($contactId)
	{
		Yii::app()->cache->delete(CrmContact::cacheIdToArray($contactId));
	}
	
	/**
	 * Gets the current action
	 */
	public function getAction()
	{
		return CrmAction::model()->getCurrentActionFor($this->id);
	}
	
	/**
	 * Filter search scope
	 * Filter records by the starting letter of the name
	 * @param string $letter 'all' (all letters (do nothing)), '0' starts with a number, 'a'-'z'
	 */
	public function filterLetter($letter='')
	{
		if ($letter == 'all' || $letter == '')
			return $this;
		if ($letter == '0') {
			$this->getDbCriteria()->mergeWith(array(
				'condition'=>'name REGEXP \'^[0-9]\'',
			));
		} else {
			$this->getDbCriteria()->mergeWith(array(
				'condition'=>'name like "'.$letter.'%"',
			));
		}
		return $this;
	}

	/**
	 * Group scope to sort by groups
	 * 
	 * @param mixed $group a string of 'all|contacts|companies|' or int group id
	 * @return \CrmContact
	 */
	public function filterGroup($group='')
	{
		// built in groups
		if ($group == 'contacts')
			return $this->contacts();
		if ($group == 'companies')
			return $this->companies();
		if ($group == 'all' || $group=='')
			return $this;
		if ($group != ''){
			// assume the group is the group id
			// get all members of the group
			$tblContact = $this->getTableAlias();
			$tblGroup = CrmGroupContact::model()->tableName();
			$this->getDbCriteria()->mergeWith(array(
				//SELECT * FROM `crmo_contact` WHERE id in (select contact_id from crmo_group_contact where group_id = 1)
				'condition'=>"$tblContact.id in (select contact_id from $tblGroup where group_id = :g)",
				'params'=>array(':g'=>$group),
			));
		}
		return $this;
	}
	
	/**
	 * Filter by owner id
	 * @param int $ownerId
	 */
	public function filterOwner($ownerId)
	{
		if (is_numeric($ownerId)) {
			$this->getDbCriteria()->mergeWith(array(
				'condition'=>'owner_id = :ownerId',
				'params'=>array(':ownerId'=>$ownerId)
			));
		}
		return $this;
	}
	
	/**
	 * Filter search scope
	 * Search a contact name
	 * @param string $search
	 * @return \CrmContact
	 */
	public function filterName($search='')
	{
		if ($search != '')
			$this->getDbCriteria()->mergeWith(array(
				'condition'=>'name like "%'.$search.'%"',
			));
		return $this;
	}
	
	public function filterHasPendingDeal()
	{
		$ta = $this->getTableAlias();
		$crmDealTable = CrmDeal::model()->tableName();
		$this->contacts()->getDbCriteria()->mergeWith(array(
			'join'=>"inner join $crmDealTable on $crmDealTable.contact_id = $ta.id",
			'condition'=>"$crmDealTable.status != '".CrmDeal::STATUS_LOST."' AND $crmDealTable.status != '".CrmDeal::STATUS_WON."'"
		));
		return $this;
	}

	public function filterHasPhone()
	{
		$ta = $this->getTableAlias();
		$this->contacts()->getDbCriteria()->mergeWith(array(
			'join'=>"inner join crm_phone on crm_phone.contact_id = $ta.id",
			'condition'=>'number is not null',
		));
		return $this;
	}
    
    public function filterPhoneNumber($number)
	{
        $phone = CrmApi::normalizePhoneNumber($number);
		$ta = $this->getTableAlias();
		$this->contacts()->getDbCriteria()->mergeWith(array(
			'join'=>"inner join crm_phone on crm_phone.contact_id = $ta.id",
			'condition'=>'number = "'.$phone.'"',
		));
		return $this;
	}

	public function filterType($type)
	{
		$criteria = $this->getDbCriteria();
		$criteria->compare($this->getTableAlias().'.type', $type, false);
	}
	
	/**
	 * Filter contacts with outstanding tasks TODO
	 * order by the due date, 
	 * @return \CrmContact
	 */
	public function filterTodo()
	{
		$ta = $this->getTableAlias();
		$this->contacts()->getDbCriteria()->mergeWith(array(
			'join'=>"inner join crm_action on crm_action.contact_id = $ta.id",
			'condition'=>'crm_action.status = \''.CrmAction::STATUS_TODO.'\'',
			'order'=>'crm_action.due ASC'
		));
		return $this;
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return NActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search() 
	{
		$criteria = $this->getDbCriteria();

		$criteria->compare('id', $this->id);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('company_id', $this->company_id, false);
		$criteria->compare('owner_id', $this->owner_id, false);
		$criteria->compare('status_id', $this->status_id, false);

		$criteria->order = 'name ASC';
		
		return new CActiveDataProvider('CrmContact', array(
			'criteria' => $criteria,
		));
	}
	
	/**
	 * Default scopes that can be applied to this model
	 * @return array
	 */
	public function scopes()
	{
		$ta = $this->getTableAlias();
		return array(
			'companies' => array(
				'condition'=>"$ta.type = '".self::TYPE_COMPANY."'",
			),
			'contacts' => array(
				'condition'=>"$ta.type = '".self::TYPE_CONTACT."'",
			),
			'actionsTodo' => array(
				'condition' => '',
				'with' => 'actions'
			)
		);
	}

	/**
	 * display a simple link
	 * @param CrmContact $contact
	 * @return string
	 */
	public static function htmlLink($contact=null)
	{
		if ($contact === null)
			return '';
		return NHtml::link($contact->name, NHtml::url($contact->route));
	}

	/**
	 * Override default implementation, to account for additional
	 * linked data, such as emails, phones, addresses etc
	 * 
	 * Note: This will save related data.
	 * 
	 * To add an an email you can do the following:
	 * ~~~
	 *     $contact->attributes = array(
	 *         'emails' => array(
	 *             array('email' => 'steve@newicon.net')
	 *         )
	 *     )
	 *	   $contact->save();
	 * ~~~
	 * The resulting save call will force cached relations to be refreshed see (toArray() function)
	 * 
	 * Supported linked models:
	 * - emails
	 * - addresses
	 * - websites
	 * - phones
	 * 
	 * !!!BE WANRED!!!
	 * The linked records are replaced, the existing linked records are deleted and the new ones added.
	 * 
	 * @param array $values
	 * @param boolean $safeOnly
	 */
	public function setAttributes($values, $safeOnly = true) 
	{
		// complete the normal assignment of safe attributes to the contact record
		parent::setAttributes($values, $safeOnly);

		// set name, the name field is a cached version of the cotacts full name, making searchin easier
		// and editing contacts easier.
		if (array_key_exists('name', $values))
			$this->setName($values['name']);
		
		// saved joined tables
		$joined = array(
			'emails'=>'CrmEmail',
			'addresses'=>'CrmAddress',
			'websites'=>'CrmWebsite',
			'phones'=>'CrmPhone'
		);
		
		foreach ($joined as $key=>$class) {
			if (array_key_exists($key, $values)) {
				// don't save contact as save will be called after set attributes
				CrmContact::model()->saveMany($values[$key], $this, $class);
			}
		}
		
		$this->updated_at = NTime::unixToDatetime();
	}
	
	public function rules()
	{
		return array(
			array('name, name_title, name_first, name_middle, name_last, name_suffix, company_id, company_role, gender, type, background, owner_id, status_id, kashflow_id', 'safe')
		);
	}
	
	public function behaviors()
	{
		return array(
//			'trashable'=>array(
//				'class'=>'nii.components.behaviors.NTrashBinBehavior',
//				'onAfterTrash'=>array($this, 'afterTrash')
//			),
//			'tag'=>array(
//				'class'=>'nii.components.behaviors.NTaggable'
//            ),
			'oldattributes'=>array(
				'class'=>'nii.components.behaviors.NOldAttributesBehavior'
			),
			'timestampable'=>array(
				'class'=>'nii.components.behaviors.NTimestampable'
            ),
			'activity'=>array(
				'class'=>'activity.behaviors.ActivityBehavior'
         )
		);
	}
	
	/**
	 * Pre save processing.
	 * Must return true to continue to save;
	 * @return boolean
	 */
	public function beforeSave()
	{
		if ($this->isCompany())
			$this->name_company = $this->name;
		self::cacheClearToArray($this->id);
		return parent::beforeSave();
	}
	
	public function afterSave()
	{
		if ($this->isNewRecord) {
			NActivityLog::add('crm', 'created', $this->toArray(), $this);
		}		
	}
	
	/**
	 * Event raised when the record has been trashed. (just before the trash changes are saved)
	 * @param CEvent $event
	 */
	public function afterTrash($event)
	{
		// we need to clear up relations when a model is trashed.
		// we do not want to see things that link to this record.
		// really we also need to restore these relations if the model is restored.
		// what a pain.
		
		Yii::app()->user->settings->set('after tash called', date('Y-m-d H:i:s'));
	}
	
	public function tableName()
	{
		return '{{crm_contact}}';
	}
	
	/**
	 * @param string $className
	 * @return CrmContact
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * DB Table schema
	 * @return array
	 */
	public function schema()
	{
		return array(
			'columns'=>array(
				'id'=>'pk',
				// file manager managed contact image
				'image_id'=>'int',
				// will always return the formated contact name
				'name'=>"string COMMENT 'Name of the contact, this always shows the full string of the contact name e.g. first middle last or the company name'",
				'name_title'=>"string COMMENT 'Title, Mr, Mrs etc'",
				'name_first'=>'string',
				'name_middle'=>'string',
				'name_last'=>'string',
				'name_suffix'=>'string',
				'name_company'=>"string COMMENT 'Company name this will mirror the name field for a company record it is useful for searching'",
				'gender' => "enum('M','F')",
				'company_id'=>"int COMMENT 'Represents the primary employment for a contact, or a sub company / department for a company'",
				'company_role'=>"string COMMENT 'The role working title for this contact, not relevent for companies'",
				'type'=>"enum('" . CrmContact::TYPE_CONTACT . "', '" . CrmContact::TYPE_COMPANY . "') NOT NULL DEFAULT '" . CrmContact::TYPE_CONTACT . "'  COMMENT 'A contact either represents a human or a place ".CrmContact::TYPE_CONTACT." for a person and ".CrmContact::TYPE_COMPANY." for a company'",
				'background'=>'text',
				'owner_id'=>"int COMMENT 'user id foreign key of the user that is responsible for this contact'",
				'status_id'=>"int COMMENT 'CrmStatus id foreign key'",
				'kashflow_id'=>"int COMMENT 'The kashflow id of this account'",
			),
			'keys'=>array(
				array('company_id'),
				array('owner_id'),
				array('status_id')
			)
		);
	}
}