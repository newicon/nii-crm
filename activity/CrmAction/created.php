<?php $contact = CrmContact::model()->findByPk($log->data['contact_id']) ?>
<i class="icon-inbox"></i>
Added a new action <br/>
<span class="label label-important">action</span> 
<?php if ($contact !== null): ?>
	<a href="<?php echo NHtml::url('/crm/index/index/') ?>#contact/<?php echo $contact->id ?>"><?php echo $contact->name; ?></a>
<?php endif; ?>:
<strong><?php echo $log->data['action'] ?></strong> <small>due:</small> <?php echo date('d/m/Y', strtotime($log->data['due'])); ?>