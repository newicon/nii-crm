<?php $contact = CrmContact::model()->findByPk($log->data['contact_id']) ?>
<i class="icon-inbox"></i>
Updated action <br/>
<?php $doneStyle = ''; ?>
<?php if ($log->data['status'] == 'DONE'): ?>
	<?php $doneStyle = 'text-decoration:line-through;font-weight:normal;' ?>
	<span class="label label-success">DONE</span>
<?php else: ?>
	<span class="label label-important">action</span> 
<?php endif; ?>
<?php if ($contact !== null): ?>
	<a href="<?php echo NHtml::url('/crm/index/index/') ?>#contact/<?php echo $contact->id ?>"><?php echo $contact->name; ?></a>
<?php endif; ?>:
<strong style="<?php echo $doneStyle ?>"><?php echo $log->data['action'] ?></strong> <small>due:</small> <span style="<?php echo $doneStyle ?>"><?php echo date('d/m/Y', strtotime($log->data['due'])); ?></span>