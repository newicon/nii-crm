<?php

/**
 * Nii class file.
 *
 * @author Newicon, Steven O'Brien <steven.obrien@newicon.net>
 * @link http://github.com/newicon/Nii
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */
?>
<i class="icon-inbox"></i>
Created contact <a href="<?php echo NHtml::url('/crm/index/index/') ?>#contact/<?php echo $log->data['id'] ?>"><?php echo $log->data['name'] ?></a>