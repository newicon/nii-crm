<i class="icon-inbox"></i>
<?php $contact = CrmContact::model()->findByPk($log->data['contact_id']) ?>
Added a new deal to 
<?php if ($contact !== null): ?>
	<a href="<?php echo NHtml::url('/crm/index/index/') ?>#contact/<?php echo $contact->id ?>"><?php echo $contact->name; ?></a>
<?php endif; ?>
<br />
<span class="label <?php echo 'BG_STAGE_' . $log->data['status']; ?>"><i class="icon-trophy"></i> Deal</span>
<strong class="text-danger">&pound;<?php echo number_format($log->data['amount']); ?></strong> - <strong class="<?php echo 'STAGE_'.$log->data['status']; ?>"><?php echo CrmDeal::getstatusLabel($log->data['status']) ?></strong> <small style="color:#999">chance to close on</small> <?php echo date('d/m/Y', strtotime($log->data['close_date'])); ?>
<br/><?php echo $log->data['name']; ?>