<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
CONVERTER!  Convert old contact module contacts to new CRM contacts (match id's)

<?php

$crmContacts = CrmContact::model()->findAll(array('order'=>'name'));
$data = CHtml::listData($crmContacts, 'id', 'name');
?>
<table class='table'>
	<tr>
		<th>Contact name</th>
		<th>Old id</th>
		<th>New Crm Contact</th>
	</tr>
	<?php foreach(Contact::model()->findAll(array('order'=>'name')) as $contact): ?>
		<tr>
			<td>
				<?php if ($contact->contact_type == 'Person'): ?>
					<?php echo $contact->givennames; ?> <?php echo $contact->lastname; ?>
				<?php else: ?>
					<?php echo $contact->company_name ?>
				<?php endif;?>
			</td>
			<td><?php echo $contact->id; ?></td>
			<td>
				<?php echo CHtml::dropDownList('update-'.$contact->id, $contact->id, $data, array('class'=>'select2', 'data-contact-id'=>$contact->id)) ?>
			</td>
		</tr>
	<?php endforeach; ?>
	
</table>

<?php $this->createWidget('crm.widgets.lookup.CrmDd')->registerScript(); ?>

<script>
$('.select2').select2().on('change', function(){
	var $el = $(this);
	var data = {
		contact_id:$el.attr('data-contact-id'),
		crm_id:$el.val()
	}
	// Update
	$.post("<?php echo NHtml::url('crm/dev/utils/converter') ?>", data, function(response){
		$el.parent().append(response);
	})
});	
</script>