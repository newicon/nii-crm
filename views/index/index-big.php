<?php

/**
 * Nii class file.
 *
 * @author Newicon, Steven O'Brien <steven.obrien@newicon.net>
 * @link http://github.com/newicon/Nii
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */
?>

<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.1.5/angular.js"></script>

<style>
	.block-grid li{display:block;float:left;list-style: none;padding-right:15px;}
	.btn.btn-letter{border-radius:4px;}
	.drop-hover{background-color:#eee;}
	.crm-c.selected .crm-card{background-color:#000099}
	.crm-c.selected.dragging{opacity:0.2}
	#feedback { font-size: 1.4em; }
</style>


<?php 
// quick cache
$upAt = Yii::app()->db->createCommand('select max(updated_at) from crm_contact')->queryScalar();
$contacts = Yii::app()->cache->get('crm-contacts-cache-1'.$upAt);

if (!$contacts) {
	$contacts1 = json_encode(NData::toAttrArray(CrmContact::model()->findAll(array('order'=>'name', 'limit'=>3000, 'offset'=>0)), 'toArraySmall'));
	$contacts2 = json_encode(NData::toAttrArray(CrmContact::model()->findAll(array('order'=>'name', 'limit'=>3000, 'offset'=>3000)), 'toArraySmall'));
	$contacts3 = json_encode(NData::toAttrArray(CrmContact::model()->findAll(array('order'=>'name', 'limit'=>3000, 'offset'=>6000)), 'toArraySmall'));
	$contacts4 = json_encode(NData::toAttrArray(CrmContact::model()->findAll(array('order'=>'name', 'limit'=>3000, 'offset'=>9000)), 'toArraySmall'));
	$contacts5 = json_encode(NData::toAttrArray(CrmContact::model()->findAll(array('order'=>'name', 'limit'=>3000, 'offset'=>12000)), 'toArraySmall'));
	Yii::app()->cache->set('crm-contacts-cache-1'.$upAt, $contacts1);
	Yii::app()->cache->set('crm-contacts-cache-2'.$upAt, $contacts2);
	Yii::app()->cache->set('crm-contacts-cache-3'.$upAt, $contacts3);
	Yii::app()->cache->set('crm-contacts-cache-4'.$upAt, $contacts4);
	Yii::app()->cache->set('crm-contacts-cache-5'.$upAt, $contacts5);
}


?>

<?php $groups = json_encode(NData::toAttrArray(CrmGroup::model()->findAll(array('order'=>'name')))); ?>

<div ng-app="CrmApp" ng-controller="CrmApp.listContacts" style="padding-left:10px;">
	
	<div class="line">
		
		<div class="unit" style="width:150px;">
			<div class="group"><a ng-class="filterGroup == 'all' ? 'btn btn-link active' : 'btn btn-link'" ng-click="selectGroup('all')" >All</a></div>
			<div class="group"><a ng-class="filterGroup == 'contacts' ? 'btn btn-link active' : 'btn btn-link'" ng-click="selectGroup('contacts')" >People</a></div>
			<div class="group"><a ng-class="filterGroup == 'companies' ? 'btn btn-link active' : 'btn btn-link'" ng-click="selectGroup('companies')" >Organisations</a></div>
			<div droppable="group.id" ng-repeat="group in groups" class="group">
				<a ng-class="filterGroup == group.id ? 'btn btn-link active' : 'btn btn-link'" ng-click="selectGroup(group.id)" >{{group.name}}</a>
			</div>
		</div>
		<div class="lastUnit">
			
			<div class="controls mbm">
				
				<div class="pull-right">
					<a href="<?php echo CrmRoute::getUrlOpportunityList() ?>" class="btn btn-small mrm"><i class="icon-briefcase"></i> Opportunities</a>
					<div class="btn-group">
						<a href="<?php echo CrmRoute::getUrlAddContact() ?>" class="btn btn-small"><i class="icon-user"></i> Add Contact</a>
						<a href="<?php echo CrmRoute::getUrlAddCompany() ?>" class="btn btn-small"><i class="icon-home"></i> Add Organisation</a>
					</div>
				</div>

				<div class="btn-group mrs mln" data-toggle="buttons-radio">
					<button ng-click="toggleGridFormat('grid')" class="btn btn-mini active"><i class="icon-th-large"></i></button>
					<button ng-click="toggleGridFormat('list')" class="btn btn-mini"><i class="icon-list"></i></button>
				</div>

				<!--<input ng-model="search" placeholder="Search..." type="text" style="font-size:12px;margin-bottom:2px;padding:3px;" />-->
				<input ng-model="query" ng-change="goSearch()" placeholder="Search..." type="text" style="font-size:12px;margin-bottom:2px;padding:3px;" />

				<i class="muted" ng-cloak style="white-space:nowrap;">{{filteredItems.length}} contacts</i>

				<br/>

				<div class="btn-group" data-toggle="buttons-radio">
					<button ng-click="letterSearch('all')"  class="btn btn-mini btn-link active btn-letter">All</button>
					<button ng-click="letterSearch('0')" class="btn btn-mini btn-link btn-letter">#</button>
					<?php foreach(range('a', 'z') as $letter): ?>
						<button ng-click="letterSearch('<?php echo $letter; ?>')" class="btn btn-mini btn-link btn-letter"><?php echo $letter; ?></button>
					<?php endforeach; ?>
				</div>

			</div>
			
			<i class="muted" ng-cloak>{{listMessage}}</i>

			<div class="line">
				<div class="pagination pull-left">
					<ul>
						<li ng-class="{disabled: currentPage == 0}">
							<a href ng-click="prevPage()">« Prev</a>
						</li>
<!--						<li ng-repeat="n in range(pagedItems.length)"
							ng-class="{active: n == currentPage}"
						ng-click="setPage()">
							<a href ng-bind="n + 1">1</a>
						</li>-->
						<li ng-class="{disabled: currentPage == pagedItems.length - 1}">
							<a href ng-click="nextPage()">Next »</a>
						</li>
					</ul>
				</div>
			</div>
			
			{{currentPage}} of {{pagedItems.length}}
			
			<div ng-switch on="gridFormat"  id="crm-contacts">

				<div ng-switch-when="list">
					<ul class="man ">
						<li ng-repeat="contact in pagedItems[currentPage] | filter:search" ng-class="contact.selected ? 'crm-c selected' : 'crm-c'" ng-click="selectContact(contact)" draggable="contact"  ng-cloak>
							<div class="crm-card" style="text-overflow:ellipsis;overflow:hidden;margin-bottom:15px;">
								<div class="media">
									<div class="pull-left">
										<a ng-href="{{contact.url}}"><img width="40" ng-src="{{contact.imageUrl}}"></a>
									</div>
									<div class="media-body">
										<a ng-href="{{contact.url}}"><strong>{{contact.name}}</strong></a> <br>
										<a ng-show="{{contact.company}}" ng-href="{{contact.company.url}}"><img width="16" ng-src="{{contact.company.imageUrlThumb24}}"></a> <a ng-show="{{contact.company}}" ng-href="{{contact.company.url}}">{{contact.company.name}} <br/></a>
										{{contact.emails[0].email}}
									</div>
								</div>
							</div>
						</li>
					</ul>
				</div>

				<div ng-switch-when="grid">
					<ul class="block-grid man">
						<li ng-repeat="contact in pagedItems[currentPage] | filter:search" ng-class="contact.selected ? 'crm-c selected' : 'crm-c'" ng-click="selectContact(contact)" draggable="contact" ng-cloak>
							<div class="well well-transparent crm-card" style="width:200px;height:40px;text-overflow:ellipsis;overflow:hidden;margin-bottom:15px;">
								<div class="media">
									<div class="pull-left">
										<a ng-href="{{contact.url}}"><img width="40" ng-src="{{contact.imageUrl}}"></a>
									</div>
									<div class="media-body">
										<a ng-href="{{contact.url}}"><strong>{{contact.name}}</strong></a> <br>
										<a ng-href="{{contact.company.url}}"><img width="16" ng-src="{{contact.company.imageUrlThumb24}}"></a> <a ng-href="{{contact.company.url}}">{{contact.company.name}}</a>
									</div>
									{{contact.selected}}
								</div>
							</div>
						</li>
					</ul>
				</div>

			</div>
		</div>
		
		
	</div>
	
	<button class="btn" ng-click="findContact()" >find</button>
	
</div>


	
<script>
	
	var CrmApp = angular.module('CrmApp', []);
	
	CrmApp.filter('getById', function() {
		return function(input, id) {
			var i=0, len=input.length;
			for (; i<len; i++) {
				if (+input[i].id == +id) {
					return input[i];
				}
			}
			return null;
		}
	});
	
	var contacts1 = <?php echo Yii::app()->cache->get('crm-contacts-cache-1'.$upAt); ?>;
	var contacts2 = <?php echo Yii::app()->cache->get('crm-contacts-cache-2'.$upAt); ?>;
	var contacts3 = <?php echo Yii::app()->cache->get('crm-contacts-cache-3'.$upAt); ?>;
	var contacts4 = <?php echo Yii::app()->cache->get('crm-contacts-cache-4'.$upAt); ?>;
	var contacts5 = <?php echo Yii::app()->cache->get('crm-contacts-cache-5'.$upAt); ?>;
	
	function combine() {
		var ar = [];
		return ar.concat.apply(ar, arguments).sort(function (a, b) {
			var aName = a.NAME;
			var bName = b.NAME;
			if (aName < bName) {
				return -1;
			} else if (aName == bName) {
				return 0;
			} else {
				return 1;
			};
		});
	};
	
	CrmApp.listContacts = function($scope, $filter) {
		
		
		
		$scope.contacts = combine(contacts1, contacts2, contacts3, contacts4, contacts5, contacts1, contacts2, contacts3, contacts4, contacts5, contacts1, contacts2, contacts3, contacts4, contacts5, contacts1, contacts2, contacts3, contacts4, contacts5,contacts1, contacts2, contacts3, contacts4, contacts5, contacts1, contacts2, contacts3, contacts4, contacts5, contacts1, contacts2, contacts3, contacts4, contacts5, contacts1, contacts2, contacts3, contacts4, contacts5, contacts1, contacts2, contacts3, contacts4, contacts5, contacts1, contacts2, contacts3, contacts4, contacts5, contacts1, contacts2, contacts3, contacts4, contacts5, contacts1, contacts2, contacts3, contacts4, contacts5,contacts1, contacts2, contacts3, contacts4, contacts5, contacts1, contacts2, contacts3, contacts4, contacts5, contacts1, contacts2, contacts3, contacts4, contacts5, contacts1, contacts2, contacts3, contacts4, contacts5,contacts1, contacts2, contacts3, contacts4, contacts5, contacts1, contacts2, contacts3, contacts4, contacts5, contacts1, contacts2, contacts3, contacts4, contacts5, contacts1, contacts2, contacts3, contacts4, contacts5,contacts1, contacts2, contacts3, contacts4, contacts5, contacts1, contacts2, contacts3, contacts4, contacts5, contacts1, contacts2, contacts3, contacts4, contacts5, contacts1, contacts2, contacts3, contacts4, contacts5, contacts1, contacts2, contacts3, contacts4, contacts5, contacts1, contacts2, contacts3, contacts4, contacts5, contacts1, contacts2, contacts3, contacts4, contacts5, contacts1, contacts2, contacts3, contacts4, contacts5,contacts1, contacts2, contacts3, contacts4, contacts5, contacts1, contacts2, contacts3, contacts4, contacts5, contacts1, contacts2, contacts3, contacts4, contacts5, contacts1, contacts2, contacts3, contacts4, contacts5, contacts1, contacts2, contacts3, contacts4, contacts5, contacts1, contacts2, contacts3, contacts4, contacts5, contacts1, contacts2, contacts3, contacts4, contacts5, contacts1, contacts2, contacts3, contacts4, contacts5,contacts1, contacts2, contacts3, contacts4, contacts5, contacts1, contacts2, contacts3, contacts4, contacts5, contacts1, contacts2, contacts3, contacts4, contacts5, contacts1, contacts2, contacts3, contacts4, contacts5, contacts1, contacts2, contacts3, contacts4, contacts5, contacts1, contacts2, contacts3, contacts4, contacts5, contacts1, contacts2, contacts3, contacts4, contacts5, contacts1, contacts2, contacts3, contacts4, contacts5,contacts1, contacts2, contacts3, contacts4, contacts5, contacts1, contacts2, contacts3, contacts4, contacts5, contacts1, contacts2, contacts3, contacts4, contacts5, contacts1, contacts2, contacts3, contacts4, contacts5,contacts1, contacts2, contacts3, contacts4, contacts5, contacts1, contacts2, contacts3, contacts4, contacts5, contacts1, contacts2, contacts3, contacts4, contacts5, contacts1, contacts2, contacts3, contacts4, contacts5,contacts1, contacts2, contacts3, contacts4, contacts5, contacts1, contacts2, contacts3, contacts4, contacts5, contacts1, contacts2, contacts3, contacts4, contacts5, contacts1, contacts2, contacts3, contacts4, contacts5, contacts1, contacts2, contacts3, contacts4, contacts5, contacts1, contacts2, contacts3, contacts4, contacts5, contacts1, contacts2, contacts3, contacts4, contacts5, contacts1, contacts2, contacts3, contacts4, contacts5,contacts1, contacts2, contacts3, contacts4, contacts5, contacts1, contacts2, contacts3, contacts4, contacts5, contacts1, contacts2, contacts3, contacts4, contacts5, contacts1, contacts2, contacts3, contacts4, contacts5, contacts1, contacts2, contacts3, contacts4, contacts5, contacts1, contacts2, contacts3, contacts4, contacts5, contacts1, contacts2, contacts3, contacts4, contacts5, contacts1, contacts2, contacts3, contacts4, contacts5, contacts1, contacts2, contacts3, contacts4, contacts5, contacts1, contacts2, contacts3, contacts4, contacts5, contacts1, contacts2, contacts3, contacts4, contacts5, contacts1, contacts2, contacts3, contacts4, contacts5, contacts1, contacts2, contacts3, contacts4, contacts5,contacts1, contacts2, contacts3, contacts4, contacts5, contacts1, contacts2, contacts3, contacts4, contacts5, contacts1, contacts2, contacts3, contacts4, contacts5, contacts1, contacts2, contacts3, contacts4, contacts5, contacts1, contacts2, contacts3, contacts4, contacts5, contacts1, contacts2, contacts3, contacts4, contacts5, contacts1, contacts2, contacts3, contacts4, contacts5, contacts1, contacts2, contacts3, contacts4, contacts5);
		//$scope.contacts = combine($scope.contacts, $scope.contacts);

		$scope.groups = <?php echo $groups; ?>;
		
		$scope.listMessage = '';
		
		// default search criteria
		$scope.filterGroup = 'all';
		$scope.filterLetter = 'all';
		
		$scope.gridFormat = 'grid';
		$scope.toggleGridFormat = function(type){
			$scope.gridFormat = type
		}
		
		$scope.letterSearch = function(letter){
			$scope.filterLetter = letter;
			$scope.goSearch();
			//$scope.searchy();
		}
		
		$scope.selectGroup = function(groupId){
			$scope.filterGroup = groupId;
			$scope.searchy();
		}
		
		// display a 'no contacts' message if the contacts array is empty
		$scope.$watch('contacts', function() {$scope.listMessage = ($scope.contacts.length == 0) ?  'No contacts found' : '';});
		
		$scope.searchUrl = function(){
			return "<?php echo NHtml::url('/crm/index/search/'); ?>?group="+$scope.filterGroup+'&letter='+$scope.filterLetter;
		}

		$scope.searchy = function(){
			$.post($scope.searchUrl(), function(json){
				$scope.contacts = json; $scope.$apply();
			}, 'json');
		}
		
		
		$scope.selectContact = function(contact){
			contact.selected = contact.selected ? false : true;
		}
		
		$scope.getSelected = function(){
			return _.filter($scope.contacts, function(contact){
				return contact.selected;
			});
		}
		
		$scope.getContactById = function(id){
			return _.find($scope.contacts, function(c) {
				return c.id == id;
			});
		}
		
		$scope.findByLetter = function(contact){
			if ($scope.filterLetter == 'all')
				return true;
			if ($scope.filterLetter == '0')
				return _.isNumber(contact.name[0]);
			return contact.name[0].toLowerCase() == $scope.filterLetter;
		}


		// experimental pagination
		$scope.itemsPerPage = 30;
		$scope.pagedItems = [];
		$scope.currentPage = 0;
		
		
		var searchMatch = function (haystack, needle) {
			if (!needle) {
				return true;
			}
			return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
		};
		
		// init the filtered items
		$scope.goSearch = function () {
			$scope.filteredItems = $filter('filter')($scope.contacts, function (contact) {
				if (searchMatch(contact.name, $scope.query))
						return true;
				if (!_.isNull(contact.company))
					if (searchMatch(contact.company.name, $scope.query))
						return true;
				return false;
			});
			
			$scope.filteredItems = $filter('filter')($scope.filteredItems, function (contact) {
				return $scope.findByLetter(contact);
				//return false;
			});
			// take care of the sorting order
			if ($scope.sortingOrder !== '') {
				$scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
			}
			$scope.currentPage = 0;
			// now group by pages
			$scope.groupToPages();
		};
		
		$scope.groupToPages = function () {
			$scope.pagedItems = [];

			for (var i = 0; i < $scope.filteredItems.length; i++) {
				if (i % $scope.itemsPerPage === 0) {
					$scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [ $scope.filteredItems[i] ];
				} else {
					$scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push($scope.filteredItems[i]);
				}
			}
		};
		
		$scope.range = function (start, end) {
			var ret = [];
			if (!end) {
				end = start;
				start = 0;
			}
			for (var i = start; i < end; i++) {
				ret.push(i);
			}
			return ret;
		};

		$scope.prevPage = function () {
			if ($scope.currentPage > 0) {
				$scope.currentPage--;
			}
		};

		$scope.nextPage = function () {
			if ($scope.currentPage < $scope.pagedItems.length - 1) {
				$scope.currentPage++;
			}
		};

		$scope.setPage = function () {
			$scope.currentPage = this.n;
		};
		
		$scope.goSearch();
		
	}
	
	CrmApp.directive('draggable', function() {
		return {
			// A = attribute, E = Element, C = Class and M = HTML Comment
			restrict:'A',
			//The link function is responsible for registering DOM listeners as well as updating the DOM.
			link: function(scope, element, attrs) {
				attrs.$observe('draggable', function(val) {
					element.contact = scope.$eval(val);
					element.draggable({
						revert:'invalid',
						zIndex: 100,
						cursorAt:{left: 0, top: 15},
						start:function(event, ui){
							// dragging has started add class to indicate all the draggables that are being dragged
							// this refers to the element being dragged (not the helper the actual element)
							$('#crm-contacts').find('.crm-c.selected').addClass('dragging');
						},
						helper:function(){
							
							element.contact.selected = true;
							$(this).addClass('selected');
							
							var count = scope.getSelected().length;
							// select a nice image
							// be very cool to use actual images...
							var img = 'drag-contact-5';
							if(count>0 && count<6){
								img = 'drag-contact-'+count;
							}
							var helperHtml = '<div class="dragHelper"><img class="img" src="<?php echo Yii::app()->getModule('crm')->getAssetsUrl(); ?>/images/'+img+'.png" />';
							helperHtml += '<span class="badge">'+count+'</span></div>';
							return $(helperHtml).appendTo('body').get()
						},
						stop:function(){
//							element.contact.selected = false;
							//
							//$('#crm-contacts').find('.crm-c.selected').removeClass('selected');
							$('#crm-contacts').find('.crm-c.dragging').removeClass('dragging');
							scope.$apply();
						}
					});
				});
			}
		};
	});
	
	CrmApp.directive('droppable', function($compile) {
		return {
			restrict: 'A',
			link: function(scope,element,attrs){
				attrs.$observe('droppable', function(val) {
					element.groupId = scope.$eval(val);
					element.droppable({
						greedy:true,
						hoverClass: "drop-hover",
						drop:function(event,ui) {
							var contacts = [];
							_.each(scope.getSelected(), function(c){
								c.selected = false;
								contacts[contacts.length] = c.id;
								scope.$apply();
							});
							$.post("<?php echo NHtml::url('/crm/index/addToGroup'); ?>",{"groupId":element.groupId,"contacts":contacts},function(r){
								$group.effect("highlight", {}, 1500);
								$group.find('.icon').removeClass('fam-hourglass').addClass('fam-vcard');
							});
							scope.$apply();
						}
					});
				});
			}
		}
    });
	
</script>