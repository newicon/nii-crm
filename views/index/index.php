<link href="<?php echo Yii::app()->getModule('crm')->assetsUrl; ?>/styles/flags.css" rel="stylesheet" type="text/stylesheet"></link>
<link href="<?php echo Yii::app()->getModule('crm')->assetsUrl; ?>/vendor/colorpicker/css/colorpicker.css" rel="stylesheet" type="text/stylesheet"></link>
<!--<script src="<?php echo Yii::app()->getModule('crm')->assetsUrl ?>/bower_components/angular/angular.js"></script>
-->
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.0/angular.min.js"></script>
<script src="<?php echo Yii::app()->getModule('crm')->assetsUrl ?>/bower_components/angular-sanitize/angular-sanitize.min.js"></script>
<script src="<?php echo Yii::app()->getModule('crm')->assetsUrl ?>/vendor/angular/angular-resource.min.js"></script>
<script src="<?php echo Yii::app()->getModule('crm')->assetsUrl ?>/vendor/angular/angular.ui.router.min.js"></script>
<script src="<?php echo Yii::app()->getModule('crm')->assetsUrl ?>/vendor/angular/angular-collection.js"></script>
<script src="<?php echo Yii::app()->getModule('crm')->assetsUrl ?>/vendor/jquery-collision.js"></script>
<?php 
    Yii::beginProfile('json encode initial data');
    $data = array(
        json_encode(NData::toAttrArray(CrmGroup::model()->findAll(array('order' => 'name')), 'toArray')),
        json_encode(CrmStatus::getStatusList()),
        json_encode(NData::toAttrArray(User::model()->findAllByAttributes(array('status'=>1)))),
        CrmContact::model()->contacts()->count(),  // can be cached
        CrmContact::model()->companies()->count(), // can be cached
        CrmApi::countOwner()
    );
    Yii::endProfile('json encode initial data');
    list($groups, $status, $users, $contactsCount, $companiesCount, $myCount) = $data;
?>
<div id="crm" ng-app="CrmApp" class="app" ng-controller="app" >
	
	<div class="col crm-nav">
		<a href="#/actions" class="icon-menu-item" ng-class="{active: contacts.filters.todo}">
			<i class="fa fa-calendar"></i><br/>
			<span>Actions</span>
		</a>
		<a href="#/contacts" class="icon-menu-item" ng-click="removeTodoFilter()" ng-class="(state.includes('contacts') && !contacts.filters.todo) ? 'active' : ''">
			<i class="fa fa-user"></i><br/>
			<span>Contacts</span>
		</a>
		<a href="#/pipeline" class="icon-menu-item" ng-class="{active: state.includes('pipeline')}">
			<i class="fa fa-filter"></i><br/>
			<span>Pipeline</span>
		</a>
		<a href="#/integration" style="position:absolute;left:10px;bottom:20px;" class="icon-menu-item" ng-class="{active: state.includes('integration')}">
			<i class="fa fa-cog"></i><br/>
			<span>Integration</span>
		</a>
	</div>
	
	<div class="" style="">
		
		<!-- needs tidying up... One UI view would make sense. With the panels as directives
			 within the view -->
		<!-- Group & Filters Column -->
		<div class="col crm-group" ng-show="state.includes('contacts')">
			<div ui-view="groups"></div>
		</div>
		<!-- Contacts List -->
		<div class="col crm-list" ng-show="state.includes('contacts')">
			<div ui-view="list"></div>
		</div>
		<!-- Contact Details -->
		<div class="col crm-detail" ng-show="state.includes('contacts')">
			<div class="ng-cloak ng-cloak-show">Loading...</div>
			<div ui-view="crmdetails"></div>
		</div>
		
		<div class="col" style="left:80px;right:0;" ng-show="state.includes('pipeline') || state.includes('integration')">
			<div class="ng-cloak ng-cloak-show">Loading...</div>
			<div ui-view="pipeline"></div>
		</div>
	</div>
	
</div>
<script>
	var CrmApp = angular.module('CrmApp', ["Andalay", "ui.state", "colorpicker.module", "ngResource", "ngCollection", "ngSanitize"]);
	CrmApp.constant('assetUrl', '<?php echo Yii::app()->getModule('crm')->assetsUrl ?>');
	CrmApp.constant('baseUrl', "<?php echo NHtml::url('') ?>");
	CrmApp.constant('userId', "<?php echo Yii::app()->user->id; ?>");
	CrmApp.constant('DEBUG', "<?php echo YII_DEBUG; ?>");
	// superseded by angular baseUrl constant
	CrmApp.baseUrl = "<?php echo NHtml::url('') ?>";
	// superseded by angular assetsUrl constant
	CrmApp.assetsUrl = "<?php echo Yii::app()->getModule('crm')->assetsUrl; ?>";
	// superseded by angular userId constant
	CrmApp.user_id = <?php echo Yii::app()->user->id; ?>;
	CrmApp.run(function(Contacts, Groups, Status, Users) {
		Groups.reset(<?php echo $groups; ?>);
		Status.reset(<?php echo $status; ?>);
		Users.reset(<?php echo $users; ?>)
		CrmApp.emailLabels = <?php echo CrmEmail::getLabelsJs() ?>;
		CrmApp.phoneLabels = <?php echo CrmPhone::getLabelsJs() ?>;
		CrmApp.websiteLabels = <?php echo CrmWebsite::getLabelsJs() ?>;
		CrmApp.addressLabels = <?php echo CrmAddress::getLabelsJs(); ?>;
		CrmApp.companiesCount = <?php echo  $companiesCount ?>;
		CrmApp.contactsCount = <?php echo $contactsCount; ?>;
		Contacts.collection.count.ownedByMe = <?php echo $myCount; ?>;
	});
	CrmApp.getInjector = function(){
		return injector = angular.element(document.body).injector();
	};
	CrmApp.getService = function(serviceName){
		return cultivateFundraiser.getInjector().get(serviceName);
	};
	// Create a url from a route
	// will prepend the base url
	// format a query sting from a passed params object
	CrmApp.url = function(route, params) {
		// add a beggining slash on the route if one does not exist
		route = (route[0] != '/') ? '/' + route : route;
		// create query string if presant
		params = params ? '?' + $.param(params) : '';
		return CrmApp.baseUrl + route + params;
	};
	CrmApp.config(function($stateProvider, $urlRouterProvider, $locationProvider){
		//$locationProvider.html5Mode(true);
		// For any unmatched url, send to /
		$urlRouterProvider.otherwise("/")
		// Now set up the states
		$stateProvider
			.state('contacts', {
				url: "/",
				views: {
					// means: render in the ui-view "crmdetails" @ "in" the parent view (a view not specified defaults to parent)
					"groups@" : {
						templateUrl: CrmApp.assetsUrl + "/views/contact.groups.html",
						controller:CrmApp.groupCtrl
					},
					"list@" : {
						templateUrl: CrmApp.assetsUrl + "/views/contact.list.html",
						controller:CrmApp.crmListCtrl
					},
					"crmdetails@" : {
						templateUrl: CrmApp.assetsUrl + "/views/contact.details.html",
						controller:CrmApp.contactCtrl
					}
				},
				resolve:{
					Contacts:function(Contacts){
						if (Contacts.collection.length==0)
							Contacts.find();
						return Contacts;
					}
				}
			})
			.state('contacts.actions', {
				url: "actions",
				// should change the group controller 
				// to expect a filter value that we can "resolve" and set the filters of the Contacts for each page load.
				resolve:{
					Contacts:function(Contacts){
						Contacts.find({todo:1}, true);
						return Contacts;
					}
				}
			})
			.state('contacts.contact', {
				url: "contact/:contactId",
				views: {
					"crmdetails@" : {
						templateUrl: CrmApp.assetsUrl + "/views/contact.details.html",
						controller:CrmApp.contactCtrl
					}
				}
			})
			.state('contacts.my', {
				url: "my/contacts",
				// should change the group controller 
				// to expect a filter value that we can "resolve" and set the filters of the Contacts for each page load.
				resolve:{
					Contacts:function(Contacts){
						Contacts.find({owner_id:CrmApp.user_id})
						return Contacts;
					}
				}
			})
			.state('contacts.tag', {
				url: "tag/:groupName",
				// should change the group controller 
				// to expect a filter value that we can "resolve" and set the filters of the Contacts for each page load.
				resolve:{
					Contacts:function(Contacts, $stateParams, Groups){
						var group = Groups.find('name', $stateParams.groupName);
						var groupId = _.isUndefined(group) ? $stateParams.groupName : group.id;
						Contacts.find({group:groupId});
						return Contacts;
					}
				}
			})
			.state('contacts.status', {
				url: "status/:statusName",
				// should change the group controller 
				// to expect a filter value that we can "resolve" and set the filters of the Contacts for each page load.
				resolve:{
					Contacts:function(Contacts, $stateParams, Status){
						if (Contacts.current !== null) {
							$stateParams.contactId = Contacts.current.id;
						}
						var status = Status.find('name', $stateParams.statusName);
						var search = _.isUndefined(status) ? $stateParams.statusName : status.id;
						Contacts.find({status_id:search});
						return Contacts;
					}
				}
			})
			.state('contacts.contacts', {
				url: "contacts",
				// should change the group controller 
				// to expect a filter value that we can "resolve" and set the filters of the Contacts for each page load.
				resolve:{
					Contacts:function(Contacts){
						Contacts.find({type:'contact'});
						return Contacts;
					}
				}
			})
			.state('contacts.companies', {
				url: "companies",
				// should change the group controller 
				// to expect a filter value that we can "resolve" and set the filters of the Contacts for each page load.
				resolve:{
					Contacts:function(Contacts){
						Contacts.find({type:'company'});
						return Contacts;
					}
				}
			})
			.state('contacts.edit', {
				url: "edit/:contactId",
				views: {
					"crmdetails@" : {
						templateUrl: CrmApp.assetsUrl + "/views/contact.edit.html",
						controller:CrmApp.contactEditCtrl
					}
				}
			})
			.state('pipeline', {
				url: "/pipeline",
				views: {
					"pipeline@" : {
						templateUrl: CrmApp.assetsUrl + "/deals-pipeline/pipeline.html",
						controller: 'pipeline'
					}
				}
			})
			.state('integration', {
				url: "/integration",
				views: {
					"pipeline@" : {
						templateUrl: CrmApp.assetsUrl + "/views/integration.html",
						controller: function(){
							
						}
					}
				}
			});
	});

	CrmApp.controller('app', function($scope, $state, Contacts) {
		$scope.state = $state;
		$scope.contacts = Contacts;
		$scope.removeTodoFilter = function(){
			if (Contacts.filters.todo) {
				Contacts.collection.removeAll();
			}
			delete Contacts.filters['todo'];
		};
	});
	
	CrmApp.filter("sanitize", ['$sce', function($sce) {
		return function(htmlCode){
			return $sce.trustAsHtml(htmlCode);
		};
	}]);

	CrmApp.filter("mysqlDate", ['$filter', function($filter) {
		return function(input, format, timezone){
			var date = moment(input, 'YYYY-MM-DD HH:mm:ss').toDate();
			return $filter('date')(date, format, timezone);
		};
	}]);

	$(function() {
		$crmGroup = $(".crm-group");
		$letterFixed = $(".letter-fixed");
		$crmList = $(".crm-list");
		$crmDetail = $('.crm-detail');
		$crmList.resizable({
			handles:"e",
			minWidth:200,
			resize:function(){resizeGui()}
		});
		$crmGroup.resizable({
			handles:"e",
			minWidth:100,
			resize:function(){resizeGui()}
		});

		var resizeGui = function(){
			var borderWidth = 1;
			$crmGroup.css('height','');
			$(".letter-fixed").width($crmList.width()-20);
			$crmList.css('left',  $crmGroup.position().left + $crmGroup.width() + borderWidth + 'px').css('height','');
			$crmDetail.css('left',$crmList.position().left + $crmList.width() + borderWidth + 'px');
		};
	});
</script>
<?php $this->createWidget('nii.widgets.select2.Select2')->registerScripts() ?>
<script src="<?php echo Yii::app()->getModule('crm')->assetsUrl ?>/bower_components/angular-underscore-module/angular-underscore-module.js"></script>
<script src="<?php echo Yii::app()->getModule('crm')->assetsUrl ?>/bower_components/andalay/src/andalay.js"></script>
<script src="<?php echo Yii::app()->getModule('crm')->assetsUrl ?>/deals-pipeline/deal-service.js"></script>
<script src="<?php echo Yii::app()->getModule('crm')->assetsUrl ?>/action/action.js"></script>
<script src="<?php echo Yii::app()->getModule('crm')->assetsUrl ?>/notes/notes.js"></script>
<script src="<?php echo Yii::app()->getModule('crm')->assetsUrl ?>/deals/contact-deal.js"></script>
<script src="<?php echo Yii::app()->getModule('crm')->assetsUrl ?>/directives/directives.js"></script>
<script src="<?php echo Yii::app()->getModule('crm')->assetsUrl ?>/directives/forms.js"></script>
<script src="<?php echo Yii::app()->getModule('crm')->assetsUrl ?>/bower_components/moment/min/moment.min.js"></script>
<script src="<?php echo Yii::app()->getModule('crm')->assetsUrl ?>/deals-pipeline/pipeline-controller.js"></script>
<script src="<?php echo Yii::app()->getModule('crm')->assetsUrl ?>/controllers/contactCtrl.js"></script>
<script src="<?php echo Yii::app()->getModule('crm')->assetsUrl ?>/controllers/crmListCtrl.js"></script>
<script src="<?php echo Yii::app()->getModule('crm')->assetsUrl ?>/controllers/groupCtrl.js"></script>
<script src="<?php echo Yii::app()->getModule('crm')->assetsUrl ?>/controllers/contactEditCtrl.js"></script>
<script src="<?php echo Yii::app()->getModule('crm')->assetsUrl ?>/service/contacts.js"></script>
<script src="<?php echo Yii::app()->getModule('crm')->assetsUrl ?>/service/groups.js"></script>
<script src="<?php echo Yii::app()->getModule('crm')->assetsUrl ?>/service/status.js"></script>
<script src="<?php echo Yii::app()->getModule('crm')->assetsUrl ?>/service/users.js"></script>
<script src="<?php echo Yii::app()->getModule('crm')->assetsUrl ?>/service/actions.js"></script>
<script src="<?php echo Yii::app()->getModule('crm')->assetsUrl ?>/vendor/colorpicker/lib/bootstrap-colorpicker.js"></script>
<script src="<?php echo Yii::app()->getModule('crm')->assetsUrl ?>/vendor/colorpicker/js/bootstrap-colorpicker-module.js"></script>