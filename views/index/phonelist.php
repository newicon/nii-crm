<style>
#topbar {
	position:fixed;
	width:100%;
	top:0;
	height:40px;
	background-color: #fff;
	padding: 5px;
}
#phonelist {
	list-style: none;
	width: 300px;
	margin:40px 0 0 0;
	border:0;
	padding:0;
}
#phonelist li {
	border-bottom: 1px solid #ccc;
	padding: 5px 8px;
	display:block;
	background-color: #f6f6f6;
}
#phonelist a {display:block;text-decoration: none; padding: 5px 8px;cursor: pointer;}
#phonelist .contact-link {
	margin: -5px -8px;
	padding: 5px 8px;
	background-color: #fff;
}
#phonelist .contact-link:hover,
#phonelist .contact-link.active {
	background-color: #eee
}
#phonelist small {color:#aaa;}
.phone-numbers {margin-top: 10px;}
</style>
<div ng-app="CrmApp" class="app" ng-controller="CrmApp.app" >
	<div ui-view="phonelist"></div>
</div>
<script src="<?php echo Yii::app()->getModule('crm')->assetsUrl ?>/vendor/angular/angular.1.1.5.min.js"></script>
<script src="<?php echo Yii::app()->getModule('crm')->assetsUrl ?>/vendor/angular/angular-sanitize.min.js"></script>
<script src="<?php echo Yii::app()->getModule('crm')->assetsUrl ?>/vendor/angular/angular-resource.min.js"></script>
<script src="<?php echo Yii::app()->getModule('crm')->assetsUrl ?>/vendor/angular/angular.ui.router.min.js"></script>
<script src="<?php echo Yii::app()->getModule('crm')->assetsUrl ?>/vendor/angular/angular-collection.js"></script>
<script>
var CrmApp = angular.module('CrmApp', ["ui.state", "ngResource", "ngCollection", "ngSanitize"]);

CrmApp.baseUrl = "<?php echo NHtml::url('') ?>";
CrmApp.assetsUrl = "<?php echo Yii::app()->getModule('crm')->assetsUrl; ?>";

CrmApp.phone = {
	host: '<?php echo $endpoint->deviceUrl ?>',
	endpoint: '<?php echo str_replace('sip:', '', $endpoint->endpoint);?>',
	deviceName: '<?php echo $endpoint->description?>' || 'default device'
};

CrmApp.run(function(Contacts, Groups, Status, Users) {
	Contacts.find({search_options:{'page_size':1000}}, true);
});

// Create a url from a route
// will prepend the base url
// format a query sting from a passed params object
CrmApp.url = function(route, params) {
	// add a beggining slash on the route if one does not exist
	route = (route[0] != '/') ? '/' + route : route;
	// create query string if presant
	params = params ? '?' + $.param(params) : '';
	return CrmApp.baseUrl + route + params;
};

CrmApp.app = function($scope, $state) {
	$scope.state=$state;
};

CrmApp.config(function($stateProvider, $urlRouterProvider, $locationProvider){
	//$locationProvider.html5Mode(true);
	// For any unmatched url, send to /
	$urlRouterProvider.otherwise("/")
	// Now set up the states
	$stateProvider
		.state('phonelist', {
			url: "/",
			views: {
				'phonelist' : {
					templateUrl: CrmApp.assetsUrl + "/views/contact.phonelist.html",
					controller:CrmApp.phoneListCtrl
				}
			}
		})
});

$(function(){

	$('#phonelist').on('click', '.contact-link', function(e){
		var contactId = $(e.currentTarget).attr('data-id');
		$('.contact-link:not([data-id='+contactId+'])').removeClass('active');
		$('.phone-numbers').addClass('hide');
		$(e.currentTarget).addClass('active');
		$('#phoneNumbers'+contactId).removeClass('hide');
	});

	$('#phonelist').on('click', '.phone-link', function(e){
		var number = $(e.currentTarget).attr('data-number');
		if (!confirm('Are you sure you want to call '+number+'?'))
			return false;
		var host = '<?php echo $endpoint->deviceUrl ?>';
		var endpoint = '<?php echo str_replace('sip:', '', $endpoint->endpoint);?>';
		var deviceName = '<?php echo $endpoint->description?>' || 'default device';
		var url = host+'command.htm?number=' + number +'&outgoing_uri='+endpoint;
		if (number.substr(0,2) == '44') {
			var phoneNumber = number.substr(2, number.length);
			window.location = '<?php echo NHtml::url(); ?>/call/index/webrtc?cc=44&number='+phoneNumber;
		} else {
			alert('Non UK number.  At the moment this only supports UK numbers starting with 44... Its really easy to do though if you fancy updating me :-)');
		}
	});

});
</script>
<script src="<?php echo Yii::app()->getModule('crm')->assetsUrl ?>/js/directives/directives.js"></script>
<script src="<?php echo Yii::app()->getModule('crm')->assetsUrl ?>/vendor/moment.min.js"></script>
<script src="<?php echo Yii::app()->getModule('crm')->assetsUrl ?>/js/controllers/phoneListCtrl.js"></script>
<script src="<?php echo Yii::app()->getModule('crm')->assetsUrl ?>/js/service/contacts.js"></script>
<script src="<?php echo Yii::app()->getModule('crm')->assetsUrl ?>/js/service/groups.js"></script>
<script src="<?php echo Yii::app()->getModule('crm')->assetsUrl ?>/js/service/status.js"></script>
<script src="<?php echo Yii::app()->getModule('crm')->assetsUrl ?>/js/service/users.js"></script>