<?php

/**
 * DavAuth class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 *
 */
class DavAuth extends Sabre\DAV\Auth\Backend\AbstractBasic
{
    protected $_identity;
 
    protected function validateUserPass($username, $password)
    {
        if ($this->_identity === null) {
            $this->_identity=new UserIdentity($username,$password);
        }
        return $this->_identity->authenticate() && $this->_identity->errorCode == UserIdentity::ERROR_NONE;
    }
}