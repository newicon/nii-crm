<?php

/**
 * RestController class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 */
class CrmRestController extends AController
{
	/**
	 * Turn off logging if this is an ajax call
	 * @param type $action
	 */
	protected function beforeAction($action)
	{
		$this->prepareAjaxResponse();
		return true;
	}
	
	/**
	 * enables actions to be defined specific to a model
	 * for example actionListProjectProject will not use the default actionList action
	 * instead it will use the actionListProjectProject when trying to list project models
	 * 
	 * @param type $actionID
	 * @return CInlineAction 
	 */
	public function createAction($actionID)
	{
		
		// overrides default action to provide a model (resource) specific action implementation
		if(isset($_REQUEST['model'])){
			$model = $_REQUEST['model'];
			$overideActionID = $model.'_'.$actionID;
			if(method_exists($this, 'action'.$overideActionID)){
				return new CInlineAction($this,$overideActionID);
			}
		}
		
		return parent::createAction($actionID);
	}
	
	/**
	 * @param string $model (string model class name or alias model name see self::modelLookup)
	 * @return json array of models
	 */
	public function actionList()
	{
		$m = $this->modelLookup();
		$res = CActiveRecord::model($m)->findAll();
		$this->response($res);
	}
	
	/**
	 * @param string $model (string model class name or alias model name see self::modelLookup)
	 * @param int $id 
	 * @return json model 
	 */
	public function actionRead($id)
	{
		$m = $this->modelLoad($id);
		$this->response($m);
	}
	
	/**
	 * Handle a RESTful POST request 
	 * by default post requests instruct the API to create new records / resources).
	 * @param string $model (string model class name or alias model name see self::modelLookup)
	 * @return json model returns the saved model on success
	 */
	public function actionCreate()
	{
		$className = $this->modelLookup();
		$m = new $className;
		if($m===null)
			throw new CHttpException (404,'no model found of type "'.$className.'"');

		$data = $this->getData();
		if (array_key_exists('id', $data) && $data['id'] != '') {
			// what the model contains and id.
			// So lets see if it already exists
			$exists = NActiveRecord::model($className)->findByPk($data['id']);
			if ($exists !== null)
				throw new CHttpException('A model already exists with this id "' . $data['id'] . '". You should call an update method, not create', 400);
		}

		$m->attributes = $data;

		$saved = $m->save();
		if ($saved) {
			$m->refresh();
			$this->response($m);
		} else {
			$this->response($m->getErrors(), 400);
		}
	}
	
	/**
	 * handle a RESTfull UPDATE request
	 * @param string $model (string model class name or alias model name see self::modelLookup)
	 * @param int $id id of the row
	 * @return json updated model
	 */
	public function actionUpdate($id)
	{
		$m = $this->modelLoad($id);
		$m->attributes = $this->getData();
		$saved = $m->save();
		$response = array();
		if ($saved) {
			$m->refresh();
			$this->response($m);
		} else {
			$this->response($m->getErrors(), 400);
		}
	}
	
	/**
	 * Handle RESTfull DELETE request
	 * @param string $model (string model class name or alias model name see self::modelLookup)
	 * @param int $id 
	 * @return void nothing, zero, zip
	 */
	public function actionDelete($id)
	{
		$m = $this->modelLoad($id);
		$deleted = $m->delete();
		$deleted ? $this->response('', 200) : $this->response('Could not delete', 400);
	}
	
	/**
	 * fetch a model based on id.
	 * @param string $model (string model class name or alias model name see self::modelLookup)
	 * @param int $id the model primary key
	 * @return NActiveRecord
	 * @throws CHttpException if no model exists with the id
	 * @return NActiveRecord
	 */
	public function modelLoad($id)
	{
		$className = $this->modelLookup();
		$model = CActiveRecord::model($className)->findByPk($id);
		if($model===null)
			throw new CHttpException (404,'no model found');
		return $model;
	}
	
	/**
	 * lookup a model's class name from a list of aliases
	 * @see self::modelResources
	 * @param string $model model alias name e.g. contact, which refers to the CrmContact class
	 * @return string model class name 
	 */
	public function modelLookup()
	{
		return $this->model;
	}
	
	/**
	 * get a list of model aliases, short convienient strings refering to model classes
	 * this makes the url's more friendly
	 * @return array of alias string => model class string  e.g. return array('cart'=>'AchiiveEcommerceCart')
	 */
	public function modelResources()
	{
		return array();
	}
	
	/**
	 * gets the request data, 
	 * i.e. the data sent by post, update, delete or get requests
	 * @return array 
	 */
	public function getData()
	{
		if(Yii::app()->request->getIsPutRequest() || Yii::app()->request->getIsPostRequest()){
			// asumming json passed so
			$data = file_get_contents('php://input');
			return CJSON::decode($data);
		}
	}
	
	/**
	 * return the response
	 * @param type $data
	 * @param int $status the html status ie 200 success or 500 or 404 etc
	 * @param string $contentType default application/json
	 */
	public function response($data, $status = 200, $contentType = 'application/json')
	{
		$output = CJSON::encode($data);
		
		// could this be improved to HTTP/2 to support chunking?
		header('HTTP/1.1: ' . $status);
		header('Status: ' . $status);

		exit($output);
	}
}