<?php

/**
 * CrmRest class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * Rest helper functions
 */
class CrmRest
{
	/**
	 * gets the request data, 
	 * i.e. the data sent by post, update, delete or get requests
	 * @return array 
	 */
	public static function getData()
	{
		if(Yii::app()->request->getIsPutRequest() || Yii::app()->request->getIsPostRequest()){
			// asumming json passed so
			$data = file_get_contents('php://input');
			return CJSON::decode($data);
		}
	}
	
	/**
	 * return the response
	 * @param type $data
	 * @param int $status the html status ie 200 success or 500 or 404 etc
	 */
	public static function responseJson($output, $status = 200)
	{
		Yii::beginProfile('json encode rest response');
		$acceptType = Yii::app()->request->getPreferredAcceptType();
		if ($acceptType['type'] == 'application' && $acceptType['subType'] == 'json') {
			$output = json_encode($output);
		} else {
			$output = json_encode($output, JSON_PRETTY_PRINT);
		}
		Yii::endProfile('json encode rest response');
		self::response($output, $status);
	}
	
	/**
	 * Output content
	 * @param type $output
	 * @param type $status
	 * @param type $contentType
	 */
	public static function response($output, $status=200, $contentType='application/json')
	{
		header('Content-type: '.$contentType);
		header('HTTP/1.1: ' . $status);
		header('Status: ' . $status);
		exit($output);
	}
	
	// basic profiling functions
	public static $profileStart = array();
	public static $profileStop = array();
	public static function beginProfile($token)
	{
		self::$profileStart[$token] = microtime(true);
	}
	public static function endProfile($token)
	{
		self::$profileStop[$token] = microtime(true);
		if (isset(self::$profileStart[$token])) {
			return self::$profileStop[$token] - self::$profileStart[$token];
		}
		return '';
	}
}