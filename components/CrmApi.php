<?php

/**
 * ProjectController class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * Crm Data format helper class
 */
class CrmApi
{
	/**
	 * 
	 * Generic search function, capable of pagination,
	 * searching by name, start character, and group
	 * Also defines URL keys
	 * 
	 * can be used in controller based $_GET searches like so:
	 * echo json_encode(CrmApi::search($_GET));
	 * 
	 * @param array $get
	 * 
	 *	[name] => 'string searchs the name' (runs CrmContact::filterName('term search')
	 *	[group] => string passed to CrmContact::filterGroup() (can be: 'all', 'companies', 'contacts', int id of group
	 *	[letter] => a letter to filter contacts by. 'a', 'b' or '0' (for numeric)
	 *	[CrmContact attributes] ...
	 * 
	 *	// Pagination options
	 *  page_size => the page number starts from 0 (default is 0)
	 *  page  => Rows Per Page default 30
	 * 
	 * @param array $options
	 *   [formatFunction] => a function like function($record){return array('name'=>$record->name)} or a string calling a function on that record
	 *						defaults to call 'getAttributes' the results of the query are passed to 
	 *						NData::toAttrArray($records, $formatFunction); see NData::toAttrArray for more info
	 *		
	 * @return array
	 *	results => array of row results
	 *	pagination => array(
	 *		item_count => total rows found by the query
	 *		page_size => total pages
	 *		page_size => the rows per page currently set
	 *		page => the current page number
	 *  )
	 */
	public static function search($get=array(), $options=array())
	{
		CrmRest::beginProfile('activereord');

		$options = CMap::mergeArray(array('formatFunction'=>'toArray'), $options);

		$dp = CrmApi::searchData($get, $options);
		$data = $dp->getData();
		
		$results = NData::toAttrArray($data, $options['formatFunction']);
		
		$profAr = CrmRest::endProfile('activereord');
		
		$return = array(
			'results'=>$results,
			'stats'=>array(
				'time'=>$profAr
			),
			'pagination'=> array(
				'page_size'=>$dp->pagination->pageSize,
				'item_count'=>$dp->pagination->itemCount,
				'page_count'=>$dp->pagination->pageCount,
				"page"=>$dp->pagination->currentPage
			)
		);
		return $return;
	}

	/**
	 * Return the active data provider with the correct search filters applied
	 * @return CActiveDataProvider
	 */
	public static function searchData($get=array(), $options=array())
	{
		$defaultOptions = array(
			'page_size'=>30
		);
		$options = CMap::mergeArray($defaultOptions, $options);
		
		self::applySearch($get, $dp, $crmContact);
		
		// pagination
		$dp->pagination->pageVar = 'page';
		$dp->pagination->pageSize = isset($get['page_size']) ? ($get['page_size'] > 1000 ? 1000 : $get['page_size']) : $options['page_size'];
		$dp->pagination->currentPage = isset($get['page']) ? $get['page'] : 0;
		return $dp;
	}

	/**
	 * Apply specific search filters to the CrmContact model
	 * @param array get search criteria
	 * @param $dp ()
	 * @return void
	 */
	public static function applySearch($get=array(), &$dp, &$crmContact)
	{
		$crmContact = new CrmContact('search');
		$crmContact->attributes = $get;
		$dp = $crmContact->search();
		
		// non model attribute supported search's

		// type: contact | company
		if (isset($get['type']))
			$crmContact->filterType($get['type']);

		// alphabet
		if (isset($get['letter']))
			$crmContact->filterLetter($get['letter']);
		
		// group
		if (isset($get['group']))
			$crmContact->filterGroup($get['group']);
		
		if (isset($get['todo']))
			$crmContact->filterTodo();
		
		if (isset($get['deal']))
			$crmContact->filterHasPendingDeal();

		if (isset($get['hasPhone']))
			$crmContact->filterHasPhone();
        
        if (isset($get['phone_number']))
			$crmContact->filterPhoneNumber($get['phone_number']);
        
		return array(
			'dp'=>$dp, 
			'CrmContact'=>$crmContact
		);
	}
	
	/**
	 * Count the number of contacts owned by owner id
	 * @param int $ownerId user id
	 * @return int
	 */
	public static function countOwner($ownerId=null)
	{
		$ownerId = ($ownerId===null) ? Yii::app()->user->id : $ownerId;
		return CrmContact::model()->filterOwner($ownerId)->count();
	}
	
	public static function searchFast($get=array(), $options=array())
	{
        $page = isset($get['page']) ? $get['page'] : 0;
        $pageSize = isset($get['page_size']) ? $get['page_size'] : 30;
        $offset = $page * $pageSize;

        $results = array();
        $count = Yii::app()->db->createCommand('select count(*) from crm_contact')->queryScalar();

        $contacts = Yii::app()->db->createCommand('select * from crm_contact order by name limit '.$pageSize.' offset '.$offset)->queryAll();
        $results = self::getContactsArray($contacts);
        $ret = array(
            'results'=>$results,
            'pagination'=> array(
                'page_size'=>$pageSize,
                'item_count'=>$count,
                'page_count'=>round($count/$pageSize),
                "page"=>$page
            )
        );
		return $ret;
	}
	
	public static function getContactArray($contact, $refresh=false)
	{
		$results = array();
		$cid = $contact->id.'-sql-array';
		if ($refresh)
			Yii::app()->cache->delete($cid);
		$data = Yii::app()->cache->get($cid);
		if ($data === false) {
			$data = $contact->getAttributes();
			if ($data['type'] == 'COMPANY') {
				$data['image_url'] = NHtml::url() . '/nii/index/show/id//type/crm-company';
				$data['staff'] = Yii::app()->db->createCommand('select id, name from crm_contact where company_id = '.$data['id'])->queryAll();
			} else {
				$data['image_url'] = NHtml::url() . '/nii/index/show/id//type/crm-contact';
			}
			if ($data['company_id']) {
				$data['company'] = Yii::app()->db->createCommand('select id, name from crm_contact where id = '.$data['company_id'])->queryRow();
				$data['company']['image_url'] = NHtml::url() . '/nii/index/show/id//type/crm-company';
			}
			$data['emails'] = Yii::app()->db->createCommand('select id, email, label from crm_email where contact_id = '.$data['id'])->queryAll();
			$data['phones'] = Yii::app()->db->createCommand('select id, number, label from crm_phone where contact_id = '.$data['id'])->queryAll();
			$data['websites'] = Yii::app()->db->createCommand('select id, address, label from crm_website where contact_id = '.$data['id'])->queryAll();
			$data['addresses'] = Yii::app()->db->createCommand('select * from crm_address where contact_id = '.$data['id'])->queryAll();
			$data['groups'] = Yii::app()->db->createCommand('select group_id as id from crm_group_contact where contact_id = '.$data['id'])->queryAll();
			$data['action'] = Yii::app()->db->createCommand('select * from crm_action where contact_id = '.$data['id']. ' and status = "TODO" order by id desc')->queryAll();
			Yii::app()->cache->set($cid, $data);
		}
		return $data;
	}
	
	/**
	 * *deprecated* in favour of self::findContactsByPhoneNumber
	 * @param string $phone
	 * @return array CrmContacts
	 */
	public static function findByPhone($phone)
	{
		$normalizedPhone = self::normalizePhoneNumber($phone);
        $phones = CrmPhone::model()->with('contact')->findAllByAttributes(array('number' => $normalizedPhone));
        $contacts = array();
		foreach($phones as $phone) {
			$contacts[] = $phone->contact;
		}
		return $contacts;
	}
    
    /**
     * Normalize a phone number. Remove 00, and + or single 0
     * Replaces single 0 with the default country code (UK 44)
     * @param string $number
     * @return string
     */
    public static function normalizePhoneNumber($number)
    {
    	// replace any whitespace
        $normalizedNuber = str_replace(' ', '', $number);
    	// if number starts with '00', strip it
		if (strcmp(substr($number, 0, 2), '00') == 0) {
			$normalizedNuber = substr($number, 2);
		} else if (strcmp(substr($number, 0, 1), '+') == 0) {
			// if the number starts with '+' strip it
			$normalizedNuber = substr($number, 1);
		} else if (strcmp(substr($number, 0, 1), '0') == 0) {
			// if the number starts with just one '0', then it is a local number. Assume UK.
			// strip the leading 0 and prepend '44'
			$normalizedNuber = '44'.substr($number, 1);
		}
		return $normalizedNuber;
    }
	
    /**
     * Get all contacts that have the phone number specified
     * Will return companies first.
     * @param string $phoneNumber
     * @param int $limit optional
     * @return array of CrmContact models
     */
    public static function findContactsByPhoneNumber($phoneNumber, $limit=null)
    {
        $number = self::normalizePhoneNumber($phoneNumber);
        $criteria = array(
            'condition' => 'crm_phone.number = "'.$number.'"',
            'join' => 'inner join crm_phone ON crm_phone.contact_id = t.id',
            // the order denoted which to list first.
            // type Desc means show contact records with field type = company first in the array
            'order' => 't.type desc'
        );
        if ($limit) {
            $criteria['limit'] = $limit;
        }
        return CrmContact::model()->with(array('company'))->findAll($criteria);
    }
    
}
