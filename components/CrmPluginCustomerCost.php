<?php



Class CrmPluginCustomerCost 
{
	/**
	 * The cost per hour per employee
	 * @var int
	 */
	public static $chargeRatePerHour = 45;
	
	/**
	 * Check if we can use the plugin
	 * Dependacies relies on TimesheetLog
	 */
	public static function checkDependancies()
	{
		$projectModule = Yii::app()->getModule('project');
		$kashflowModule = Yii::app()->getModule('kashflow');
		return ($projectModule && $kashflowModule);
	}
	
	/**
	 * @param int $customerId
	 * @param $ignoreAllBeforeDate strtotime interpreted string, ignores all data before this time. 
	 * Newicon's time sheet records started on 16th September 2013
	 * @return array
	 *  [totalStaffMinutes] => the total minutes logged by all staff members to this customer
	 *  [totalCost] => total cost totalStaffMinutes / 60 * charge rate
	 *  [totalInvoiced] => Total invoiced to this customer after the $ignoreAllBeforeDate
	 */
	public static function getStats($customerId, $ignoreAllBeforeDate='16th September 2013')
	{
		// Ignore all invoices before this date (when the timesheet started!)
		// 16th of September 2013 is when we launched the timesheets!
		
		// look up kashflow customer id?
		$contact = CrmContact::model()->findByPk($customerId);
		if ($contact->kashflow_id === null)
			throw new CHttpException(400, 'No Kashflow ID is specified for this customer');
		
		$kashflowCustId = $contact->kashflow_id;
		
		$ignoreAllBeforeDate = strtotime($ignoreAllBeforeDate);
		
		$kash = KashflowApi::get();
		$invoices = $kash->GetInvoicesForCustomer($kashflowCustId);
		
		$totalAmount = 0;
		foreach ($invoices as $r) {
			if (strtotime($r->InvoiceDate) > $ignoreAllBeforeDate) {
				//$totalVAT += $r->VATAmount;
				$totalAmount += $r->NetAmount;
			}
		}
		
		// get total time spent on customer
		$dateQuery = date('Y-m-d', $ignoreAllBeforeDate);
		$totalMinutes = TimesheetLog::model()->cmdSelect('SUM(minutes)')
			->where("start > '$dateQuery'")
			->andWhere("contact_id = $customerId")
			->queryScalar();

		$hours = $totalMinutes / 60;
		
		list ($chargeRate, $billRate) = array(45, 75);
		if (Yii::app()->getModule('project')){
			$chargeRate = ProjectSettings::model()->cost_rate_per_hour;
			$billRate = ProjectSettings::model()->bill_rate;
		}
		
		$cost = $chargeRate * $hours;
		$NumBilledHours = $totalAmount / $billRate;
		
		$totalInvoiced = number_format($totalAmount, 2);
		$totalShouldHave = ($totalMinutes/60)*$billRate;
		$totalShouldHaveInvoiced = number_format($totalShouldHave, 2);
		$unbilledTime = $totalMinutes - ($NumBilledHours*60);
        
        $totalStaffHours = $totalMinutes/60;
        $earntPerHour = ($totalStaffHours > 0) ? $totalAmount / $totalStaffHours : 0;
        
		return array(
			'since' => $dateQuery,
			'account' => $contact->name,
			'chargeRate' => $chargeRate,
			'totalStaffMinutes' => $totalMinutes,
            'totalStaffHours' => $totalStaffHours,
			'totalBilledMinutes' => $NumBilledHours*60,
			'totalStaffTime' => NTime::minutesToDays($totalMinutes),
			'totalBilledTime' => NTime::minutesToDays($NumBilledHours*60),
            'totalBilledTime_explained'=>'Uses the total invoiced amount to calculate the billable hours. The total raised invoices for this customer / our current billable rate',
			'totalUnbilledTime' => NTime::minutesToDays($unbilledTime),
			'totalUnbilledTimeNice' => NTime::minutesToNiceShort($unbilledTime),
			'totalStaffTimeNice' => NTime::minutesToNiceShort($totalMinutes),
			'totalBilledTimeNice' => NTime::minutesToNiceShort($NumBilledHours*60),
			'totalCost' => number_format($cost, 2),
			'totalInvoiced' => $totalInvoiced,
            'totalInvoiced_explained'=>'The total sum of money invoiced to this customer',
			'totalShouldHaveInvoiced' => $totalShouldHaveInvoiced,
			'uncharged' => number_format($totalShouldHave - $totalAmount, 2),
			'profitLossRaw' => $totalAmount - $cost,
			'profitLoss' => number_format(($totalAmount - $cost), 2),
            'earntPerHour' => $earntPerHour,
            'earntPerHour_explained' => 'The amount in pounds we have been paid by this customer for each hour of effort'
		);
	}
}