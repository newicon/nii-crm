<?php

/**
 * ProjectController class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * This class patches references to the old Contact module,
 * should be refactored and implemented as some kind of interface accessed via a dependancy injecytor of somekind
 * Then other modules could potentially register to override the class
 */
class CrmExternal 
{
	public static function getDropDownData($valueField='id', $nameField='name')
	{
		$companies = CrmContact::model()->companies()->findAll(array('order'=>'name'));
		$companies = CHtml::listData($companies, $valueField, $nameField);
		return $companies;
	}
}
