<?php

/**
 * NoteController class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * NoteController
 *
 */
class NoteController extends CrmRestController
{
	/**
	 * notes via get 
	 * Get all the notes for contact id $contact
	 * @request: GET: /crm/api/note/for/contact/<contact_id>
	 */
	public function actionFor($contact)
	{
		$array = CJSON::encode(CrmNote::model()->findAllByAttributes(array('contact_id'=>$contact), array('order'=>'added_on desc') ));
		CrmRest::response($array);
	}
	
	/**
	 * Get a single note by id
	 * @param int $id
	 */
	public function actionGet($id)
	{
		$n = $this->loadNote($id);
		CrmRest::responseJson($n->toArray());
	}
	
	/**
	 * Handles create and update functionality. If an id is provided an update will run
	 * otherwise a new record is created
	 * @param int $id
	 * @request: POST : /crm/api/note/33 || /crm/api/note/
	 */
	public function actionSave($id='')
	{
		$data = CrmRest::getData();
		if ($id == '') {
			$note = new CrmNote;
		} else {
			$note = $this->loadNote($id);
		}
		$note->attributes = $data;
		$saved = $note->save();
		if (!$saved) {
			CrmRest::responseJson($note->getErrors(), 400);
		} else {
			// load in the timestamp from the database on a successfull save otherwise it will still be null
			$note->refresh();
			CrmRest::responseJson($note->toArray());
		}
	}
	
	/**
	 * Load a CrmNote model by id
	 * @param int $id
	 * @return CrmNote
	 */
	public function loadNote($id)
	{
		return NData::loadModel('CrmNote', $id, 'No note found with id "'.$id.'"');
	}
}