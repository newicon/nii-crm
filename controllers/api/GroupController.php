<?php

/**
 * GroupController class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * Description of GroupController
 *
 * @author 
 */
class GroupController extends CrmRestController
{
	public $model = 'CrmGroup';
	/**
	 * group via get 
	 * @request: GET: /crm/api/group
	 */
	public function actionIndex()
	{
		$limit = 100;
		$groups = CrmGroup::model()->findAll(array('limit'=>$limit));
		CrmRest::responseJson(NData::toAttrArray($groups, 'toArray'));
	}
	
	/**
	 * Saves all groups posted as a list
	 * This is based on the idea that each resource is a seperate "Rest" based document
	 * With this rationale a collection of groups is in fact its own resource.
	 * Thereofre posting to the collection means the collection resource can update itself.
	 * This is actually quite usefull for updating the order. As the order of each group item within the collection
	 * changes.
	 * @request: POST: /crm/api/groups
	 */
	public function actionSaveList()
	{
		$groups = $this->getData();
		if (!is_array($groups)){
			$this->response('You must post an array of groups to save', 400);exit;
		}
		$response = array(); $errors = array();
		foreach($groups as $group) {
			$groupModel = $this->loadGroup($group['id']);
			$groupModel->attributes = $group;
			$saved = $groupModel->save();
			
			if ($saved) {
				$groupModel->refresh();
				$response[] = $groupModel;
			} else {
				$errors[] = $this->response($groupModel->getErrors(), 400);
			}
		}
		if ($saved) {
			$this->response($response);
		} else {
			$this->response($errors, 400);
		}
	}
	
	/**
	 * 
	 * @param int $id group id
	 * @request: GET: /crm/api/group/23
	 */
	public function actionRead($id)
	{
		$group = $this->loadGroup($id);
		CrmRest::responseJson($group->toArray());
	}
	
	/**
	 * Handles create and update functionality. If an id is provided an update will run, 
	 * otherwise a new record is created
	 * @param int $id
	 * @request: POST : /crm/api/group/33 || /crm/api/group/
	 */
	public function actionSave($id='')
	{
		$data = CrmRest::getData();
		if ($id == '')
			$group = new CrmGroup;
		else
			$group = $this->loadGroup($id);
		$group->attributes = $data;
		$saved = $group->save();
		if ($saved) {
			$group->refresh();
			$this->response($group->toArray());
		} else {
			$this->response($group->getErrors(), 400);
		}
	}
	
	/**
	 * Removes contacts from a group
	 * echos json number of contacts in group
	 * @request: POST: /crm/api/group/33/removeContacts
	 * @returns int echos out the new contact count for the group
	 */
	public function actionRemoveContacts($id)
	{
		$data = CrmRest::getData();
		$group = CrmGroup::removeContacts($id, $data['contacts']);
		CrmRest::responseJson(array('result'=>$group->contactsCount));
	}
	
	/**
	 * Add contacts to a group
	 * echos json number of contacts in group
	 * @request: POST: /crm/api/group/33/addContacts
	 */
	public function actionAddContacts($id)
	{
		$data = CrmRest::getData();
		$contacts = isset($data['contacts']) ? $data['contacts'] : array();
		$group = CrmGroup::addContacts($id, $contacts);
		if ($group === null)
			throw new CHttpException(404, "No group found with id '$id'");
		CrmRest::responseJson($group->toArray());
	}
	
	/**
	 * Load a CrmGroup model by id
	 * @param int $id
	 * @return CrmGroup
	 */
	public function loadGroup($id)
	{
		return NData::loadModel('CrmGroup', $id, 'No group record found');
	}
}