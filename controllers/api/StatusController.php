<?php

/**
 * StatusController
 *
 */
class StatusController extends CrmRestController
{
	public function actionIndex()
	{
		$array = json_encode(CrmStatus::getStatusList());
		CrmRest::response($array);
	}
}

