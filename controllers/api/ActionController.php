<?php

/**
 * ProjectController class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * Controller to handle actions for the CrmAction model
 */
class ActionController extends CrmRestController
{
	/**
	 * action via get
	 * @request: GET: /crm/api/action
	 */
	public function actionIndex()
	{
		$limit = 100;
		$actions = CrmAction::model()->findAll(array('limit'=>$limit));
		CrmRest::responseJson(NData::toAttrArray($actions));
	}
	
	/**
	 * Get a single model record
	 * @param int pk $id
	 * @request: GET /crm/api/action/24
	 */
	public function actionGet($id)
	{
		$action = NData::loadModel('CrmAction', $id);
		CrmRest::responseJson($action->toArray());
	}
	
	/**
	 * Get a single model record and download an iCal file
	 * @param int pk $id
	 * @request: GET /crm/api/action/24.ical
	 */
	public function actionIcal($id)
	{
		$action = NData::loadModel('CrmAction', $id);
		header("Content-Type: text/Calendar");
		header("Content-Disposition: inline; filename=calendar.ics");
		$action->toIcal();
		exit;
	}
	
	/**
	 * Handles create and update functionality. If an id is provided an update will run, 
	 * otherwise a new record is created
	 * @param int $id
	 * @request: POST : /crm/api/action/33 || /crm/api/action/
	 */
	public function actionSave($id='')
	{
		$data = CrmRest::getData();
		if ($id == '')
			$model = new CrmAction;
		else
			$model = NData::loadModel('CrmAction', $id);
		$model->attributes = $data;
		$saved = $model->save();
		if ($saved)
			CrmRest::responseJson($model->toArray());
		else
			CrmRest::responseJson ($model->getErrors(), 500);
	}
}
