<?php

/**
 * ContactController class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * Api functions for contacts
 *
 */
class ContactController extends CrmRestController
{
	/**
	 * List contacts.
	 * 
	 * Will recognise and filter on the following GET params:
	 * 
	 *  - group : the group name or id (all, contacts, companies) or id of the group
	 *  - letter : the first character of the name (alphabet search)
	 *  - name : query test string name match
	 * @return json array of contact attributes
	 */
	public function actionIndex()
	{
		CrmRest::beginProfile('profile-request-query');
		$searchOptions = isset($_GET['search_options']) ? $_GET['search_options'] : array();
		$return = CrmApi::search($_GET, $searchOptions);
		$logQuery = CrmRest::endProfile('profile-request-query');
		CrmRest::beginProfile('profile-request-encode');
		CrmRest::responseJson($return);
	}
	
	/**
	 * Get a single contact record CrmContact
	 * @param int $id
	 */
	public function actionGet($id)
	{
		$c = NData::loadModel('CrmContact', $id);
		CrmRest::responseJson($c->toArrayExpanded());
	}
	
	/**
	 * Handles create and update functionality. If an id is provided an update will run, 
	 * otherwise a new record is created
	 * @param int $id
	 * @request: POST : /crm/api/contact/33 || /crm/api/contact/
	 */
	public function actionSave($id='')
	{
		$data = CrmRest::getData();
		if ($id == '')
			$contact = new CrmContact;
		else
			$contact = NData::loadModel ('CrmContact', $id, "No contact found with id '$id'");
		$contact->attributes = $data;
		$saved = $contact->save();
		if (!$saved) {
			CrmRest::responseJson($contact->getErrors(), 400);
		} else {
			CrmRest::responseJson($contact->toArrayExpanded());
		}
	}
	
	/**
	 * Delete a contact
	 * @param int $id contact id
	 */
	public function actionDelete($id)
	{
		$contact = NData::loadModel('CrmContact', $id);
		$contact->delete();
	}
	
	/**
	 * Get the next action for this contact
	 * @param int $id
	 */
	public function actionAction($id)
	{
		$contact = NData::loadModel('CrmContact', $id);
		$action = $contact->getAction();
		if ($action)
			CrmRest::responseJson($action->toArray());
		else
			CrmRest::response('', 204);
	}
	
	/**
	 * http://crm/api/contact/countOwnedBy/userid/3
	 * @param type $userid
	 */
	public function actionCountOwnedBy($userid)
	{
		CrmRest::response(CrmApi::countOwner($userid));
	}
}