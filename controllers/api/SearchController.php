<?php

/**
 * SearchController class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * Description of SearchController
 *
 * @author 
 */
class SearchController extends CrmRestController
{
	public function actionSearch()
	{
		
		$return = CrmApi::search($_GET);
		
		dp(json_encode($return, JSON_PRETTY_PRINT));
		
	}

	public function actionTelephoneList()
	{
		$ids = NData::listAttribute(CrmPhone::model()->findAll('number <> ""'), 'contact_id');
		$contacts = CrmContact::model()->findAllByAttributes(array('id'=>$ids), array('order'=>'name', 'condition'=>'name IS NOT NULL AND name <> ""'));
		$return = array();
		foreach ($contacts as $c)
			$return[$c->id] = CrmContact::getContactArray($c);

		echo CJSON::encode($return);

	}
}