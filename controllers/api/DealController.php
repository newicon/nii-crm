<?php
/**
 * DealController class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * DealController
 */
class DealController extends CrmRestController
{
	/**
	 * Defines the model. Making CrmRestController actions generic across models.
	 * @var string ClassName	
	 */
	public $model = 'CrmDeal';
	
	/**
	 * @param string $model (string model class name or alias model name see self::modelLookup)
	 * @return json array of models
	 */
	public function actionList()
	{
		$sql = 'select crm_deal.*, contact.company_id, contact.name as contact_name, user_user.first_name as owner_first_name, user_user.last_name as owner_last_name, company.name as company_name from crm_deal ';
        $sql .= 'inner join crm_contact as contact on contact.id = crm_deal.contact_id ';
		$sql .= 'inner join user_user on user_user.id = crm_deal.owner_id ';
		$sql .= 'left join crm_contact as company on company.id = contact.company_id';
		$res = Yii::app()->db->createCommand($sql)->queryAll();
		$this->response($res);
	}
	
	/**
	 * Get deals for this contact
	 * specified by contact id
	 * @param int $contact
	 * @return echo string json
	 */
	public function actionFor($contact)
	{
		$deals = CrmDeal::model()->findAllByAttributes(array('contact_id'=>$contact));
		$this->response($deals);
	}
}