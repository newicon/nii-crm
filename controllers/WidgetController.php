<?php

/**
 * WidgetController class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * Widget controller hosts default action paths for widgets,
 * The controller itself is simply a placeholder for widget actions, but defines no actions itself.
 * In other words.  CRM widgets will initially have URL's pointing to crm/widget/action however the action itself
 * will be defined by a CAction class in the library
 */
class WidgetController extends AController
{
	public function actions()
	{
		return array(
			'ddFind'=>'crm.widgets.lookup.ActionDdFind',
			'ddLookup'=>'crm.widgets.lookup.ActionDdLookup',
			'ddPhoneLookup'=>'crm.widgets.lookup.ActionPhoneLookup'
		);
	}
}
