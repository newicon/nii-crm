<?php

/**
 * ImportController class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * Controller to handle importing data.  This could potentially get complex
 */
class ImportController extends AController
{
	/**
	 * Action to import data from current hub contact module system
	 */
	public function actionImport()
	{
		foreach(Contact::model()->findAll() as $contact) {

			// 'dob' => "date",
			// 'comment' => "text",

			$crm = new CrmContact;

			// Contact / Company
			if ($contact->contact_type == 'Person') {
				$crm->type = CrmContact::TYPE_CONTACT;

				if ($contact->givennames) {
					$crm->setName($contact->givennames . ' ' . $contact->lastname);
				} else {
					$crm->setName($contact->name);
				}
				$crm->gender = $contact->gender;
				$crm->save();

			} else {
				$crm->type = CrmContact::TYPE_COMPANY;
				$crm->setName($contact->name);
				$crm->save();
			}

			// emails
			$crm->addEmail($contact->email, '');
			if ($contact->email_secondary)
				$crm->addEmail($contact->email_secondary, 'secondary');

			// Phone
			if ($contact->tel_primary)
				$crm->addPhone($contact->tel_primary, 'Primary');
			if ($contact->tel_secondary)
				$crm->addPhone($contact->tel_secondary, 'Secondary');
			if ($contact->mobile)
				$crm->addPhone($contact->mobile, 'Mobile');
			if ($contact->fax)
				$crm->addPhone($contact->fax, 'Fax');

			// Website
			if ($contact->website)
				$crm->addWebsite($contact->website, 'Website');

			// Address
			if ($contact->addr1) {
				$lines = $contact->addr1 . "\n" . $contact->addr2 . "\n" . $contact->addr3;
				$crm->addAddress($lines, $contact->city, $contact->county, $contact->country, $contact->postcode);
			}

		}
	}
	
	public function actionKashflow()
	{
		
	}
}