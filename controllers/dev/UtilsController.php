<?php

/**
 * ProjectController class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * Hacky utils!
 */
class UtilsController extends AController 
{
	public function actionConvert()
	{
		$this->render('convert');
	}
	
	public function actionConvertIds()
	{
		$idStart = 9990;
		foreach(CrmContact::model()->findAll(array('order'=>'name')) as $contact){
			$idStart += 1; 
			$contact->id = $idStart;
			$contact->save();
		}
		echo 'done';
	}
	
	public function actionConverter()
	{
		$contactId = $_POST['contact_id'];
		$crmId = $_POST['crm_id'];
		// lookup see if the id is already in use in the CrmContact database
		$contact = CrmContact::model()->findByPk($crmId);
		$contact->id = $contactId;
		$contact->save();
		echo $contact->id;
	}
}
