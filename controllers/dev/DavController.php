<?php

/**
 * DavController class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

include Yii::getPathOfAlias('base.vendor') . '/autoload.php';

Yii::setPathOfAlias('Sabre', Yii::getPathOfAlias('base.vendor.Sabre'));
Yii::import('crm.components.dav.*');

/**
 * Description 
 */
class DavController extends CController
{
	public function actionIndex()
	{
		// only 3 simple classes must be implemented by extending SabreDAVs base classes
//        $principalBackend = new Sabre\DAV\MyPrincipalBackend;
//        $calendarBackend = new Sabre\DAV\MyCalendarBackend;
		
		$pdo = new PDO('mysql:host=localhost;dbname=newiconhub_development', 'root', '');
		$pdo->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
		$principalBackend = new Sabre\DAVACL\PrincipalBackend\PDO($pdo);
		$carddavBackend   = new CrmCardDavBackend($pdo);
		$authBackend = new DavAuth();

		// Setting up the directory tree //
		$nodes = array(
			new Sabre\DAVACL\PrincipalCollection($principalBackend),
			new Sabre\CardDAV\AddressBookRoot($principalBackend, $carddavBackend),
		);

		// The object tree needs in turn to be passed to the server class
		$server = new Sabre\DAV\Server($nodes);
		$server->setBaseUri($this->createUrl('/dav'));

		// Plugins
		$server->addPlugin(new Sabre\DAV\Auth\Plugin($authBackend, 'Nii'));
		$server->addPlugin(new Sabre\DAV\Browser\Plugin());
		//$server->addPlugin(new Sabre\CalDAV\Plugin());
		$server->addPlugin(new Sabre\CardDAV\Plugin());
		$server->addPlugin(new Sabre\DAVACL\Plugin());

		// And off we go!
		$server->exec();
		
		
        // this defines the root of the tree, here just principals and calendars are stored
//        $tree = array(
//            //new Sabre\DAVACL\PrincipalCollection($principalBackend),
//            new Sabre\CalDAV\Principal\Collection($principalBackend),
//            // new Sabre\CalDAV\CalendarRootNode($principalBackend, $calendarBackend),
//        );
// 
//        $server = new DAV\Server($tree);
//        $server->setBaseUri($this->createUrl('/crm/dav/index'));
//        $server->addPlugin($authPlugin);
// 
//        $server->addPlugin(new Sabre\DAVACL\Plugin());
//        $server->addPlugin(new Sabre\CalDAV\Plugin());
//        // this is fun, try to open that action in a plain web browser
//        $server->addPlugin(new Sabre\DAV\Browser\Plugin());
// 
//        $server->exec();
	}
}