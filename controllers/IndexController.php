<?php

/**
 * IndexController class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 */
class IndexController extends AController
{
	/**
	 * @inheritdoc
	 * allows calls to ical
	 */
	public function accessRules()
	{
		$rules = parent::accessRules();
		// enable ical action to be accessed without login credentials
		array_unshift($rules, array('allow', 'actions'=>array('ical')));
		return $rules;
	}

	public function actionTheme($theme)
	{
		Yii::app()->theme = $theme;
	}

	/**
	 * boot strap the app
	 */
	public function actionIndex()
	{
		$this->render('index');
	}

	public function actionQuality($customerId)
	{
		$acceptType = Yii::app()->request->getPreferredAcceptType();
		if ($acceptType['type'] == 'application' && $acceptType['subType'] == 'json') {
			echo json_encode(CrmPluginCustomerCost::getStats($customerId, '1st December 2014'));
		} else {
			echo '<pre>'.json_encode(CrmPluginCustomerCost::getStats($customerId, '1st January 2020'), JSON_PRETTY_PRINT).'</pre>';
		}
	}

	public function actionPhoneList()
	{
		$this->layout = '//layouts/phonelist1column';
		$ids = NData::listAttribute(CrmPhone::model()->findAll('number <> "" AND label <> "Fax"'), 'contact_id');
		$contacts = CrmContact::model()->findAllByAttributes(array('id'=>$ids), array('order'=>'name', 'condition'=>'name IS NOT NULL AND name <> ""'));
		$data = array();
		foreach ($contacts as $c)
			$data[$c->id] = CrmContact::getContactArray($c);
		$endpoint = CallEndpoint::model()->findByAttributes(array('user_id'=>Yii::app()->user->id, 'use_for_outbound'=>1));
		$this->render('phonelist', array('data'=>$data, 'endpoint'=>$endpoint));
	}

	/**
	 * Redirects to google calendar, and prepopulates with details of the action
	 */
	public function actionGoogleCal($actionId='')
	{
		$action = CrmAction::model()->findByPk($actionId);
		$url = $action->getGCalLink();
		$this->redirect($url);
		exit;
	}

	public function actionExportPrint()
	{
		CrmApi::applySearch($_GET, $dp, $crmContact);
		$emails = $crmContact->with('emails')->findAll();
		$array = array();
		$array[] = array('Email', 'Name', 'First name', 'Last name');
		foreach($emails as $e){
			$array[] = array(
			//	$e->emails,
				$e->email->email,
				$e->name_first,
				$e->name_last,
			);
		}
		$csv = $this->array2csv($array);
		dp($csv);
	}

	/**
	 * Send a csv file of the contacts.
	 * This function supports the same search criteria as the main search controller
	 * /crm/api/contact/index
	 * Called by the export link on the contacts filter page.
	 * @return csv file
	 */
	public function actionExport()
	{
		CrmApi::applySearch($_GET, $dp, $crmContact);
		$dp->pagination->pageSize = 10000;



		foreach ($dp->getData() as $i => $contact) {
			$c = $contact->toArray();

			$i++;
			$array[$i] = array();
			$array[$i]['id'] = $c['id'];

			$array[$i]['kashflow_id'] = $c['kashflow_id'];

			$array[$i]['Status'] = '';
			$status = CrmStatus::model()->findByPk($c['status_id']);
			if ($status) {
				$array[$i]['Status'] = $status->name;
			}

			$array[$i]['Name'] = $c['name'];
			$array[$i]['First Name'] = $c['name_first']; // Person - First name
			$array[$i]['Last Name'] = $c['name_last']; // Person - Last name
			$array[$i]['Company Name'] = $c['name_company']; // Organization - Name
			$array[$i]['Company Role'] = $c['company_role'];
			// get the contacts company
			if ($c['type'] == 'CONTACT' && isset($c['company']['name'])) {
				$array[$i]['Company Name'] = $c['company']['name'];
			}
			// Email
			$array[$i]['Email'] = '';
			$array[$i]['Email 1'] = '';
			$array[$i]['Email 2'] = '';
			if (isset($c['emails'][0])) {
				foreach($c['emails'] as $key => $email) {
					$suffix = ($key === 0) ? '' : " $key";
					$array[$i]["Email$suffix"] = $email['email'];
				}
			}

			// Phone 1
			$array[$i]['Phone'] = '';
			$array[$i]['Phone 1'] = '';
			$array[$i]['Phone 2'] = '';
			if (isset($c['phones'])) {
				foreach($c['phones'] as $key => $phone) {
					$suffix = ($key === 0) ? '' : " $key";
					$array[$i]["Phone$suffix"] = $phone['number'];
				}
			}

			// addresss
			$array[$i]['Address'] = '';
			if (isset($c['addresses'])) {
				foreach($c['addresses'] as $key => $address) {
					$suffix = ($key === 0) ? '' : " $key";
					$array[$i]["Address$suffix"] = $address['lines'] . ', ' . $address['city'] . ', ' . $address['postcode'];
				}
			}

			// groups
			$array[$i]['Group'] = '';
			$array[$i]['Group 1'] = '';
			if (isset($c['groups'][0])) {
				foreach ($c['groups'] as $key => $group) {
					$suffix = ($key === 0) ? '' : " $key";
					$array[$i]["Group$suffix"] = CrmGroup::model()->findByPk($c['groups'][$key]['id'])->name;
				}
			}

			// Type
			$array[$i]['Type'] = $c['type'];
		}



		$csv = $this->array2csv($array);
		Yii::app()->request->sendFile('contacts.csv', $csv, 'text/csv');
	}

	/**
	 * Generate an ical feed for syncing calendars
	 */
	public function actionICal($key=null)
	{
		header('Content-type: text/calendar; charset=utf-8');
		header('Content-Disposition: attachment; filename=crm-actions.ics');
		$results = CrmAction::model()->with('contact')->findAllByAttributes(array('status'=>CrmAction::STATUS_TODO), array('order'=>'due ASC'));
		$output = $this->getIcalHeader();
		foreach($results as $action) {
			$output .= $action->toIcal($action);
		}
		$output .= $this->getIcalFooter();
		exit($output);
	}

	public function getIcalHeader()
	{
		$iCalHeader  = "BEGIN:VCALENDAR\n";
		$iCalHeader .= "VERSION:2.0\n";
		$iCalHeader .= "CALSCALE:GREGORIAN\n";
		$iCalHeader .= "METHOD:PUBLISH\n"; // requied by Outlook can be REQUEST or PUBLISH (publish shows save/cancel) request shows (accept/decline)
		$iCalHeader .= "PRODID:-//Newicon Crm iCal Feed Generator//EN\n";
		$iCalHeader .= "X-PUBLISHED-TTL:PT1H\n"; // Only checks every hour. PT3H would be every 3 hours
		return $iCalHeader;
	}

	public function getIcalFooter()
	{
		return "END:VCALENDAR\n";
	}

	/**
	 * Takes an associative array of items and converts it to a csv file
	 * @return string csv file
	 */
	public function array2csv($array) {
		if (count($array) == 0)
			return null;
		ob_start();
		$df = fopen("php://output", 'w');
		fputcsv($df, array_keys(reset($array)));
		foreach ($array as $row) {
		   fputcsv($df, $row);
		}
		fclose($df);
		return ob_get_clean();
	}
}
