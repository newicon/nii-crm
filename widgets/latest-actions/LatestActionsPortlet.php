<?php

/**
 * LatestActions class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * Show a list of the latest crm actions
 */
class LatestActionsPortlet extends NPortlet {
	
	public $title = '<h5><span class="icon"><i class="icon-user"></i></span> Latest Actions</h5>';
	public $limit = 5;
	
	public function __construct() {
		$this->title = '<div class="buttons widget-rightlink">'.NHtml::btnLink('View All',array('/crm#/actions')).'</div>'.$this->title;
	}
	
	protected function renderContent() {
		$contacts = CrmApi::search(array('todo'=>true,'page_size'=>5));
		$this->render('latest', array('contacts'=>$contacts));
	}
}