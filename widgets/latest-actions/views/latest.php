<?php

/**
 * Nii class file.
 *
 * @author Newicon, Steven O'Brien <steven.obrien@newicon.net>
 * @link http://github.com/newicon/Nii
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */
?>
<?php foreach($contacts['results'] as $contact): ?>
	<div class="media">
		<div class="pull-left">
			<a href="<?php echo $contact['url']; ?>"><img width="24" src="<?php echo $contact['image_url']; ?>" /></a>
		</div>
		<div class="media-body">
			<?php echo $contact['name'] ? CHtml::link($contact['name'], $contact['url']) : CHtml::link('No Name', $contact['url']) ; ?>
			<span class="label pull-right" style="background-color:<?php echo CrmAction::dueColor($contact['action']); ?>"><?php echo CrmAction::dueLabel($contact['action']); ?></span><br/>
			<?php if (isset($contact['action']) && isset($contact['action']['action'])): ?>
			<?php echo $contact['action']['action'] ?>
			<?php endif; ?>
		</div>
	</div>
<?php endforeach; ?>