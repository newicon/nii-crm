<?php

/**
 * Nii view file.
 *
 * Expects CrmContact $contact param
 * 
 * @author Newicon, Steven O'Brien <steven.obrien@newicon.net>
 * @link http://github.com/newicon/Nii
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */
?>

<div class="media">
	<div class="pull-left">
		<a href="<?php echo $contact->url; ?>"><img width="40" src="<?php echo $contact->imageUrl ?>" /></a>
	</div>
	<div class="media-body">
		<a href="<?php echo $contact->url; ?>"><strong><?php echo $contact->name; ?></strong></a> <br/>
		<?php if ($contact->isContact()): ?>
			<?php if ($company = $contact->company()): ?>
				<?php if($contact->company_role) : ?>
					<?php //echo $contact->company_role; ?>
				<?php endif; ?>
				<a href="<?php echo $company->url ?>"><img width="16" src="<?php echo $company->imageUrlThumb24 ?>" /></a> <a href="<?php echo $company->url ?>"><?php echo $company->name; ?></a>
			<?php endif; ?>
		<?php endif; ?>
	</div>
</div>