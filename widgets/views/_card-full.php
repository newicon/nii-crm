<?php

/**
 * Nii view file.
 *
 * @author Newicon, Steven O'Brien <steven.obrien@newicon.net>
 * @link http://github.com/newicon/Nii
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */
?>


<div class="well well-transparent" style="width: <?php echo $this->cardWidth; ?>;">
	<a class="pull-right muted" href="<?php echo NHtml::url($contact->routeEdit) ?>"><small>Edit</small></a>
	<?php $this->widget('crm.widgets.CrmCard', array('contact'=>$contact, 'mediaHtmlOptions'=>array('class'=>'media mtn'))) ?>
	
	<?php if ($contact->emails): ?>
		<br/>
		<!--<h5 class="mbn">Emails</h5>-->
		<?php foreach($contact->emails as $email) : ?>
			<i class="icon-envelope icon-subtle"></i> <a target="_blank" href="<?php echo $email->mailtoLink ?>"><?php echo $email['email']; ?></a> <span class="badge badge-attribute-label"><?php echo $email['label']; ?></span><br/>
		<?php endforeach; ?>
	<?php endif; ?>

	<?php if ($contact->phones): ?>
		<!--<h5 class="mbn">Tel</h5>-->
		<br/>
		<?php foreach($contact->phones as $phone) : ?>
			<i class="icon-phone icon-subtle"></i> <?php echo $phone['number']; ?> <span class="badge badge-attribute-label"><?php echo $phone['label']; ?></span><br/>
		<?php endforeach; ?>
	<?php endif; ?>

	<?php if ($contact->websites): ?>
		<!--<h5 class="mbn">Websites</h5>-->
		<br/>
		<?php foreach($contact->websites as $website) : ?>
			<i class="icon-desktop icon-subtle"></i> <a target="_blank" href="<?php echo $website->url; ?>"><?php echo $website['address']; ?></a> <span class="badge badge-attribute-label"><?php echo $website['label']; ?></span><br/>
		<?php endforeach; ?>
	<?php endif; ?>

	<?php if ($contact->addresses): ?>
		<div class="line">
			<!--<h5 class="mbn">Addresses</h5>-->
			<br/>
			<?php foreach($contact->addresses as $address) : ?>
				<i class="icon-home pull-left icon-subtle"></i> 
				<address class="pull-left mbn">
					<?php echo $address->printAddress(); ?>
					<a traget="_blank" href="<?php echo $address->mapLink() ?>"><small>Map</small></a> <i class="icon icon-"></i>
					<a target="_blank" href="<?php echo $address->mapLink(true) ?>"><small>Directions</small></a>
				</address>
				<span class="badge pull-left badge-attribute-label mls mts"><?php echo $address['label']; ?></span><br/>
			<?php endforeach; ?>
		</div>
	<?php endif; ?>
</div>