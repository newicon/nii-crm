<?php

/**
 * Nii view file.
 *
 * @author Newicon, Steven O'Brien <steven.obrien@newicon.net>
 * @link http://github.com/newicon/Nii
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */
?>

<div <?php echo CHtml::renderAttributes($this->mediaHtmlOptions); ?>>
	<div class="pull-left">
		<a href="<?php echo NHtml::url($contact->route); ?>"><img width="72" class="media-object" src="<?php echo $contact->imageUrl ?>" /></a>
	</div>
	<div class="media-body">
		<h4 class="mtn"><a href="<?php echo NHtml::url($contact->route); ?>"><?php echo $contact->name; ?></a></h4>
		<?php if ($contact->isContact()): ?>
			<?php if ($company = $contact->company()): ?>
				<?php if($contact->company_role) : ?>
					<?php echo $contact->company_role; ?> <span class="muted">at</span>
				<?php endif; ?>
				<a href="<?php echo $company->url ?>"><img width='30' src="<?php echo $company->imageUrlThumb24 ?>" /></a> <a href="<?php echo $company->url ?>"><?php echo $company->name; ?></a>
			<?php endif; ?>
		<?php endif; ?>
	</div>
</div>