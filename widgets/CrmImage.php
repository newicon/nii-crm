<?php

/**
 * CrmImage class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * Display image
 */
class CrmImage
{
	
	public $contact;	
	
	public function run()
	{
		$url = Yii::app()->getModule('crm')->assetsUrl;
		
		$this->widget('nii.widgets.Gravatar',array('size'=>$this->size,'email'=>$email, 'htmlOptions'=>$this->htmlOptions),true);
	}
}