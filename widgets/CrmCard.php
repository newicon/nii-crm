<?php

/**
 * CrmCard class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * Display the contact / companies image and name
 */
class CrmCard extends CWidget
{
	/**
	 * A CrmContact model record
	 * @var CrmContact 
	 */
	public $contact = null;
	
	/**
	 * the type of card
	 * Possible values include:
	 * 'list', 'header'
	 * @var string 
	 */
	public $type = 'header';
	
	/**
	 * Html options for the media div
	 * @var array 
	 */
	public $mediaHtmlOptions = array();
	
	public function run()
	{
		if ($this->contact === null)
			throw new CException('You must specify a contact property object of type CrmContact');
		
		$this->mediaHtmlOptions = CMap::mergeArray(array('class'=>'media'), $this->mediaHtmlOptions);
		
		$this->render('_card-' . $this->type, array('contact'=>$this->contact));
	}
}