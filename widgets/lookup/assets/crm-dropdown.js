
// CrmDropDown jQuery plugin
// Depends on select2
// useage:
// ~~~
//   <input id="contact_id">
//   $('#contact_id').crmDropDown({group:'contacts', canCreateNew:true})
// ~~~
(function ($) {
    $.fn.crmDropDown = function( options ) {
		// default settings
        var settings = $.extend({
            group: "",
			canCreateNew:false,
			url:'',
			width:200,
			placeholder:'Select Contact'
        }, options );
 
		var $el = this;
        return $el.select2({
			createSearchChoice:function(term, data) {
				if (settings.canCreateNew) {
					var filter = $(data).filter(function() {
						return this.text.toLowerCase().localeCompare(term)===0;
					});
					if (filter.length===0) {
						return {img:'http://placehold.it/24x24',id:term, text:term, isNew:true};
					}
				}
			},
			width:settings.width,
			placeholder:settings.placeholder,
			dropdownAutoWidth:settings.dropdownAutoWidth,
			//multiple: true,
			allowClear: true,
			maximumSelectionSize:1,
			quietMillis: 50,
			ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
				url: settings.url,
				dataType: 'json',
				data: function (term, page) {
					return {
						name: term, // search term
						group: settings.group,
						page_limit: 30,
						page: page
					};
				},
				results: function (data, page) { 
					var more = (page * 30) < data.pagination.rows
					// parse the results into the format expected by Select2.
					// since we are using custom formatting functions we do not need to alter remote JSON data
					return {results: data.results, more:more};
				}
			},
			initSelection: function(element, callback) {
                var id=$(element).val();
				$.ajax(settings.url,{data:{contact_id:id}, dataType: "json"}).done(function(data) { 
					var item = {id:id, text:data.name, img:data.image_url}
					callback(item); 
				});
            },
			formatResult:function(item){
				return '<img width="24" src="'+item.img+'"/> '+item.text;
			},
			formatSelection:function(item){
				if (item.isNew) {
					alert('you are creating a new record');
					// do some ajax to save the new record and return an id.
					//$el.val(item.id);
				}
				return '<img width="24" src="'+item.img+'"/> '+item.text;
			},
			escapeMarkup: function(m) { return m; }
		});
    };
}(jQuery));
