<?php

/**
 * ActionContactLookup class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * ContactDropDown action
 */
class ActionDdLookup extends CAction
{
	/**
	 * Find a contact by contactId
	 * @return json encode contact
	 */
	public function run($contactId='')
	{
		$contact = NData::loadModel('CrmContact', $contactId);
		echo json_encode($contact->toArray());
	}
}