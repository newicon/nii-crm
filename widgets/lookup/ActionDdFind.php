<?php

/**
 * ActionContactLookup class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * ContactDropDown action
 */
class ActionDdFind extends CAction
{
	/**
	 * Return a result set or an individual contact
	 * if $GET['contact_id'] is specified will return the individual contaqct otherwise returns array
	 * Search contacts by the group they are in, or/and the first letter of their name
	 * @param mixed $group
	 * group is either an id of the crm_group, or one of the following key strings:
	 * all, contacts, companies
	 * @param string $letter
	 * @return json encode list of contacts for use with select2 widget (ContactDropDown)
	 */
	public function run()
	{
		if (isset($_GET['contact_id'])) {
			// lookup just one contact
			$contact = NData::loadModel('CrmContact', $_GET['contact_id']);
			$return = $contact->toArray();
			
		} else {
			$name = isset($_GET['name']) ? $_GET['name'] : '';
			$group = isset($_GET['group']) ? $_GET['group'] : 'all';
			$page = isset($_GET['page']) ? $_GET['page']-1 : 0;
			$pageSize = isset($_GET['page_limit']) ? $_GET['page_limit'] :30;

			$return = CrmApi::search(array(
				'name'=>$name,
				'group'=>$group,
				'page'=>$page,
				'page_size'=>$pageSize,
			), array(
				'select'=>'id, name',
				'formatFunction'=>function($c){
					return array(
						'img'=>$c->imageUrlThumb24, 
						'id'=>$c->id, 
						'text'=>$c->name
					);
				}
			));
		}
		
		echo json_encode($return);
	}
}