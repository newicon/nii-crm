<?php

/**
 * ContactDropDown class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * Display a dropdown of contacts.
 * returns the selected contacts id
 * <?php echo $form->fieldWidget($model, 'customer_id', 'crm.widgets.lookup.CrmDd', array('group'=>'companies')) ?>
 * //
 * $this->widget('crm.widgets.lookup.CrmDd', array('model'=>$model, 'attribute'=>'customer_id'))
 */
class CrmDd extends NInputWidget
{
	/**
	 * The CrmContact group to filter
	 * @var mixed
	 * all, companies, contacts, 1, 2 (int id of group)
	 */
	public $group = '';
	
	/**
	 * Whether the input dropdown allows new contacts to be entered
	 * Effectively coverting the dropdown to a combo box
	 * @var type 
	 */
	public $canCreateNew = false;
	
	/**
	 * Placeholder text for the drop down
	 * @var string 
	 */
	public $placeholder = 'Select Contact';
	
	/**
	 * Html Options for the input box
	 * @var array
	 */
	public $htmlOptions = array('class'=>'mbn');
	
	/**
	 * Widths of the widget
	 * @var int
	 */
	public $width = 220;
	
	/**
	 * The action to call to lookup contacts
	 * @return string
	 */
	public function getUrl()
	{
		return NHtml::url('/crm/widget/ddFind');
	}
	
	public function run()
	{
		if($this->hasModel())
			echo CHtml::activeTextField($this->model,$this->attribute,$this->htmlOptions);
		else
			echo CHtml::textField($this->name,$this->value,$this->htmlOptions);
		$this->js();
	}
	
	public function registerScript()
	{
		Yii::app()->clientScript->registerScriptFile($this->getAssetsUrl() . '/crm-dropdown.js');
		$this->createWidget('nii.widgets.select2.Select2')->registerScripts();
	}
	
	/**
	 * publish js assets for dropdown, and register scrips
	 */
	public function js()
	{
		$this->registerScript();
		// requires select2 js
		
		list($name, $id) = $this->resolveNameID();
		
		$options = array(
			'group'=>$this->group, 
			'canCreateNew'=>$this->canCreateNew, 
			'placeholder'=>$this->placeholder,
			'url'=> $this->url,
			'width'=>$this->width,
		);
		
		$options = json_encode($options);
		
		$js = "$('#$id').crmDropDown(" . $options . ')';
		Yii::app()->clientScript->registerScript("crm-dd-$id", $js, CClientScript::POS_READY);
	}
}