<?php

/**
 * CrmModule class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 *	Crm Module Class
 */
class CrmModule extends NWebModule
{
	public $name = 'CRM';
	public $description = 'Extensible Customer Relationship Management';
	
	public function init()
	{
		Yii::import('crm.models.*');
		Yii::import('crm.components.*');
	}
	
	/**
	 * Before any controller actions run within the CRM module
	 * set the theme to bs3
	 * @inheritdoc
	 * @return boolean
	 */
	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller,$action)) {
			Yii::app()->theme = 'bs3';
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * @inheritdoc
	 */
	public function setup()
	{
		Yii::app()->menus->addItem('main', 'CRM', array('/crm/index/index'), null, array('order'=>1, 'icon'=>'icon-inbox'));
		Yii::app()->image->addTypes(array(
			'crm-contact' => array(
				'noimage' => Yii::getPathOfAlias("crm.assets.styles.images.contact") . '.jpg'
			),
			'crm-contact-24' => array(
				'resize' => array('width' => 24, 'height' => 24, 'master' => 'width'),
				'crop' => array('left' => 0, 'top' => 0, 'width' => 24, 'height' => 24),
				'noimage' => Yii::getPathOfAlias("crm.assets.styles.images.contact") . '.jpg'
			),
			'crm-company' => array(
				'noimage' => Yii::getPathOfAlias("crm.assets.styles.images.company") . '.jpg'
			),
			'crm-company-24' => array(
				'resize' => array('width' => 24, 'height' => 24, 'master' => 'width'),
				'crop' => array('left' => 0, 'top' => 0, 'width' => 24, 'height' => 24),
				'noimage' => Yii::getPathOfAlias("crm.assets.styles.images.company") . '.jpg'
			),
		));
		Yii::app()->clientScript->registerCssFile($this->getAssetsUrl().'/styles/crm.css');
		
		// Add rules for restfull resources
		Yii::app()->urlManager->addRules(array(
			// group actions
			array('/crm/api/group/index',				'pattern'=>'crm/api/group',							'verb'=>'GET'),
			array('/crm/api/group/read',				'pattern'=>'crm/api/group/<id:\w+>',				'verb'=>'GET'),
			array('/crm/api/group/save',				'pattern'=>'crm/api/group/<id:\w+>',				'verb'=>'POST'),
			array('/crm/api/group/save',				'pattern'=>'crm/api/group',							'verb'=>'POST'),
			array('/crm/api/group/saveList',			'pattern'=>'crm/api/groups',						'verb'=>'POST'),
			array('/crm/api/group/removeContacts',		'pattern'=>'crm/api/group/<id:\w+>/removeContacts',	'verb'=>'POST'),
			array('/crm/api/group/addContacts',			'pattern'=>'crm/api/group/<id:\w+>/addContacts',	'verb'=>'POST'),
			array('/crm/api/group/delete',				'pattern'=>'crm/api/group/<id:\w+>',				'verb'=>'DELETE'),
			
			// search
			array('/crm/api/search/search',				'pattern'=>'crm/api/search',					'verb'=>'GET'),
			array('/crm/api/search/phone',				'pattern'=>'crm/api/phone',					'verb'=>'GET'),
			
			// contact actions
			array('/crm/api/contact/save',			'pattern'=>'crm/api/contact/<id:\w+>',			'verb'=>'POST'),
			array('/crm/api/contact/save',			'pattern'=>'crm/api/contact',					'verb'=>'POST'),
			array('/crm/api/contact/get',			'pattern'=>'crm/api/contact/<id:\w+>',			'verb'=>'GET'),
			array('/crm/api/contact/delete',		'pattern'=>'crm/api/contact/<id:\w+>',			'verb'=>'DELETE'),
			array('/crm/api/contact/action',		'pattern'=>'crm/api/contact/<id:\w+>/action',	'verb'=>'GET'),
		
			// CrmAction
			array('/crm/api/action/get',		'pattern'=>'crm/api/action/<id:\w+>',			'verb'=>'GET'),
			array('/crm/api/action/ical',		'pattern'=>'crm/api/action/<id:\w+>',			'verb'=>'GET', 'urlSuffix'=>'.ical'),
			array('/crm/api/action/save',		'pattern'=>'crm/api/action/<id:\w+>',			'verb'=>'POST'),
			array('/crm/api/action/save',		'pattern'=>'crm/api/action',					'verb'=>'POST'),
			
			// CrmNote
			array('/crm/api/note/get',			'pattern'=>'crm/api/note/<id:\w+>',		'verb'=>'GET'),
			array('/crm/api/note/save',			'pattern'=>'crm/api/note/<id:\w+>',		'verb'=>'POST'),
			array('/crm/api/note/save',			'pattern'=>'crm/api/note',				'verb'=>'POST'),
			array('/crm/api/note/delete',		'pattern'=>'crm/api/note/<id:\w+>',		'verb'=>'DELETE'),
			
			// CrmDeal
			array('pattern'=>'crm/api/deal',			'/crm/api/deal/list',			'verb'=>'GET'),
			array('pattern'=>'crm/api/deal/<id:\w+>',	'/crm/api/deal/read',			'verb'=>'GET'),
			array('pattern'=>'crm/api/deal/<id:\w+>',	'/crm/api/deal/update',			'verb'=>'PUT'),
			array('pattern'=>'crm/api/deal',			'/crm/api/deal/create',			'verb'=>'POST'),
			array('pattern'=>'crm/api/deal/<id:\w+>',	'/crm/api/deal/delete',			'verb'=>'DELETE'),
		));
		
		Yii::app()->getModule('admin')->dashboard->addPortlet('crm-latest','crm.widgets.latest-actions.LatestActionsPortlet','side');
		Yii::app()->getModule('nii')->registerScripts();
	}
	
	public function install()
	{
		NActiveRecord::install('CrmContact');
		NActiveRecord::install('CrmContactCompany');
		NActiveRecord::install('CrmEmail');
		NActiveRecord::install('CrmPhone');
		NActiveRecord::install('CrmWebsite');
		NActiveRecord::install('CrmAddress');
		NActiveRecord::install('CrmGroup');
		NActiveRecord::install('CrmGroupContact');
		NActiveRecord::install('CrmAction');
		NActiveRecord::install('CrmStatus');
		NActiveRecord::install('CrmNote');
		NActiveRecord::install('CrmDeal');
	}
	
}